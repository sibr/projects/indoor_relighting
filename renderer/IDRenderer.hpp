#ifndef __SIBR_ASSETS_IDRENDER_HPP__
#define __SIBR_ASSETS_IDRENDER_HPP__

# include "Config.hpp"
# include "core/assets/InputCamera.hpp"
# include "core/graphics/Texture.hpp"
# include "core/graphics/Camera.hpp"
# include "core/graphics/RenderUtility.hpp"
# include "core/assets/Resources.hpp"
# include "core/graphics/Shader.hpp"
# include "core/graphics/Mesh.hpp"


namespace sibr
{
	class SIBR_INDOORRELIGHTING_EXPORT IDRenderer
	{

	public:
		IDRenderer(int w,int h)  ;
		~IDRenderer();

		void render( const sibr::InputCamera &cam, const Mesh& mesh, bool backFaceCulling=false, bool frontFaceCulling=false);

		std::shared_ptr<sibr::RenderTargetInt1> _id_RT;

		
	private:
		sibr::GLShader				_idShader;
		sibr::GLParameter			_idShader_MVP;

	};

} // namespace

#endif
