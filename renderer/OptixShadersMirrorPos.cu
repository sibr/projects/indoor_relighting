
#include <optix_device.h>

#include <projects/optix/renderer/OptixSharedStructures.h>

using namespace sibr;

namespace sibr {

	extern "C" __global__ void __closesthit__pos()
	{
		const TriangleMeshData& sbtData = *(const TriangleMeshData*)optixGetSbtDataPointer();
		const int   primID = optixGetPrimitiveIndex();

		const sibr::Vector3u index = sbtData.index[primID];
		const float u = optixGetTriangleBarycentrics().x;
		const float v = optixGetTriangleBarycentrics().y;

		sibr::Vector3f pos = (1.0f - u - v) * sbtData.vertex[index[0]]
			+ u * sbtData.vertex[index[1]]
			+ v * sbtData.vertex[index[2]];

		sibr::Vector3f& prd = *(sibr::Vector3f*)getPRD<sibr::Vector3f>();
		prd = pos;

	}


	extern "C" __global__ void __anyhit__radiance()
	{ /*! for this simple example, this will remain empty */
	}

	extern "C" __global__ void __miss__radiance()
	{
		sibr::Vector3f& prd = *(sibr::Vector3f*)getPRD<sibr::Vector3f>();
		// set to constant white as background color
		prd = { 0.0f,0.0f, 0.0f };
	}

	extern "C" __global__ void __closesthit__shadow()
	{
	}

	extern "C" __global__ void __anyhit__shadow()
	{ /*! for this simple example, this will remain empty */
	}

	extern "C" __global__ void __miss__shadow()
	{
		float& prd = *(float*)getPRD<float>();
		// set to constant white as background color
		prd += 1.0f;
	}

	extern "C" __global__ void __raygen__renderPosFromBuffer()
	{
		// compute a test pattern based on pixel ID
		const int ix = optixGetLaunchIndex().x;
		const int iy = optixGetLaunchIndex().y;

		sibr::Vector4f posPRD = { 0.0f, 0.0f, 0.0f, 1.0f };

		// Pack the pointer to the result color in the payload.
		uint32_t u0, u1;
		packPointer(&posPRD, u0, u1);

		// Compute pixel coordinates, flipping to be in the same orientation as OpenGL.
		sibr::Vector2f screen(float(ix) + 0.5f, optixLaunchParams.size[1] - float(iy) - 0.5f);

		// generate ray direction
		const float4 value = tex2D<float4>(optixLaunchParams.buffers.positions, ix, iy);
		float3 cudaPos = { value.x, value.y, value.z };
		const float4 dir = tex2D<float4>(optixLaunchParams.buffers.directions, ix, iy);
		float3 cudaDir = { dir.x ,dir.y, dir.z };

		float dist = (optixLaunchParams.camera.position - sibr::Vector3f(value.x, value.y, value.z)).norm();

		optixTrace(optixLaunchParams.traversable,
			cudaPos, cudaDir, dist * 0.001f, 1e20f, 0.0f,
			OptixVisibilityMask(255), OPTIX_RAY_FLAG_DISABLE_ANYHIT,
			SURFACE_RAY_TYPE, RAY_TYPE_COUNT, SURFACE_RAY_TYPE /* missSBTIndex */, u0, u1);

		// and write to frame buffer ...

// Pack the floats in the target.
		const uint32_t fbIndex = 4 * (ix + iy * optixLaunchParams.size[0]);

		// Convert to 32-bit values.
		optixLaunchParams.dst[fbIndex + 0] = __float_as_uint(posPRD[0]);
		optixLaunchParams.dst[fbIndex + 1] = __float_as_uint(posPRD[1]);
		optixLaunchParams.dst[fbIndex + 2] = __float_as_uint(posPRD[2]);
		optixLaunchParams.dst[fbIndex + 3] = __float_as_uint(posPRD[3]);

	}

}
