#ifndef OFFLINERENDERER_IR_H
#define OFFLINERENDERER_IR_H

#include "Config.hpp"
#include <core/graphics/Image.hpp>
#include <core/graphics/Window.hpp>
#include "core/graphics/Texture.hpp"
#include <torch/torch.h>
#include "projects/torchgl_interop/renderer/torchgl_interop.h"
#define BOOST_FILESYSTEM_VERSION 3
#define BOOST_FILESYSTEM_NO_DEPRECATED 
#include <boost/filesystem.hpp>

namespace sibr {

	class SIBR_INDOORRELIGHTING_EXPORT OfflineRenderer
	{

	public:
		OfflineRenderer::OfflineRenderer(int w, int h, std::string modelPath,sibr::Window::Ptr window);
		OfflineRenderer::~OfflineRenderer();
		
		void loadLayout(std::string path);
		void get_files(const boost::filesystem::path& root, const std::string& ext, std::vector<boost::filesystem::path>& ret);

		void render(std::string path);

		//This function take as input an image in [0,1] and decode it in [0,1]
		template<class T> void decodeImLog(T& im);



	private:

		int _width;
		int _height;
		sibr::Window::Ptr _window;
		std::vector<sibr::Texture2DRGBA32F::Ptr> _textures;
		sibr::Texture2DRGB32F::Ptr _outTex;

		std::vector<std::shared_ptr<sibr::TexInfo>> _input_textures;
		std::vector<std::shared_ptr<sibr::TexInfo>> _output_textures;

		//Tensors
		std::shared_ptr<at::Tensor> _mat_tensor;
		std::shared_ptr<at::Tensor> _diff_tensor;
		std::shared_ptr<at::Tensor> _vdep_tensor;
		std::shared_ptr<at::Tensor> _out_tensor;
		std::shared_ptr<at::Tensor> _numInput;

		//Network definition
		std::string _model_path;
		std::vector<std::vector<int>> _input_texture_channels;
		std::vector<std::vector<int>> _input_texture_mapping;

		std::shared_ptr<sibr::TORCH_GL> _network;



	};
}

#endif

