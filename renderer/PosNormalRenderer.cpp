#include "PosNormalRenderer.hpp"

namespace sibr {
	PosNormalRenderer::PosNormalRenderer()
	{

		_shader.init("TexturedMesh",
			sibr::loadFile(sibr::getShadersDirectory("indoor_relighting") + "/posNormal.vert"),
			sibr::loadFile(sibr::getShadersDirectory("indoor_relighting") + "/posNormal.frag"));

		_paramMVP.init(_shader, "MVP");
	}

	sibr::PosNormalRenderer::~PosNormalRenderer()
	{
	}

	void	PosNormalRenderer::process(const Mesh& mesh, const Camera& eye, IRenderTarget& dst)
	{
		dst.bind();
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		_shader.begin();
		_paramMVP.set(eye.viewproj());
		mesh.render(true, true);
		_shader.end();
		dst.unbind();

	}

} /*namespace sibr*/
