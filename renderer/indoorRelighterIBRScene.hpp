#ifndef INDOORRELIGHTERIBRSCENE_H
#define INDOORRELIGHTERIBRSCENE_H

#include <random>
#include <core/scene/BasicIBRScene.hpp>
#include <core/graphics/Window.hpp>
#include "Config.hpp"
#include <core/raycaster/Raycaster.hpp>
#include <core/view/FPSCamera.hpp>
#include <core/system/Array2d.hpp>
#include <torch/torch.h>
#include "OrderRendering.hpp"
#include "ctpl_stl.hpp"
#include <imgui/imgui.h>

#include <projects/optix/renderer/OptixDenoiser.hpp>

namespace sibr
{
	class TORCH_GL;
	struct TexInfo;
	class MultiTexturedMeshRenderer;
	struct interactiveSession;

	enum RLOADMODE {
		ADD_ONLY = 0,
		RM_ONLY = 1,
		ADD_RM = 2
	};

	struct indoorRelighterAppArgs :
		virtual sibr::BasicIBRAppArgs {

		sibr::RequiredArg<std::string> model = { "model" };
		sibr::Arg<std::string> outPath = { "outPath", "" };
		sibr::Arg<std::string> camPath = { "camPath", "" };
		sibr::Arg<std::string> lighting = { "lighting", "default" };
		sibr::Arg<bool> genTrainData = { "genTrain" };
		sibr::Arg<bool> saveSyntheticSRGB = { "saveSyntheticSRGB" };
		sibr::Arg<bool> genTestData = { "genTest" };
		sibr::Arg<bool> runTest = { "runTest" };
		sibr::Arg<bool> reuseIfP = { "reuseIfPossible" };
		sibr::Arg<bool> synthetic = { "synthetic" };
		sibr::Arg<bool> video = { "video" };
		sibr::Arg<bool> interactive = { "interactive" };
		sibr::Arg<bool> preprocessOnly = { "preprocessOnly" };
		sibr::Arg<bool> gtGeom = { "useGTG" };
		sibr::Arg<int> outH = { "outH", -1 };
		sibr::Arg<int> outW = { "outW", -1 };
		sibr::Arg<int> numFrames = { "numFrames", 120 };
	};

	struct lightingSpec {
		std::string name;
		std::string type; // for now default or sphere
		sibr::Vector3f pos;
		float radius;
	};

	struct radDelta {
		sibr::ImageRGB32F::Ptr add;
		sibr::ImageRGB32F::Ptr rm;
	};


	class SIBR_INDOORRELIGHTING_EXPORT indoorRelighterIBRScene : public sibr::BasicIBRScene
	{


	public:
		typedef std::shared_ptr<indoorRelighterIBRScene>									Ptr;
		indoorRelighterIBRScene::indoorRelighterIBRScene(
			indoorRelighterAppArgs& myArgs,
			sibr::Window::Ptr window,
			bool noRTs = false);

		//Data generation and loading
		std::vector<sibr::ImageRGB32F::Ptr>  getData(
			void (sibr::indoorRelighterIBRScene::* renderFunc)(
				const Camera& cam,
				const sibr::ImageRGB32F::Ptr&,
				const sibr::ImageRGB32F::Ptr&,
				sibr::ImageRGB32F::Ptr&),
			std::string suffix,
			std::vector<sibr::ImageRGB32F::Ptr> pos3DMaps,
			std::vector<sibr::ImageRGB32F::Ptr> normalMaps
		);

		//Data generation and loading
		std::vector<sibr::ImageRGB32F::Ptr>  getOrComputeRadData(
			std::vector<sibr::ImageFloat1::Ptr> depthMaps,
			std::vector<sibr::ImageRGB32F::Ptr> normalMaps
		);

		std::vector<std::vector<sibr::ImageRGB32F::Ptr>>  getDataMultiScale(
			void (sibr::indoorRelighterIBRScene::* renderFunc)(
				const Camera& cam,
				const sibr::ImageFloat1::Ptr,
				const sibr::ImageRGB32F::Ptr,
				std::vector<sibr::ImageRGB32F::Ptr>&,
				float
				),
			std::string suffix,
			std::vector<sibr::ImageFloat1::Ptr> pos3DMaps,
			std::vector<sibr::ImageRGB32F::Ptr> normalMaps
		);

		// Setup textures and initialize the renderer
		bool setupRenderer();

		// Generate input buffers for current _userCamera
		void generateInputBuffers(const Camera& renderCam, int deactivate = -1, float exp = 1.0);

		// Generate full training dataset from scene
		void generateDataset();
		void dumpTestData(std::vector<sibr::InputCamera::Ptr> cams, float exp, std::string folderName = "test", std::string prefix = "");
		void static saveMosaic(const sibr::ImageRGB& im, int& num, int globalNum, std::string path, std::string name, int sideIm);
		template <class T> void static numMosaic(const T& im, int& num, int sideImW, int sideImH = -1);
		template <class T> void static fillMosaic(const T& im, int& num, std::vector<T>& mosaics, int pos, int sideImW, int sideImH = -1);

		void exportMesh2Mitsuba();
		void computeNormals(Mesh& mesh);
		void colorizeMesh(
			sibr::Mesh::Ptr mesh,
			std::vector<sibr::ImageRGB32F::Ptr> imgs,
			const std::vector<sibr::InputCamera::Ptr>& cams,
			double maxVal = 1.0);
		void normalBasedSurfaceSmoothing(sibr::Mesh& mesh);

		std::vector<sibr::Vector4f> estimateLight();
		void detectLightSources(float thresold);


		//
		void runInteractiveSession();
		struct uiData {
			int imId;
			int numInput;
			int numOutput;
			int objectToEdit;
			float exposure;
			float speedCam;
			float fovyCam;
			float expo_rdr;
			float expo_lp; //Exposure for dynamic light path
			float tempK_rdr;
			float gamma_rdr;
			float sat_rdr;
			bool imChanged;
			bool tarChanged;
			bool lightChanged;
			bool objectLoaded;
			bool networkActive;
			bool savePlayBack;
			bool computePathStats;
			std::string camPathFile;
			std::string lightPathFolder;

			uiData::uiData() : imId(-1), numInput(1), numOutput(0), objectToEdit(0), exposure(0.0), speedCam(0.0),
				imChanged(false), tarChanged(false), lightChanged(false), objectLoaded(false), networkActive(false), savePlayBack(false), computePathStats(false),
				fovyCam(70.0), expo_rdr(0.0),expo_lp(0.0), gamma_rdr(1.0), sat_rdr(1.0), tempK_rdr(6500.0),camPathFile(""),lightPathFolder("")
			{};
		};
		struct relightingDelta {
			std::string name;
			bool active = false;
			sibr::Mesh::Ptr mesh = sibr::Mesh::Ptr(new sibr::Mesh());
			std::vector<radDelta> rdlt = std::vector<radDelta>();
			float emissionExposure = 0.0;
			ImVec4 emissionColor = ImVec4(1.0, 1.0, 1.0, 1.0);

		};
		void gui(
			uiData& uiDt,
			float dlt,
			sibr::FPSCamera& fpsCam,
			interactiveSession& IS
		);
		//
		void runSaveSession(std::vector<sibr::InputCamera::Ptr>& cams, std::string outputFolder);

		void resetRelighting(bool update = true, bool deactivateAll = true);
		void loadRelightingDelta(std::string lightName, RLOADMODE mode = ADD_ONLY);
		void updateRelighting(std::string lightName, float exposure, sibr::Vector4f emissionColor, bool switchState = false);
		void updateRelighting(bool updateGeometry = true);

		//// Rendering function
		void initDM();
		void initFaceVisibility();
		void initCamsOrder();
		void renderReflexionIm(
			const Camera& renderCam,
			const sibr::ImageFloat1::Ptr depthMap,
			const sibr::ImageRGB32F::Ptr normalMap,
			std::vector<sibr::ImageRGB32F::Ptr>& mirrorIms,
			float exp = 1.0
		);

		void renderRadianceIm(
			const Camera& renderCam,
			const sibr::ImageFloat1::Ptr& depthMap,
			const sibr::ImageRGB32F::Ptr& normalMap,
			sibr::ImageRGB32F::Ptr& radIm,
			int lightId
		);

		//Helper function to sample in images

		void chooseSample(
			const std::vector<sibr::InputCamera::Ptr>& cams,
			const sibr::Vector3f& point,
			const std::vector<sibr::Vector3f>& nfPrecom,
			const sibr::Vector3f& rayDir,
			int& bestIm,
			sibr::Vector2i& pos,
			std::vector<bool>& vibility = std::vector<bool>());

		void chooseNormalSample(
			const std::vector<sibr::InputCamera::Ptr>& cams,
			const sibr::Vector3f& point,
			int& bestIm,
			sibr::Vector2i& pos,
			const std::vector<int>& camsOrder);


		void vibilityTest(
			const std::vector<sibr::InputCamera::Ptr>& cams,
			const std::vector<sibr::Vector3f>& points,
			const std::vector<sibr::Vector3f>& nfPrecom,
			std::vector<bool>& vibility);


		sibr::Mesh loadGTMesh();


		//This function take as input an image in [0,1] and encode it in [0,1] for 16bit efficient storage
		template<class T> void encodeImLog(T& im);
		template<class T> void encodeImLog(T& im,float scale);
		//This function take as input an image in [0,1] and decode it in [0,1]
		template<class T> void decodeImLog(T& im);


		lightingSpec static loadLighting(std::string pathLights, std::string filename);

		void loadLayout(std::string path, int numEntry);
		//This function uses the denoiser
		bool imFrom16BitOrRender(sibr::ImageRGB32F::Ptr img, int im, std::string suffixExr, std::string suffix16b, std::string lightingName = "", float varEV = 0.0, bool useAlbedo= false);
		//This function is to use to load captured EXR
		bool imFrom16BitOrExr(sibr::ImageRGB32F::Ptr img, int im);
		//This function tires only to get the 16bit version
		bool imFrom16Bit(sibr::ImageRGB32F::Ptr img, int im, std::string suffixExr, std::string suffix16b, std::string lightingName);


		//Optimization
		struct optim : torch::nn::Module {
			optim(int64_t N) {
				alphas = register_parameter("alphas", 1.5 * torch::ones({ 1,N,3,1,1 }, torch::kF32));
			}
			std::tuple<torch::Tensor, torch::Tensor> forward(torch::Tensor I, torch::Tensor Re, torch::Tensor Rls) {
				torch::Tensor sumR = Re + (alphas * Rls).sum(1);
				return std::make_tuple((I / (sumR + 0.0001f)), sumR);
			}

			torch::Tensor alphas;
		};


		struct sgd {
			sgd(optim& opt, int numLight) :
				_numLight(numLight),
				_SGD_opt(torch::optim::SGD(opt.parameters(), torch::optim::SGDOptions(0.1f).momentum(0.9f))),
				_opt(&opt),
				_step(0),
				_lossAcc(0)
			{};

			void step(
				torch::Tensor& I,
				torch::Tensor& Re,
				torch::Tensor& Rls,
				torch::Tensor& VisLs,
				std::tuple<torch::Tensor, torch::Tensor>& out) {

				_opt->zero_grad();
				out = _opt->forward(I, Re, Rls);

				int64_t bc = I.sizes()[0];
				torch::Tensor albedo = std::get<0>(out).view({ bc, 3,-1 });

				torch::Tensor visMask = VisLs.slice(1, 0, 1).view({ bc, 3,-1 });
				torch::Tensor invisMask = 1.0 - visMask;

				torch::Tensor weight = torch::pow(2, -(visMask.sum(2) / invisMask.sum(2)).log2().abs());

				torch::Tensor visMean = (visMask * albedo).sum(2) / visMask.sum(2);
				torch::Tensor invisMean = (invisMask * albedo).sum(2) / invisMask.sum(2);

				auto relStd = (weight * (invisMean - visMean).abs()).sum();
				//Cost to prevent alpha to be less then 1;
				auto alphaCost = torch::clamp((1.0 - _opt->alphas), 0).sum();

				auto cost = relStd + alphaCost; // +loss_l1;

				cost.backward();

				_SGD_opt.step();

				_lossAcc += cost.item<float>();
				_step++;
				int freq = 1000;
				if (_step % freq == 0) {
					torch::Tensor a0 = _opt->alphas.cpu();
					std::cout << _step << " Loss: " << std::fixed << std::setprecision(6) << _lossAcc / (freq) << "  - Alpha: " << a0.accessor<float, 5>()[0][0][0][0][0] << std::endl;
					_lossAcc = 0;
				}
			}

			torch::optim::SGD _SGD_opt;
			optim* _opt;
			int _numLight;
			int _step;
			double _lossAcc;

		};


		sibr::Vector3f tempToRGB(float kelvin);
		indoorRelighterIBRScene::~indoorRelighterIBRScene(void);

		//Renderer state
		int _width;
		int _height;
		int _maxInput;
		int _rtMax;
		bool _synthetic;
		const float _hdrF;
		const int _tileSizeW;
		const int _tileSizeH;

		bool _dmInit;

		float _scale;

		std::shared_ptr<sibr::OrderRenderer> _OR; // Contains the render targets for network input
		std::shared_ptr<sibr::TORCH_GL> _network;
		// Texture and images for network input
		sibr::Texture2DRGB32F::Ptr _outTex;

		std::vector<std::shared_ptr<sibr::TexInfo>> _input_textures;
		std::vector<std::shared_ptr<sibr::TexInfo>> _output_textures;
	private:

		sibr::Window::Ptr _window;
		sibr::indoorRelighterAppArgs _args;

		lightingSpec _lighting;
		//int _indexLight; //Index at wich the light starts in the proxy;

		//Geometry
		sibr::Raycaster _raycaster;
		sibr::Mesh::Ptr _proxyTarget;
		ctpl::thread_pool _pool;
		std::vector<sibr::Vector4f> _lights;

		//Visbility helper
		std::vector<sibr::Vector3f> _nfPrecom;
		std::vector<std::vector<bool>> _faceVisibility;
		std::vector<std::vector<int>> _camsOrder;

		//Denoiser

		std::shared_ptr<Denoiser> _denoiser;

		// Cpu stored data
		std::vector<sibr::ImageFloat1::Ptr> _dm;
		std::vector<sibr::ImageInt1::Ptr> _labelsIm;
		std::vector<sibr::ImageRGB32F::Ptr> _linDiffs;
		std::vector<sibr::ImageRGB32F::Ptr> _linVdeps;
		std::vector<sibr::ImageRGB32F::Ptr> _linImgsAlbedo;
		//std::vector<sibr::ImageRGB32F::Ptr> _pos3D;
		std::vector<sibr::ImageFloat1::Ptr> _dmTex;
		std::vector<sibr::ImageRGB32F::Ptr> _normals;
		std::vector<sibr::ImageFloat1::Ptr> _occs;
		std::vector<sibr::ImageRGB32F> _linRadAdds;
		std::vector<sibr::ImageRGB32F> _linRadRms;
		std::vector<relightingDelta> _relightingDeltas;

		// Gpu stored data
		sibr::Texture2DArrayRGB32F::Ptr _TexturesRGB;
		sibr::Texture2DArrayLum32F::Ptr _TexturesDm;
		sibr::Texture2DArrayRGB32F::Ptr _TexturesNormals;
		sibr::Texture2DArrayLum32F::Ptr _TexturesOcclusion;
		//std::vector<sibr::Texture2DArrayRGB32F::Ptr> _TexturesSpec;
		sibr::Texture2DArrayRGB32F::Ptr _TexturesSpec;
		sibr::Texture2DArrayRGB32F::Ptr _TexturesRadiance;
		sibr::Texture2DArrayRGB32F::Ptr _TexturesRadianceAdd;
		//sibr::Texture2DArrayRGB32F::Ptr _TexturesRadianceRm;


		//Tensors
		std::shared_ptr<at::Tensor> _mat_tensor;
		std::shared_ptr<at::Tensor> _diff_tensor;
		std::shared_ptr<at::Tensor> _vdep_tensor;
		std::shared_ptr<at::Tensor> _gt_tensor;
		std::shared_ptr<at::Tensor> _out_tensor;

		//Network definition
		std::string _model_path;
		std::vector<std::vector<int>> _input_texture_channels;
		std::vector<std::vector<int>> _input_texture_mapping;



	};



}

#endif