
#ifndef __SIBR_INDOORRELIGHTING_CONFIG_HPP__
# define __SIBR_INDOORRELIGHTING_CONFIG_HPP__

# include "core/graphics/Config.hpp"


# ifdef SIBR_OS_WINDOWS
#  ifdef SIBR_STATIC_RELIGHTING_DEFINE
#    define SIBR_EXPORT
#    define SIBR_NO_EXPORT
#  else
#    ifndef SIBR_INDOORRELIGHTING_EXPORT
#      ifdef SIBR_INDOORRELIGHTING_EXPORTS
          /* We are building this library */
#        define SIBR_INDOORRELIGHTING_EXPORT __declspec(dllexport)
#      else
          /* We are using this library */
#        define SIBR_INDOORRELIGHTING_EXPORT __declspec(dllimport)
#      endif
#    endif
#    ifndef SIBR_NO_EXPORT
#      define SIBR_NO_EXPORT
#    endif
#  endif
# else
#  define SIBR_INDOORRELIGHTING_EXPORT
# endif


#endif // !__SIBR_INDOORRELIGHTING_CONFIG_HPP__
