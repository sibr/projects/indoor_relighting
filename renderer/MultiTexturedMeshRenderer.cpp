#include "MultiTexturedMeshRenderer.hpp"

namespace sibr {
	MultiTexturedMeshRenderer::MultiTexturedMeshRenderer()
	{

		_shader.init("TexturedMesh",
			sibr::loadFile(sibr::getShadersDirectory("indoor_relighting") + "/multiTexturedMesh.vert"),
			sibr::loadFile(sibr::getShadersDirectory("indoor_relighting") + "/multiTexturedMesh.frag"));
		_paramMVP.init(_shader, "MVP");
		_paramExp.init(_shader, "exposure");
		_paramGam.init(_shader, "gamma");
		_paramNumTex.init(_shader, "numTex");
	}

	sibr::MultiTexturedMeshRenderer::~MultiTexturedMeshRenderer()
	{
	}

	void	MultiTexturedMeshRenderer::process(const Mesh& mesh, const Camera& eye, int numTex, IRenderTarget& dst, float exposure, float gamma)
	{
		dst.bind();
		_shader.begin();
		_paramMVP.set(eye.viewproj());
		_paramExp.set(exposure);
		_paramGam.set(gamma);
		_paramNumTex.set(numTex);
		mesh.render(true, true);
		_shader.end();
		dst.unbind();

	}

} /*namespace sibr*/
