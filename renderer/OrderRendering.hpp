#ifndef ORDERRENDERER_HPP
#define ORDERRENDERER_HPP

# include <projects/optix/renderer/OptixRaycaster.hpp>

# include <core/graphics/Shader.hpp>
# include <core/graphics/Mesh.hpp>
# include <core/graphics/Texture.hpp>
# include <core/assets/InputCamera.hpp>

# include "Config.hpp"

namespace sibr {
	class SIBR_INDOORRELIGHTING_EXPORT OrderRenderer
	{
	public:
		typedef std::shared_ptr<OrderRenderer>	Ptr;

	public:
		OrderRenderer(int w, int h,
			const std::vector<sibr::InputCamera::Ptr>& cameras,
		const sibr::Mesh::Ptr & proxy,
		sibr::Texture2DArrayRGB32F::Ptr& rgbs,
		sibr::Texture2DArrayRGB32F::Ptr& specs,
		sibr::Texture2DArrayLum32F::Ptr& depths,
		sibr::Texture2DArrayRGB32F::Ptr& normals,
		sibr::Texture2DArrayRGB32F::Ptr& radiance,
		sibr::Texture2DArrayRGB32F::Ptr& radianceAdd,
		//sibr::Texture2DArrayRGB32F::Ptr& radianceRm,
		sibr::Texture2DArrayLum32F::Ptr& occs,
		float scale
		);

		void updateTargetGeometry(sibr::Mesh::Ptr proxyTar,const std::vector<sibr::InputCamera::Ptr>& cameras);

		~OrderRenderer();

		void updateCameras(const std::vector<uint>& camIds);
		void switchInput();

		void	process(
			/*input*/	const Camera& eye,
			/*input*/	int deactivate = -1,
			/*input*/	float exp = 1.0f
		);

		void renderPosNormals(const Camera& eye, float scaleFocalHeight);
		void renderRgbSpecIBR(const Camera& eye,float exp);
		void renderRgbSpecLum(const Camera& eye,float exp);
		void renderRadAlbedo(const Camera& eye,float exp);

		bool getTurnOffState() { return _turnOffState; };
		
		//Meshes
		const sibr::Mesh::Ptr _proxy;
		sibr::Mesh::Ptr _proxyTar;
	
		//Raycaster with
		OptixRaycaster raycaster;

		//Intermidiate and final RT
		sibr::RenderTargetRGBA32F::Ptr _posNormRT;
		sibr::RenderTargetRGBA32F::Ptr _posMirrorRT;
		sibr::RenderTargetRGBA32F::Ptr _posNormTarRT;
		sibr::RenderTargetRGBA32F::Ptr _radAlbedo;
		sibr::RenderTargetRGBA32F::Ptr _tarData;
		sibr::RenderTargetRGB32F::Ptr _out_rgb_spec_IBR;
		sibr::RenderTargetRGB32F::Ptr _out_rgb_spec_Lum;

	private:
		GLShader			_shaderPosNorm;
		GLShader			_shaderRgbSpecIBR;
		GLShader			_shaderRgbSpecLum;
		GLShader			_shaderRadAlbedo;
		GLShader			_shaderTarData;

		//Size
		int _w;
		int _h;
		float _scale;
		int _rtMax;
		int _numRT;

		GLParameter			_paramMVP;
		GLParameter			_paramM;
		GLParameter			_paramV;

		/// Camera infos data structure shared between the CPU and GPU.
				/// We have to be careful about alignment if we want to send those struct directly into the UBO.
		struct CameraUBOInfos {
			Matrix4f vp;
			Matrix4f inv_vp;
			Vector3f pos;
			int selected = 0;
			Vector3f dir;
			float dummy = 0.0f; // padding to a multiple of 16 bytes.
		};

		size_t _maxNumCams = 0;
		GLuniform<int> _camsCount = 0;
		GLuniform<float> _expO = 0;
		GLuniform<float> _expS = 0;
		GLuniform<float> _expIBR = 0;
		GLuniform<float> _expLum = 0;
		GLuniform<float> _expTarSce = 0;
		bool _turnOffState = false;
		GLuniform<bool> _turnOff = false;
		GLuniform<int> _compressNormals = 0;
		std::vector<CameraUBOInfos> _cameraInfos;
		GLuint _uboIndex;
		GLuniform<Vector3f>					_uCamPos;
		GLuniform<Vector3f>					_uCamPosTar;
		GLuniform<Vector3f>					_uCamPosIBR;
		GLuniform<Vector3f>					_uCamPosLum;
		GLuniform<Vector3f>					_uCamDir;
		GLuniform<Vector3f>					_uCamDirIBR;
		GLuniform<Vector3f>					_uCamDirLum;
		GLuniform<float>					_uScaling;
		GLuniform<std::vector<int>>			_orderCam;
		
		sibr::Texture2DArrayRGB32F::Ptr _rgbs;
		sibr::Texture2DArrayRGB32F::Ptr _rads;
		sibr::Texture2DArrayRGB32F::Ptr _specs;

		sibr::Texture2DArrayLum32F::Ptr _depths;
		sibr::Texture2DArrayRGB32F::Ptr _normals;
		sibr::Texture2DArrayLum32F::Ptr _occs;

		sibr::Texture2DArrayLum32F::Ptr _depthsTar;
		sibr::Texture2DArrayRGB32F::Ptr _tarRadAdds;
		//sibr::Texture2DArrayRGB32F::Ptr _tarRadRms;
		sibr::Texture2DArrayRGB32F::Ptr _albedosTar;

	};

} /*namespace sibr*/

#endif // __SIBR_EXP_RENDERER_TEXTUREDMESHRENDERER_HPP__
