#ifndef TEXTUREMAPRENDER_HPP
#define TEXTUREMAPRENDER_HPP

# include "Config.hpp"

# include "core/assets/InputCamera.hpp"
# include "core/graphics/Texture.hpp"
# include "core/graphics/Camera.hpp"
# include "core/graphics/RenderUtility.hpp"
# include "core/assets/Resources.hpp"
# include "core/graphics/Shader.hpp"
# include "core/graphics/Mesh.hpp"


namespace sibr
{
	class SIBR_INDOORRELIGHTING_EXPORT TextureMapRenderer
	{

	public:
		TextureMapRenderer(int w,int h, bool useFloat) ;
		~TextureMapRenderer();

		void render( const sibr::InputCamera &cam, const Mesh& mesh);
		void setWH(int w, int h);
		std::shared_ptr<sibr::RenderTargetRGB32F> _textureMap_RT32F;
		std::shared_ptr<sibr::RenderTargetRGB> _textureMap_RT;

	private:
		sibr::GLShader				_textureMapShader;
		sibr::GLParameter			_textureMapShader_proj;
		bool _useFloat;
	};


} // namespace

#endif
