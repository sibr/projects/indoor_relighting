
#include "OfflineRenderer.hpp"

namespace fs = ::boost::filesystem;




namespace sibr {

	OfflineRenderer::~OfflineRenderer() {};

	OfflineRenderer::OfflineRenderer(int w, int h, std::string modelPath, sibr::Window::Ptr window)
	{

		_width = w;
		_height = h;
		_window = window;
		_textures = std::vector<sibr::Texture2DRGBA32F::Ptr>();

		for (int i = 0; i < 32; i++) {
			_textures.push_back(
				std::make_shared<sibr::Texture2DRGBA32F>(
					sibr::ImageRGBA32F(_width, _height, sibr::Vector4f(0.0f, 0.0f, 0.0f, 1.0f))
					, SIBR_CLAMP_UVS)
			);

		}


		_window->makeContextCurrent();


		////Setup torch architecture
		loadLayout(modelPath + "/layout.txt");
		_model_path = modelPath + "/model.pt";

		for (const auto & t : _textures) {
			_input_textures.push_back(std::make_shared<sibr::TexInfo>(sibr::TexInfo(t)));
		}

		_outTex = std::make_shared<sibr::Texture2DRGB32F>(sibr::ImageRGB32F(_width, _height, sibr::Vector3f(1.0f, 0.7f, 0.7f)), SIBR_CLAMP_UVS);
		_output_textures.push_back(std::make_shared<sibr::TexInfo>(sibr::TexInfo(_outTex)));

		//// Setup the tensors for the architecture :
		std::vector<torch::jit::IValue> input_Ivalue;
		//// these are the actual input to the network
		_mat_tensor = std::make_shared<at::Tensor>(torch::zeros({ 1,8,14,_height,_width }).cuda());
		_diff_tensor = std::make_shared<at::Tensor>(torch::zeros({ 1,6,_height,_width }).cuda());
		_vdep_tensor = std::make_shared<at::Tensor>(torch::zeros({ 1,5,_height,_width }).cuda());
		_numInput = std::make_shared<at::Tensor>(8 * torch::ones({ 1 }).toType(torch::ScalarType::Int).cuda());

		input_Ivalue.push_back(*_mat_tensor);
		input_Ivalue.push_back(*_diff_tensor);
		input_Ivalue.push_back(*_vdep_tensor);
		//input_Ivalue.push_back(*_numInput);

		////Here we setup the slices in wich to copy the textures. This is necessary because we input a tensor with 5 dimensions
		std::vector<std::shared_ptr<at::Tensor>> preAllocTensors;
		preAllocTensors.push_back(_mat_tensor);
		preAllocTensors.push_back(_diff_tensor);
		preAllocTensors.push_back(_vdep_tensor);

		//// Setup torch_gl linker
		_network = std::make_shared<sibr::TORCH_GL>(
			_model_path,
			_input_textures,
			_input_texture_mapping,
			_input_texture_channels,
			_output_textures,
			_window,
			preAllocTensors,
			input_Ivalue);




	}

	void OfflineRenderer::render(std::string path)
	{
		std::string ext = ".png";
		std::vector<fs::path> ret;
		std::replace(path.begin(), path.end(), '\\', '/');
		fs::path path_fs(path);
		get_files(path_fs, ext, ret);
		
		int chunk = 32;
		for (int f_i = 0; f_i < ret.size();f_i+=chunk) {


			std::vector<sibr::ImageRGBA32F> chunk_ims(chunk);
#pragma omp parallel for
			for (int f_j = 0; f_j < chunk; f_j++) {
				int f_num = f_i + f_j;
				if (f_num < ret.size()) {
					fs::path p(path + "/" + ret[f_num].string());

					sibr::ImageRGBA32F im;
					im.load(p.string());
					decodeImLog(im);
					chunk_ims[f_j] = im.clone();
				}
			}

			for (int f_j = 0; f_j < chunk; f_j++) {
				int f_num = f_i + f_j;
				if (f_num < ret.size()) {
					int t_id = 0;
					for (auto & t : _textures) {
						sibr::ImageRGBA32F subIm;
						subIm.fromOpenCV(chunk_ims[f_j].toOpenCV()(cv::Rect(0, t_id*_height, _width, _height)));
						t->update(subIm);
						t_id++;
					}

					_network->run(std::vector<at::Tensor>() = { *_numInput });

					sibr::ImageRGB32F imOut = _outTex->readBack();
					imOut.flipH();
					imOut.saveHDR(path + "/" + ret[f_num].stem().string() + "_out.exr");
				}
			}
		}

	}

	template<class T>
	void sibr::OfflineRenderer::decodeImLog(T& im)
	{
		cv::pow(im.toOpenCV(), 1.0 / 0.4, im.toOpenCVnonConst());
		cv::divide(im.toOpenCV(), cv::Scalar(1.0, 1.0, 1.0) - im.toOpenCV(), im.toOpenCVnonConst());
		im.toOpenCVnonConst() = im.toOpenCV() /1.0;
	}

	void OfflineRenderer::loadLayout(std::string path)
	{
		//loading the layout
		if (!boost::filesystem::exists(path)) {
			std::cout << "Error : layout.txt file not found at " << path << ", you need to have a layout file" << std::endl;
			SIBR_ERR;
		}
		std::ifstream inLayout(path);
		std::string line;

		int l = 0;
		while (std::getline(inLayout, line))
		{
			std::stringstream check1(line);
			std::string intermediate;
			std::vector<int> input_layout;
			while (getline(check1, intermediate, ' '))
			{
				input_layout.push_back(std::strtof(intermediate.c_str(), 0));
			}

			if (l < 3)
				_input_texture_channels.push_back(input_layout);
			else
				_input_texture_mapping.push_back(input_layout);

			l++;
		}


	}

	// return the filenames of all files that have the specified extension
	// in the specified directory and all subdirectories
	void sibr::OfflineRenderer::get_files(const fs::path& root, const std::string& ext, std::vector<fs::path>& ret)
	{
		if (!fs::exists(root) || !fs::is_directory(root))
			return;

		fs::directory_iterator it(root);
		fs::directory_iterator endit;

		while (it != endit)
		{
			if (fs::is_regular_file(*it) && it->path().extension() == ext) ret.push_back(it->path().filename());
			++it;

		}

	}


}
