#include "indoorRelighterIBRScene.hpp"
#include <core/scene/ParseData.hpp>
#include <core/scene/CalibratedCameras.hpp>
#include <core/scene/ProxyMesh.hpp>
#include <core/scene/InputImages.hpp>
#include "projects/torchgl_interop/renderer/torchgl_interop.h"
#include <core/graphics/Input.hpp>
#include <core/raycaster/CameraRaycaster.hpp>
#include <core/renderer/DepthRenderer.hpp>
#include <core/renderer/TexturedMeshRenderer.hpp>
#include "TextureMapRenderer.hpp"
#include "IDRenderer.hpp"
#include "MultiTexturedMeshRenderer.hpp"
#include "OrderRendering.hpp"
#include "PosNormalRenderer.hpp"
#include "core/system/XMLTree.h"
#include "core/imgproc/MeshTexturing.hpp"
#include "core/raycaster/PlaneEstimator.hpp"

namespace sibr {

	template <typename T, typename Compare>
	std::vector<std::size_t> sort_permutation(
		const std::vector<T>& vec,
		const Compare& compare)
	{
		std::vector<std::size_t> p(vec.size());
		std::iota(p.begin(), p.end(), 0);
		std::sort(p.begin(), p.end(),
			[&](std::size_t i, std::size_t j) { return compare(vec[i], vec[j]); });
		return p;
	}

	template <typename T>
	std::vector<T> apply_permutation(
		const std::vector<T>& vec,
		const std::vector<std::size_t>& p)
	{
		std::vector<T> sorted_vec(vec.size());
		std::transform(p.begin(), p.end(), sorted_vec.begin(),
			[&](std::size_t i) { return vec[i]; });
		return sorted_vec;
	}

	//typedef sibr::Array2d<std::vector<std::pair<float, int>>> WeightMap;


	indoorRelighterIBRScene::~indoorRelighterIBRScene(void) {};

	indoorRelighterIBRScene::indoorRelighterIBRScene(
		indoorRelighterAppArgs& myArgs,
		sibr::Window::Ptr window,
		bool noRTs) : _model_path(myArgs.model), _args(myArgs), _window(window), _hdrF(myArgs.synthetic ? 64.0 : 1.0), _tileSizeW(704), _tileSizeH(560),

		_denoiser(std::make_shared<Denoiser>()), _dmInit(false)
	{

		_pool.resize(40);
		_maxInput = 8;
		_rtMax = SIBR_MAX_SHADER_ATTACHMENTS;
		_synthetic = _args.synthetic;


		/////TEST Mesh smoothing
/*
		sibr::Mesh meshTest;
		meshTest.load(_args.dataset_path.get() + "/pmvs_recon_normals.ply");

		normalBasedSurfaceSmoothing(meshTest);

		meshTest.save(_args.dataset_path.get() + "/pmvs_recon_smoothed.ply");
		return;
*/
////

		std::ifstream infile(_args.dataset_path.get() + "/scale.txt");

		if (infile.good())
		{
			std::string sLine;
			std::getline(infile, sLine);
			_scale = std::stof(sLine);
		}
		else {
			std::cout << "Error, scale.txt invalid" << std::endl;
			SIBR_ERR;
		}
		infile.close();
		///////// LOADING /////////


		if (!_synthetic) {

			//New loading func
			std::cout << "loading real scene" << std::endl;

			//Proxy first
			_proxies.reset(new ProxyMesh());

			sibr::Mesh::Ptr mesh(new sibr::Mesh());
			if (!mesh->load(_args.dataset_path.get() + "/meshes/recon.ply")) {
				mesh->load(_args.dataset_path.get() + "/meshes/pmvs_recon_simplified.ply");
				computeNormals(*mesh);
				normalBasedSurfaceSmoothing(*mesh);
				mesh->save(_args.dataset_path.get() + "/meshes/recon.ply");
			}
			sibr::Vector3f center;
			float radius;
			mesh->getBoundingSphere(center, radius, true);

			sibr::Mesh::Ptr envMesh = sibr::Mesh::getSphereMesh(center, 2.0 * radius, false, 72);
			envMesh->makeWhole();
			mesh->merge(*envMesh);

			_proxies->replaceProxy(mesh);

			// setup calibrated cameras
			_data.reset(new ParseData());
			_data->getParsedData(myArgs);
			_cams->setupFromData(_data);


			_linDiffs = std::vector<sibr::ImageRGB32F::Ptr>(_cams->inputCameras().size());
			_linVdeps = std::vector<sibr::ImageRGB32F::Ptr>(_cams->inputCameras().size());

#pragma omp parallel for
			for (int im = 0; im < _cams->inputCameras().size(); im++) {

				sibr::ImageRGB32F::Ptr imgDiff(new sibr::ImageRGB32F());

				if (!imFrom16BitOrExr(imgDiff, im)) {
					std::cout << "Could not load diff or vdep image " << im << std::endl;
					SIBR_ERR;
				}

				_linDiffs[im] = imgDiff;
				sibr::ImageRGB32F::Ptr imgVdep(new sibr::ImageRGB32F(imgDiff->w(), imgDiff->h()));
				_linVdeps[im] = imgVdep;

			}


		}
		else {

			_lighting = loadLighting(myArgs.dataset_path.get() + "/lightings/", myArgs.lighting);
			std::cout << "loading synthetic scene" << std::endl;

			// load proxy
			_proxies.reset(new ProxyMesh());
			sibr::Mesh::Ptr synthMesh(new sibr::Mesh());
			//synthMesh->loadMtsXML(_args.dataset_path.get() + "/scene.xml");
			if (_args.gtGeom) {
				synthMesh->loadMtsXML(_args.dataset_path.get() + "/mitsuba/scene.xml");
			}
			else if (!synthMesh->load(_args.dataset_path.get() + "/pmvs_recon_normals.ply")) {
				synthMesh->load(_args.dataset_path.get() + "/pmvs_recon_aligned_simplified.ply");
				computeNormals(*synthMesh);
				synthMesh->save(_args.dataset_path.get() + "/pmvs_recon_normals.ply");
			}

			if (_lighting.type == "sphere") {
				//Add a sphere to the mesh
				sibr::Mesh::Ptr lightMesh = sibr::Mesh::getSphereMesh(_lighting.pos, _lighting.radius, false, 72);
				lightMesh->makeWhole();
				lightMesh->generateSmoothNormals(1);
				//_indexLight = synthMesh->vertices().size();
				synthMesh->merge(*lightMesh);
			}
			else if (_lighting.type == "default") {
				sibr::Vector3f center;
				float radius;
				synthMesh->getBoundingSphere(center, radius, true);

				sibr::Mesh::Ptr envMesh = sibr::Mesh::getSphereMesh(center, 2.0 * radius, false, 72);
				envMesh->makeWhole();
				synthMesh->merge(*envMesh);

			}


			_proxies->replaceProxy(synthMesh);

			sibr::Vector3f center;
			float radius;
			synthMesh->getBoundingSphere(center, radius, true);

			// setup calibrated cameras
			_cams.reset(new CalibratedCameras());
			std::vector<sibr::InputCamera::Ptr> synthCam = sibr::InputCamera::loadLookat(
				_args.dataset_path.get() + "/cameras.lookat",
				{ sibr::Vector2u(1152,864) });
			_cams->setupCamerasFromExisting(synthCam);
			sibr::InputCamera::saveAsBundle(synthCam, _args.dataset_path.get() + "/bundle.out");

			// load input images
			_imgs.reset(new InputImages());
			std::vector<sibr::ImageRGB::Ptr> synthImgs8(synthCam.size());
			_linDiffs = std::vector<sibr::ImageRGB32F::Ptr>(_cams->inputCameras().size());
			_linVdeps = std::vector<sibr::ImageRGB32F::Ptr>(_cams->inputCameras().size());

			std::vector<int> imToCompute;
#pragma omp parallel for
			for (int im = 0; im < synthCam.size(); im++) {

				sibr::ImageRGB32F::Ptr imgDiff(new sibr::ImageRGB32F());
				sibr::ImageRGB32F::Ptr imgVdep(new sibr::ImageRGB32F());
				std::string suffixExrDiff = "_diff";
				std::string suffixExrVdep = "_spec";
				if (_lighting.type == "default") {
					suffixExrDiff = "_diffPath";
					suffixExrVdep = "_specPath";
				}
				if (!imFrom16Bit(imgDiff, im, suffixExrDiff, "_diff", "") || !imFrom16Bit(imgVdep, im, suffixExrVdep, "_vdep", "")) {
#pragma omp critical
					{
						imToCompute.push_back(im);
					}
				}
				else {
					_linDiffs[im] = imgDiff;
					_linVdeps[im] = imgVdep;
				}
				
				if (_args.saveSyntheticSRGB) {
					sibr::ImageRGB32F save;
					save.fromOpenCV(16.0f * (_linDiffs[im]->toOpenCV() + _linVdeps[im]->toOpenCV()));
					cv::pow(save.toOpenCV(), 1.0 / 2.2, save.toOpenCV());
					save.save(_args.dataset_path.get() + "/images/tm_" + std::to_string(im) + ".png");
				}

			}
			// /!\ Do not parallelize, denoising needs to be single threaded for cuda to work properly
			for (int im : imToCompute) {

				sibr::ImageRGB32F::Ptr imgDiff(new sibr::ImageRGB32F());
				sibr::ImageRGB32F::Ptr imgVdep(new sibr::ImageRGB32F());

				std::string suffixExrDiff = "_diff";
				std::string suffixExrVdep = "_spec";
				if (_lighting.type == "default") {
					suffixExrDiff = "_diffPath";
					suffixExrVdep = "_specPath";
				}
				if (!imFrom16BitOrRender(imgDiff, im, suffixExrDiff, "_diff", "", 0.0, true) || !imFrom16BitOrRender(imgVdep, im, suffixExrVdep, "_vdep", "", 0.0, true)) {
					std::cout << "Could not load diff or vdep image " << im << std::endl;
					SIBR_ERR;
				}
				_linDiffs[im] = imgDiff;
				_linVdeps[im] = imgVdep;
			}



			//_imgs->InputImages::loadFromExisting(synthImgs8);
			//std::cout << "Number of Images loaded: " << _imgs->inputImages().size() << std::endl;

			//synthMesh->save(_args.dataset_path.get() + "/recon.ply");
		}

		if (_args.preprocessOnly) {
			//Setup the raycaster
			_raycaster.init();
			_raycaster.addMesh(_proxies->proxy());
		}

		if (_args.outH < 0 || _args.outW < 0) {

			if (_args.outH > 0) {
				_height = _args.outH;
				_width = 4 * (_height * _linDiffs[0]->w() / (4 * _linDiffs[0]->h()));
			}
			else if (_args.outW > 0) {
				_width = _args.outW;
				_height = 4 * (_width * _linDiffs[0]->h() / (4 * _linDiffs[0]->w()));
			}
			else {
				_width = 768;
				_height = 4 * (_width * _linDiffs[0]->h() / (4 * _linDiffs[0]->w()));
			}
		}
		else {
			_width = _args.outW;
			_height = _args.outH;
		}


		//Pre-compute near and far for faster camera selection
		const auto& cams = _cams->inputCameras();
		for (int im = 0; im < cams.size(); im++) {
			_nfPrecom.push_back(
				sibr::Vector3f(
					(2.0 * cams[im]->znear() * cams[im]->zfar()),
					cams[im]->zfar() + cams[im]->znear(),
					(cams[im]->zfar() - cams[im]->znear())
				)
			);
		}


		//Setup the renderer
		if (!setupRenderer()) {
			return;
		}
		if (_args.preprocessOnly)
			return;

		//_denoiser = nullptr;


											   //Eventually generate the dataset for training
		if (_args.synthetic && _args.genTrainData) {
			generateDataset();
			return;
		}
		else if (!_args.synthetic && _args.genTestData) {
			std::vector<sibr::InputCamera::Ptr> cams;
			cams.push_back(_cams->inputCameras()[0]);
			dumpTestData(cams, 1.0f);
			return;
		}




		std::cout << "Creating textures for network input:";

		//// First the mat_data
		/// 0->2*maxInput-1
		for (int k = 0; k < 4; k++) {
			_input_textures.push_back(std::make_shared<sibr::TexInfo>(sibr::TexInfo(_OR->_out_rgb_spec_IBR, k)));
			_input_textures.push_back(std::make_shared<sibr::TexInfo>(sibr::TexInfo(_OR->_out_rgb_spec_IBR, k + 4)));
		}
		for (int k = 0; k < 4; k++) {
			_input_textures.push_back(std::make_shared<sibr::TexInfo>(sibr::TexInfo(_OR->_out_rgb_spec_Lum, k)));
			_input_textures.push_back(std::make_shared<sibr::TexInfo>(sibr::TexInfo(_OR->_out_rgb_spec_Lum, k + 4)));
		}

		/// 16
		//Radiance source
		_input_textures.push_back(std::make_shared<sibr::TexInfo>(sibr::TexInfo(_OR->_radAlbedo, 0)));

		/// 17
		//Normal source
		_input_textures.push_back(std::make_shared<sibr::TexInfo>(sibr::TexInfo(_OR->_posNormRT, 4)));

		/// 18
		//Disp source
		_input_textures.push_back(std::make_shared<sibr::TexInfo>(sibr::TexInfo(_OR->_posNormRT, 5)));

		//// Then the diff data
		/// 19
		//Target Rad Add
		_input_textures.push_back(std::make_shared<sibr::TexInfo>(sibr::TexInfo(_OR->_tarData, 0)));
		/// 20
		//Target Rad Remove
		_input_textures.push_back(std::make_shared<sibr::TexInfo>(sibr::TexInfo(_OR->_tarData, 1)));

		//// Then the vdep data
		/// 21
		//Target Spec
		_input_textures.push_back(std::make_shared<sibr::TexInfo>(sibr::TexInfo(_OR->_tarData, 2)));
		/// 22
		//Target Angle
		_input_textures.push_back(std::make_shared<sibr::TexInfo>(sibr::TexInfo(_OR->_posNormRT, 3)));
		/// 23
		//Target Mirror Depth
		_input_textures.push_back(std::make_shared<sibr::TexInfo>(sibr::TexInfo(_OR->_tarData, 6)));

		_outTex = std::make_shared<sibr::Texture2DRGB32F>(sibr::ImageRGB32F(_width, _height, sibr::Vector3f(1.0f, 0.7f, 0.7f)), SIBR_CLAMP_UVS);
		_output_textures.push_back(std::make_shared<sibr::TexInfo>(sibr::TexInfo(_outTex)));



		//// Setup the tensors for the architecture :
		std::vector<torch::jit::IValue> input_Ivalue;
		//// these are the actual input to the network
		_mat_tensor = std::make_shared<at::Tensor>(torch::zeros({ 1,55,_height,_width }, torch::TensorOptions().dtype(torch::kFloat32).device(torch::kCUDA, 0)));
		_diff_tensor = std::make_shared<at::Tensor>(torch::zeros({ 1,6,_height,_width }, torch::TensorOptions().dtype(torch::kFloat32).device(torch::kCUDA, 0)));
		_vdep_tensor = std::make_shared<at::Tensor>(torch::zeros({ 1,5,_height,_width }, torch::TensorOptions().dtype(torch::kFloat32).device(torch::kCUDA, 0)));

		input_Ivalue.push_back(*_mat_tensor);
		input_Ivalue.push_back(*_diff_tensor);
		input_Ivalue.push_back(*_vdep_tensor);
		//input_Ivalue.push_back(*_numInput);

		////Here we setup the slices in wich to copy the textures. This is necessary because we input a tensor with 5 dimensions
		std::vector<std::shared_ptr<at::Tensor>> preAllocTensors;
		preAllocTensors.push_back(_mat_tensor);
		preAllocTensors.push_back(_diff_tensor);
		preAllocTensors.push_back(_vdep_tensor);


		////Setup torch architecture
		loadLayout(_args.model.get() + "/layout.txt", input_Ivalue.size());
		_model_path = _args.model.get() + "/model.pt";

		//// Setup torch_gl linker
		_network = std::make_shared<sibr::TORCH_GL>(
			_model_path,
			_input_textures,
			_input_texture_mapping,
			_input_texture_channels,
			_output_textures,
			window,
			preAllocTensors,
			input_Ivalue);
	};

	bool indoorRelighterIBRScene::imFrom16Bit(sibr::ImageRGB32F::Ptr img, int im, std::string suffixExr, std::string suffix16b, std::string lightingName)
	{
		if (lightingName == "") {
			lightingName = _lighting.name;
		}
		sibr::ImageRGB32F imgF;
		bool fromExr = false;
		if (
			!imgF.load(_args.dataset_path.get() + "/16bitData/" + lightingName + "/" + std::to_string(im) + suffix16b + ".png", false, false)
			) {
			bool loaded = imgF.load(_args.dataset_path.get() + "/exr/" + lightingName + "/" + _cams->inputCameras()[im]->name() + suffixExr + ".exr", false, false);
			if (!loaded) {
				SIBR_WRG << "Failed to load image"
					<< _args.dataset_path.get() + "/16bitData/" + lightingName + "/" + std::to_string(im) + suffix16b + ".png"
					<< "and" << _args.dataset_path.get() + "/exr/" + lightingName + "/" + _cams->inputCameras()[im]->name() + suffixExr + ".exr" << std::endl;
				return false;
			}
			fromExr = true;
		}

		if (!fromExr) {
			decodeImLog(imgF);
			img->fromOpenCV(imgF.toOpenCV());
			return true;
		}
		else {
			return false;
		}
	}


	bool indoorRelighterIBRScene::imFrom16BitOrRender(sibr::ImageRGB32F::Ptr img, int im, std::string suffixExr, std::string suffix16b, std::string lightingName, float varEV, bool useAlbedo)
	{
		if (lightingName == "") {
			lightingName = _lighting.name;
		}
		sibr::ImageRGB32F imgF;
		bool fromExr = false;
		if (
			!imgF.load(_args.dataset_path.get() + "/16bitData/" + lightingName + "/" + std::to_string(im) + suffix16b + ".png", false, false)
			) {
			bool loaded = imgF.load(_args.dataset_path.get() + "/exr/" + lightingName + "/" + _cams->inputCameras()[im]->name() + suffixExr + ".exr", false, false);
			if (!loaded)
				loaded = imgF.load(_args.dataset_path.get() + "/exr/" + lightingName + "/" + sibr::removeExtension(_cams->inputCameras()[im]->name()) + suffixExr + ".exr", false, false);
			if (!loaded) {
				SIBR_WRG << "Failed to load image"
					<< _args.dataset_path.get() + "/16bitData/" + lightingName + "/" + std::to_string(im) + suffix16b + ".png"
					<< "and" << _args.dataset_path.get() + "/exr/" + lightingName + "/" + _cams->inputCameras()[im]->name() + suffixExr + ".exr"
					<< "and" << _args.dataset_path.get() + "/exr/" + lightingName + "/" + sibr::removeExtension(_cams->inputCameras()[im]->name()) + suffixExr + ".exr" << std::endl;
				return false;
			}
			fromExr = true;
		}

		if (!fromExr) {
			decodeImLog(imgF);
			img->fromOpenCV(imgF.toOpenCV());
		}
		else {
			//Artificial vignetting
			if (_synthetic && (suffixExr == "_diff" || suffixExr == "_spec")) {
#pragma omp parallel for
				for (int j = 0; j < imgF.h(); j++) {
					for (int i = 0; i < imgF.w(); i++) {

						float dC = (float)(sibr::Vector2f(i, j) - sibr::Vector2f(imgF.w() / 2, imgF.h() / 2)).norm() / (float)sibr::Vector2f(imgF.w() / 2, imgF.h() / 2).norm();
						float cos4 = pow(cos(dC / 2.0f), 4.0f);

						imgF(i, j).x() = std::min(1.1f, imgF(i, j).x() * cos4);
						imgF(i, j).y() = std::min(1.1f, imgF(i, j).y() * cos4);
						imgF(i, j).z() = std::min(1.1f, imgF(i, j).z() * cos4);
					}
				}
			}

			sibr::ImageRGB32F::Ptr albedo(new sibr::ImageRGB32F());
			if (useAlbedo) {
				albedo->load(_args.dataset_path.get() + "/exr/common/" + _cams->inputCameras()[im]->name() + "_albedo.png");
			}
			else {
				albedo = nullptr;
			}
			sibr::ImageRGB32F::Ptr denoised = _denoiser->denoise(imgF, albedo);
			//denoised->fromOpenCV(denoised->toOpenCV());

			sibr::makeDirectory(_args.dataset_path.get() + "/16bitData/" + lightingName + "/");
			img->fromOpenCV(denoised->toOpenCV());

			//Saving
			encodeImLog(*denoised);

			sibr::ImageRGB16 toSave;
			denoised->toOpenCV().convertTo(toSave.toOpenCVnonConst(), CV_16UC3, 65535.0f);
			cv::imwrite(_args.dataset_path.get() + "/16bitData/" + lightingName + "/" + std::to_string(im) + suffix16b + ".png", toSave.toOpenCVBGR());
		}

		return true;

	}

	bool indoorRelighterIBRScene::imFrom16BitOrExr(sibr::ImageRGB32F::Ptr img, int im)
	{
		sibr::ImageRGB32F imgF;
		bool fromExr = false;
		if (
			!imgF.load(_args.dataset_path.get() + "/16bitData/" + std::to_string(im) + ".png", false)
			) {
			bool loaded = imgF.load(_args.dataset_path.get() + "/images/" + _cams->inputCameras()[im]->name(), false);
			if (!loaded) {
				SIBR_WRG << "Failed to load image"
					<< _args.dataset_path.get() + "/16bitData/" + std::to_string(im) + ".png"
					<< "and" << _args.dataset_path.get() + "/images/" + _cams->inputCameras()[im]->name() << std::endl;
				return false;
			}
			fromExr = true;
		}

		if (!fromExr) {
			decodeImLog(imgF);
			img->fromOpenCV(imgF.toOpenCV());
		}
		else {
			sibr::makeDirectory(_args.dataset_path.get() + "/16bitData/");
			img->fromOpenCV(imgF.toOpenCV());
			//Saving
			encodeImLog(imgF);

			sibr::ImageRGB16 toSave;
			imgF.toOpenCV().convertTo(toSave.toOpenCVnonConst(), CV_16UC3, 65535.0f);
			cv::imwrite(_args.dataset_path.get() + "/16bitData/" + std::to_string(im) + ".png", toSave.toOpenCVBGR());
		}

		return true;

	}



	std::vector<sibr::ImageRGB32F::Ptr> sibr::indoorRelighterIBRScene::getData(
		void (sibr::indoorRelighterIBRScene::* renderFunc)(
			const Camera& cam,
			const sibr::ImageRGB32F::Ptr&,
			const sibr::ImageRGB32F::Ptr&,
			sibr::ImageRGB32F::Ptr&),
		std::string suffix,
		std::vector<sibr::ImageRGB32F::Ptr> pos3DMaps,
		std::vector<sibr::ImageRGB32F::Ptr> normalMaps
	)
	{
		std::vector<sibr::ImageRGB32F::Ptr> vecIm(_cams->inputCameras().size());
		std::vector<int> imToCompute;
		//Load in parallel
#pragma omp parallel for
		for (int im = 0; im < _cams->inputCameras().size(); im++) {
			sibr::ImageRGB32F imgF;
			if (imgF.load(_args.dataset_path.get() + "/16bitData/" + _lighting.name + "/" + std::to_string(im) + "_" + suffix + ".png", false, false) &&
				imgF.w() >= _args.texture_width) {
				sibr::ImageRGB32F::Ptr imgRender(new sibr::ImageRGB32F(imgF.w(), imgF.h()));
				//imgF.toOpenCV().convertTo(imgSpec->toOpenCVnonConst(), CV_16UC3, 65535.0f);
				decodeImLog(imgF);
				imgRender->fromOpenCV(imgF.resized(pos3DMaps[im]->w(), pos3DMaps[im]->h(), cv::INTER_AREA).toOpenCV());
				vecIm[im] = imgRender;
			}
			else {
#pragma omp critical
				{
					imToCompute.push_back(im);
				}
			}
		}

		//Compute the missing ones
		for (int im : imToCompute) {
			std::cout << "Computing " << suffix << " num " << im << std::endl;
			sibr::ImageRGB32F imgF;
			sibr::ImageRGB16 img16;
			sibr::ImageRGB32F::Ptr imgRender(new sibr::ImageRGB32F(pos3DMaps[im]->w(), pos3DMaps[im]->h()));
			(this->*renderFunc)(*_cams->inputCameras()[im], pos3DMaps[im], normalMaps[im], imgRender);
			//Saving
			imgF.fromOpenCV(imgRender->toOpenCV());
			encodeImLog(imgF);
			sibr::ImageRGB16 toSave;
			imgF.toOpenCV().convertTo(toSave.toOpenCVnonConst(), CV_16UC3, 65535.0f);
			bool written = cv::imwrite(_args.dataset_path.get() + "/16bitData/" + _lighting.name + "/" + std::to_string(im) + "_" + suffix + ".png", toSave.toOpenCVBGR());
			if (!written) {
				SIBR_ERR << "Image could not be written";
			}
			vecIm[im] = imgRender;
		}

		return vecIm;

	}

	std::vector<sibr::ImageRGB32F::Ptr> sibr::indoorRelighterIBRScene::getOrComputeRadData(
		std::vector<sibr::ImageFloat1::Ptr> depthMaps,
		std::vector<sibr::ImageRGB32F::Ptr> normalMaps
	)
	{
		std::vector<sibr::ImageRGB32F::Ptr> vecIm(_cams->inputCameras().size());
		std::vector<int> imToCompute;
		//Load in parallel
#pragma omp parallel for
		for (int im = 0; im < _cams->inputCameras().size(); im++) {
			sibr::ImageRGB32F imgF;
			if (imgF.load(_args.dataset_path.get() + "/16bitData/" + _lighting.name + "/" + std::to_string(im) + "_rad.png", false, false) &&
				imgF.w() >= _args.texture_width) {
				sibr::ImageRGB32F::Ptr imgRender(new sibr::ImageRGB32F(imgF.w(), imgF.h()));
				//imgF.toOpenCV().convertTo(imgSpec->toOpenCVnonConst(), CV_16UC3, 65535.0f);
				decodeImLog(imgF);
				imgRender->fromOpenCV(imgF.resized(depthMaps[im]->w(), depthMaps[im]->h(), cv::INTER_AREA).toOpenCV());
				vecIm[im] = imgRender;
			}
			else {
#pragma omp critical
				{
					imToCompute.push_back(im);
				}
			}
		}


		if (!_args.synthetic) {
			detectLightSources(1.0f);

			std::cout << "Found " << _lights.size() << " burnt light sources in the scene" << std::endl;

			sibr::Mesh lights;
			for (auto& l : _lights) {
				std::cout << l.xyz() << "  " << l.w() << std::endl;
				lights.merge(*sibr::Mesh::getSphereMesh(l.xyz(), l.w()));
			}
			if (lights.vertices().size() > 0)
				lights.save(_args.dataset_path.get() + "/lights.ply");

		}

		if (imToCompute.size() > 0) {

			std::map<uint, std::map<int, sibr::ImageRGB32F::Ptr>> radImLight;
			std::map<uint, std::map<int, sibr::ImageRGB32F::Ptr>> visibilityLight;
			struct patchCoord {
				int im;
				sibr::Vector2i pc;
			};
			std::vector<patchCoord> patchCoords;
			std::map<int, sibr::ImageRGB32F::Ptr> validityMasks;
			std::map<int, sibr::ImageRGB32F> imsResized;

			int hk = 8;
			int count = 0;

			std::string tempFolder = "/tempRad/" + _lighting.name + "/";
			sibr::makeDirectory(_args.dataset_path.get() + "/tempRad/");

			for (int im : imToCompute) {

				std::cout << "Computing " << "rad" << " num " << im << " - " << (100 * count) / imToCompute.size() << "%" << std::endl;
				count++;
				//Render the indirect illumination radiance as id -1
				for (int li = -1; li < (int)_lights.size(); li++) {
					std::cout << "    light " << li << std::endl;
					sibr::ImageRGB32F::Ptr imgRender(new sibr::ImageRGB32F());
					if (!imgRender->load(_args.dataset_path.get() + tempFolder + std::to_string(im) + "_" + std::to_string(li + 1) + ".exr", false, false)
						|| imgRender->w() < _args.texture_width)
					{
						imgRender = sibr::ImageRGB32F::Ptr(new sibr::ImageRGB32F(depthMaps[im]->w(), depthMaps[im]->h()));
						renderRadianceIm(*_cams->inputCameras()[im], depthMaps[im], normalMaps[im], imgRender, li);
						sibr::makeDirectory(_args.dataset_path.get() + tempFolder);
						imgRender->saveHDR(_args.dataset_path.get() + tempFolder + std::to_string(im) + "_" + std::to_string(li + 1) + ".exr");
					}
					radImLight[im][li] = imgRender;
					sibr::ImageRGB32F::Ptr visLight(new sibr::ImageRGB32F(depthMaps[im]->w(), depthMaps[im]->h(), sibr::Vector3f(0, 0, 0)));
					visibilityLight[im][li] = visLight;
				}

				if (_args.synthetic) {

					for (int l = -1; l < (int)_lights.size(); l++) {
						radImLight[im][l] = _denoiser->denoise(*radImLight[im][l]);
					}
					sibr::ImageRGB32F::Ptr imgRender = radImLight[im][-1];
					sibr::ImageRGB32F imgF;
					imgF.fromOpenCV(imgRender->toOpenCV());
					//Saving
					encodeImLog(imgF);
					sibr::ImageRGB16 toSave;
					imgF.toOpenCV().convertTo(toSave.toOpenCVnonConst(), CV_16UC3, 65535.0f);
					bool written = cv::imwrite(_args.dataset_path.get() + "/16bitData/" + _lighting.name + "/" + std::to_string(im) + "_rad.png", toSave.toOpenCVBGR());
					if (!written) {
						SIBR_ERR << "Image could not be written";
					}
					vecIm[im] = imgRender;
					continue;
				}
				/*
				//Validity mask that takes into account camera deformation and burnt pixels
				sibr::ImageInt1 labels = _labelsIm[im]->resized(pos3DMaps[im]->w(), pos3DMaps[im]->h(), cv::INTER_NEAREST);
				imsResized[im] = _linDiffs[im]->resized(pos3DMaps[im]->w(), pos3DMaps[im]->h());

				sibr::ImageL32F stdLum(pos3DMaps[im]->w(), pos3DMaps[im]->h(), 0.0f);
				sibr::ImageL32F locality(pos3DMaps[im]->w(), pos3DMaps[im]->h(), 0.0f);
				sibr::ImageL32F stdRad(pos3DMaps[im]->w(), pos3DMaps[im]->h(), 0.0f);

				sibr::ImageRGB32F::Ptr weight(
					new sibr::ImageRGB32F(pos3DMaps[im]->w(), pos3DMaps[im]->h(), sibr::Vector3f(0, 0, 0))
				);

				std::vector<float> ws;

#pragma omp parallel for
				for (int j = 0; j < pos3DMaps[im]->h(); j++) {
					for (int i = 0; i < pos3DMaps[im]->w(); i++) {

						for (int li = -1; li < (int)_lights.size(); li++) {
							if (radImLight[im][li](i, j).x() >= 0.000001f) {
								visibilityLight[im][li](i, j) = sibr::Vector3f(1, 1, 1);
							}
						}

						std::vector<sibr::Vector3f> valsPos;

						float zeroFound = 0;
						float valFound = 0;

						bool valid = true;
						for (int jj = -hk; jj <= hk; jj++) {
							for (int ii = -hk; ii <= hk; ii++) {

								if (pos3DMaps[im]->isInRange(i + ii, j + jj)) {
									if (pos3DMaps[im](i + ii, j + jj).norm() != 0 && labels(i + ii, j + jj).x() < 0) {
										valsPos.push_back(pos3DMaps[im](i + ii, j + jj));

										if (radImLight[im][0](i + ii, j + jj).x() < 0.000001f) {
											zeroFound++;
										}
										else {
											valFound++;
										}
									}
									else {
										valid = false;
									}
								}
								else {
									valid = false;
								}


							}
						}

						if (valid) {
							if (zeroFound > 0 && valFound > 0) {

								stdRad(i, j).x() = exp(-pow(log2(zeroFound / valFound) / 2.0, 2));

								sibr::Vector3f sumPos = std::accumulate(valsPos.begin(), valsPos.end(), sibr::Vector3f(0, 0, 0));
								sibr::Vector3f meanPos = sumPos / valsPos.size();
								float distNormFactor = (meanPos - _cams->inputCameras()[im].position()).squaredNorm();
								double accdevPos = 0;
								for (const sibr::Vector3f& p : valsPos) {
									accdevPos += (p - meanPos).squaredNorm() / distNormFactor;
								}
								double varPos = accdevPos / valsPos.size();

								locality(i, j).x() = exp(-varPos / 0.001f);

								float w = stdRad(i, j).x() * locality(i, j).x();

								weight(i, j) = sibr::Vector3f(w, w, w);

#pragma omp critical
								{
									ws.push_back(w);
								}

							}

						}

					}
				}

				//showFloat(stdRad);
				//showFloat(locality);
				//show(*weight);

				std::sort(ws.begin(), ws.end());

				float pc50 = ws[ws.size() / 2];
				for (int j = hk; j < pos3DMaps[im]->h() - hk; j++) {
					for (int i = hk; i < pos3DMaps[im]->w() - hk; i++) {
						if (weight(i, j).x() >= pc50) {
							patchCoords.push_back(patchCoord({ im,sibr::Vector2i(i,j) }));
						}
					}
				}

				validityMasks[im] = weight;*/
			}

			if (_args.synthetic) {
				return vecIm;
			}

			std::vector<ImVec4> colors(_lights.size(), ImVec4(1, 1, 1, 1));
			std::vector<float> exposures(_lights.size(), 1.0f);
			std::vector<float> temps(_lights.size(), 6600.0f);
			bool needClose = false;
			int im = 0;
			int dispMode = 0;
			int editLight = 0;

			sibr::GLShader quadShader;
			quadShader.init("Texture",
				sibr::loadFile(sibr::Resources::Instance()->getResourceFilePathName("texture.vp")),
				sibr::loadFile(sibr::Resources::Instance()->getResourceFilePathName("texture.fp")));


			sibr::ImageRGB32F albedo;
			sibr::ImageRGB32F rad;
			sibr::Texture2DRGB32F::Ptr imTex(std::make_shared<sibr::Texture2DRGB32F>(sibr::ImageRGB32F()));

			if (!sibr::fileExists(_args.dataset_path.get() + "/light_exposures.txt") ||
				!sibr::fileExists(_args.dataset_path.get() + "/light_colors.txt"))
			{
				while (!needClose) {

					sibr::Input::poll();
					_window->makeContextCurrent();


					ImGui::Begin("Lighting adjustment");
					bool change = false;
					if (ImGui::SliderInt("Im", &im, 0, _cams->inputCameras().size() - 1))
						change = true;
					if (ImGui::SliderInt("Light", &editLight, 0, _lights.size() - 1)) {
						change = true;
					}
					if (ImGui::SliderFloat("Exposure", &exposures[editLight], 0, 15.0f))
						change = true;
					if (ImGui::SliderFloat("Temp", &temps[editLight], 1000.0f, 10000.0f)) {
						colors[editLight].x = tempToRGB(temps[editLight]).x();
						colors[editLight].y = tempToRGB(temps[editLight]).y();
						colors[editLight].z = tempToRGB(temps[editLight]).z();
						change = true;
					}
					if (ImGui::ColorEdit3("light Color", &colors[editLight].x)) {
						change = true;
					}
					ImGui::SliderInt("Display Mode", &dispMode, 0, 1);
					ImGui::End();


					if (sibr::Input::global().key().isPressed(sibr::Key::Escape))
						needClose = true;

					int w = radImLight[im][-1]->w();
					int h = radImLight[im][-1]->h();

					if (change) {
						rad = radImLight[im][-1]->clone();

						for (int l = 0; l < _lights.size(); l++) {

							//sibr::Vector3f tempSibr = tempToRGB(colorTemps[l]);
							cv::Scalar tempColor(colors[l].x, colors[l].y, colors[l].z);
							rad.fromOpenCV(rad.toOpenCV() + pow(2.0f, exposures[l]) * radImLight[im][l]->toOpenCV().mul(tempColor));

						}

						rad = _denoiser->denoise(rad)->clone();
						_window->size(w + 512, std::max(512,h));

						albedo.fromOpenCV(
							(_linDiffs[im]->resized(w, h).toOpenCV()) /
							(rad.toOpenCV() + 1e-7)
						);

						albedo.flipH();
						imTex->update(albedo);
						if (dispMode == 1) {
							rad.flipH();
							imTex->update(rad);
						}

					}

					sibr::Viewport vp(0, 0, rad.w(), rad.h());
					glClearColor(0.3f, 0.1f, 0.1f, 1.0f);
					_window->bind();
					_window->clear();

					glDisable(GL_DEPTH_TEST);

					quadShader.begin();
					glActiveTexture(GL_TEXTURE0);
					vp.bind();
					glBindTexture(GL_TEXTURE_2D, imTex->handle());
					sibr::RenderUtility::renderScreenQuad();
					glFinish();
					quadShader.end();


					_window->swapBuffer();

				}

				std::ofstream fileExp;
				fileExp.open(_args.dataset_path.get() + "/light_exposures.txt");
				std::ofstream fileTemp;
				fileTemp.open(_args.dataset_path.get() + "/light_colors.txt");
				for (int l = 0; l < _lights.size(); l++) {
					fileExp << exposures[l] << "\n";
					fileTemp << colors[l].x << " " << colors[l].y << " " << colors[l].z << "\n";
				}
				fileExp.close();
				fileTemp.close();

			}
			else {
				//Now we replace burnt pixels by the light value
				std::fstream fileExp(_args.dataset_path.get() + "/light_exposures.txt");
				float exp;
				for (int l = 0; l < _lights.size(); l++) {
					fileExp >> exp;
					exposures[l] = exp;
				}

				std::fstream fileColor(_args.dataset_path.get() + "/light_colors.txt");
				ImVec4 color;
				for (int l = 0; l < _lights.size(); l++) {
					fileColor >> color.x >> color.y >> color.z;
					colors[l] = color;
				}

			}


			for (int im : imToCompute) {

				rad = radImLight[im][-1]->clone();
				for (int l = 0; l < _lights.size(); l++) {

					cv::Scalar tempColor(colors[l].x, colors[l].y, colors[l].z);
					rad.fromOpenCV(rad.toOpenCV() + pow(2.0f, exposures[l]) * radImLight[im][l]->toOpenCV().mul(tempColor));

				}
				rad = _denoiser->denoise(rad)->clone();


				sibr::ImageRGB32F::Ptr imgRender(new sibr::ImageRGB32F(rad.clone()));
				sibr::ImageRGB32F imgF;
				imgF.fromOpenCV(imgRender->toOpenCV());
				//Saving
				encodeImLog(imgF);
				sibr::ImageRGB16 toSave;
				imgF.toOpenCV().convertTo(toSave.toOpenCVnonConst(), CV_16UC3, 65535.0f);
				bool written = cv::imwrite(_args.dataset_path.get() + "/16bitData/" + _lighting.name + "/" + std::to_string(im) + "_rad.png", toSave.toOpenCVBGR());
				if (!written) {
					SIBR_ERR << "Image could not be written";
				}
				vecIm[im] = imgRender;

			}

			/*optim myOptim(_lights.size());
			sgd mySgd(myOptim, _lights.size());

			//RUN THE OPTIMIZATION
			std::cout << "Before " << myOptim.alphas << std::endl;
			std::tuple<torch::Tensor, torch::Tensor> out;
			for (int it = 0; it < 10000; it++) {

				int dPatch = (2 * hk + 1);
				int bS = 64;

				//Construct empty vectors
				torch::Tensor I = torch::zeros({ bS,3,dPatch,dPatch });
				torch::Tensor Re = torch::zeros({ bS,3,dPatch,dPatch });
				torch::Tensor Rls = torch::zeros({ bS,(int)_lights.size(),3,dPatch,dPatch });
				torch::Tensor VisLs = torch::zeros({ bS,(int)_lights.size(),3,dPatch,dPatch });

				std::random_device rd;
				std::mt19937 mt(rd());
				std::uniform_int_distribution<int> dist(0, patchCoords.size() - 1);

				std::vector<std::future<void>> results;
				for (int be = 0; be < bS; be++) {

					results.push_back(_pool.push(
						[&, be](int id) {
							int pc = dist(mt);
							int i = patchCoords[pc].pc.x();
							int j = patchCoords[pc].pc.y();
							int im = patchCoords[pc].im;
							cv::Rect rect(i - hk, j - hk, 2 * hk + 1, 2 * hk + 1);
							if (i < hk) {
								std::cout << pc << std::endl;
							}

							I.slice(0, be, be + 1) = cvMatToTensor(imsResized[im].toOpenCV()(rect));
							Re.slice(0, be, be + 1) = cvMatToTensor(radImLight[im][-1]->toOpenCV()(rect));
							for (int li = 0; li < _lights.size(); li++) {
								Rls.slice(0, be, be + 1).slice(1, li, li + 1) = cvMatToTensor(radImLight[im][li]->toOpenCV()(rect)).view({ 1,1, 3,dPatch,dPatch });
								VisLs.slice(0, be, be + 1).slice(1, li, li + 1) = cvMatToTensor(visibilityLight[im][li]->toOpenCV()(rect)).view({ 1,1, 3,dPatch,dPatch });
							}
							pc++;
						}));
				}
				for (int be = 0; be < bS; be++) {
					results[be].get();
				}

				mySgd.step(I, Re, Rls, VisLs, out);
			}
			std::cout << "After " << myOptim.alphas << std::endl;

			for (int im : imToCompute) {

				for (int l = -1; l < (int)_lights.size(); l++) {
					radImLight[im][l] = _denoiser->denoise(*radImLight[im][l]);
				}


				sibr::ImageRGB32F imResized = _linDiffs[im]->resized(pos3DMaps[im]->w(), pos3DMaps[im]->h());
				torch::Tensor imResizedTs = imToTensor(imResized);
				torch::Tensor ReTs = imToTensor(*radImLight[im][-1]);
				torch::Tensor RlTs = imToTensor(*radImLight[im][0]).view({ 1,1,3,ReTs.sizes()[2],ReTs.sizes()[3] });
				for (int l = 1; l < (int)_lights.size(); l++) {
					RlTs = torch::cat({
						RlTs,
						imToTensor(*radImLight[im][l]).view({1,1,3,ReTs.sizes()[2],ReTs.sizes()[3]}) }
					, 1);
				}

				auto outFinal = myOptim.forward(imResizedTs, ReTs, RlTs);

				sibr::ImageRGB32F::Ptr imgRender = std::make_shared<sibr::ImageRGB32F>(tensorToIm(std::get<1>(outFinal)));
				sibr::ImageRGB32F imgF;

				imgF.fromOpenCV(imgRender->toOpenCV());
				//Saving
				encodeImLog(imgF);
				sibr::ImageRGB16 toSave;
				imgF.toOpenCV().convertTo(toSave.toOpenCVnonConst(), CV_16UC3, 65535.0f);
				bool written = cv::imwrite(_args.dataset_path.get() + "/16bitData/" + _lighting.name + "/" + std::to_string(im) + "_rad.png", toSave.toOpenCVBGR());
				if (!written) {
					SIBR_ERR << "Image could not be written";
				}
				vecIm[im] = imgRender;
			}*/

		}

		if (!_args.synthetic) {
			//Now we replace burnt pixels by the light value
			std::vector<sibr::Vector3f> colors;
			std::vector<float> exposures;

			std::fstream fileExp(_args.dataset_path.get() + "/light_exposures.txt");
			float exp;
			for (int l = 0; l < _lights.size(); l++) {
				fileExp >> exp;
				exposures.push_back(exp);
			}

			std::fstream fileColor(_args.dataset_path.get() + "/light_colors.txt");
			sibr::Vector3f color;
			for (int l = 0; l < _lights.size(); l++) {
				fileColor >> color[0] >> color[1] >> color[2];
				colors.push_back(color);
			}


			for (int im = 0; im < cameras()->inputCameras().size(); im++) {

				sibr::InputCamera cam = *cameras()->inputCameras()[im];
				int h = _linDiffs[im]->h();
				int w = _linDiffs[im]->w();

				for (int j = 0; j < _linDiffs[im]->h(); j++) {
					for (int i = 0; i < _linDiffs[im]->w(); i++) {
						int& label = _labelsIm[im](i, j).x();
						if (label >= 0) {

							_linDiffs[im](i, j) = pow(2.0, exposures[label]) * colors[label];

						}
					}
				}

			}
		}


		return vecIm;

	}

	std::vector<std::vector<sibr::ImageRGB32F::Ptr>> sibr::indoorRelighterIBRScene::getDataMultiScale(
		void (sibr::indoorRelighterIBRScene::* renderFunc)(
			const Camera& cam,
			const sibr::ImageFloat1::Ptr,
			const sibr::ImageRGB32F::Ptr,
			std::vector<sibr::ImageRGB32F::Ptr>&,
			float
			),
		std::string suffix,
		std::vector<sibr::ImageFloat1::Ptr> depthMaps,
		std::vector<sibr::ImageRGB32F::Ptr> normalMaps
	)
	{
		std::vector<std::vector<sibr::ImageRGB32F::Ptr>> vecIm(_cams->inputCameras().size());

		vecIm[0] = std::vector<sibr::ImageRGB32F::Ptr>(_cams->inputCameras().size());
		vecIm[1] = std::vector<sibr::ImageRGB32F::Ptr>(_cams->inputCameras().size());
		vecIm[2] = std::vector<sibr::ImageRGB32F::Ptr>(_cams->inputCameras().size());

		std::set<int> imToCompute;
		for (int s = 0; s < 3; s++)
		{
			//Load in parallel
#pragma omp parallel for
			for (int im = 0; im < _cams->inputCameras().size(); im++) {
				sibr::ImageRGB32F imgF;
				if (
					imgF.load(_args.dataset_path.get() + "/16bitData/" + _lighting.name + "/" + std::to_string(im) + "_" + suffix + "_s" + std::to_string(s) + ".png", false, false) &&
					imgF.w() >= _args.texture_width
					) {
					sibr::ImageRGB32F::Ptr imgRender(new sibr::ImageRGB32F(depthMaps[im]->w(), depthMaps[im]->h()));
					//imgF.toOpenCV().convertTo(imgSpec->toOpenCVnonConst(), CV_16UC3, 65535.0f);
					decodeImLog(imgF);
					imgRender->fromOpenCV(imgF.resized(depthMaps[im]->w(), depthMaps[im]->h(), cv::INTER_AREA).toOpenCV());
					vecIm[s][im] = imgRender;
				}
				else {
#pragma omp critical
					{
						imToCompute.insert(im);
					}
				}
			}

		}

		//Compute the missing ones
		for (int im : imToCompute) {
			sibr::ImageRGB32F imgF;
			sibr::ImageRGB16 img16;
			std::vector<sibr::ImageRGB32F::Ptr> imgRender(3);
			for (int s = 0; s < 3; s++) {
				imgRender[s] = std::make_shared<sibr::ImageRGB32F>(depthMaps[im]->w(), depthMaps[im]->h());
			}
			(this->*renderFunc)(*_cams->inputCameras()[im], depthMaps[im], normalMaps[im], imgRender, 1.0);
			//Saving


			for (int s = 0; s < 3; s++) {
				imgF.fromOpenCV(imgRender[s]->toOpenCV());
				encodeImLog(imgF);
				sibr::ImageRGB16 toSave;
				imgF.toOpenCV().convertTo(toSave.toOpenCVnonConst(), CV_16UC3, 65535.0f);
				cv::imwrite(_args.dataset_path.get() + "/16bitData/" + _lighting.name + "/" + std::to_string(im) + "_" + suffix + "_s" + std::to_string(s) + ".png", toSave.toOpenCVBGR());
				vecIm[s][im] = imgRender[s];
			}
		}

		return vecIm;

	}

	void sibr::indoorRelighterIBRScene::initFaceVisibility() {
		_faceVisibility =
			std::vector<std::vector<bool>>(
				_proxies->proxy().triangles().size(),
				std::vector<bool>(_cams->inputCameras().size(), false)
				);

		if (!_dmInit) {
			initDM();
		}

		std::vector<std::future<void>> results;
		for (int f = 0; f < _proxies->proxy().triangles().size(); f++) {

			results.push_back(_pool.push(
				[&, f](int id) {
					sibr::Vector3u tri = _proxies->proxy().triangles()[f];
					std::vector<sibr::Vector3f> points;
					points.push_back(_proxies->proxy().vertices()[tri[0]]);
					points.push_back(_proxies->proxy().vertices()[tri[1]]);
					points.push_back(_proxies->proxy().vertices()[tri[2]]);
					vibilityTest(
						_cams->inputCameras(),
						points,
						_nfPrecom,
						_faceVisibility[f]);
				}));
		}
		for (int f = 0; f < _proxies->proxy().triangles().size(); f++) {
			results[f].get();
		}

	}

	void sibr::indoorRelighterIBRScene::initCamsOrder() {
		std::cout << "Initializing per face priority ... ";
		_camsOrder =
			std::vector<std::vector<int>>(
				_proxies->proxy().triangles().size(),
				std::vector<int>()
				);

		if (!_dmInit) {
			initDM();
		}

		const auto& cams = cameras()->inputCameras();

		std::vector<std::future<void>> results;
		for (int f = 0; f < _proxies->proxy().triangles().size(); f++) {

			results.push_back(_pool.push(
				[&, f](int id) {


					std::map<int, float> score;

					sibr::Vector3u tri = _proxies->proxy().triangles()[f];
					sibr::Vector3f normal =
						(_proxies->proxy().normals()[tri[0]] +
							_proxies->proxy().normals()[tri[0]] +
							_proxies->proxy().normals()[tri[0]]).normalized();

					sibr::Vector3f point =
						(_proxies->proxy().vertices()[tri[0]] +
							_proxies->proxy().vertices()[tri[0]] +
							_proxies->proxy().vertices()[tri[0]]) / 3.0f;

					for (int cam = 0; cam < cams.size(); cam++) {

						sibr::Vector3f proj = cams[cam]->project(point);

						if (

							abs(proj.x()) < 0.99 &&
							abs(proj.y()) < 0.99
							) {

							float w = -normal.dot((point - cams[cam]->position()).normalized());
							score[cam] = w;
							_camsOrder[f].push_back(cam);
						}
					}

					std::sort(_camsOrder[f].begin(), _camsOrder[f].end(),
						[&](int a, int b) -> bool {
							return score[a] > score[b];
						});


				}));
		}
		for (int f = 0; f < _proxies->proxy().triangles().size(); f++) {
			results[f].get();
		}
		std::cout << " Done" << std::endl;

	}

	bool sibr::indoorRelighterIBRScene::setupRenderer()
	{

		_TexturesRGB = std::make_shared<sibr::Texture2DArrayRGB32F>();
		_TexturesDm = std::make_shared<sibr::Texture2DArrayLum32F>();
		_TexturesOcclusion = std::make_shared<sibr::Texture2DArrayLum32F>();
		_TexturesNormals = std::make_shared<sibr::Texture2DArrayRGB32F>();
		_TexturesSpec = std::make_shared<sibr::Texture2DArrayRGB32F>();
		_TexturesRadiance = std::make_shared<sibr::Texture2DArrayRGB32F>();
		_TexturesRadianceAdd = std::make_shared<sibr::Texture2DArrayRGB32F>();

		std::vector<sibr::ImageL32F> imData1;
		std::vector<sibr::ImageRGB32F> imData3;

		sibr::PosNormalRenderer PNR;
		for (int im = 0; im < _cams->inputCameras().size(); im++)
		{
			int w = _args.texture_width;
			int h = w * _cams->inputCameras()[im]->h() / _cams->inputCameras()[im]->w();

			sibr::ImageRGB32F::Ptr composed(new sibr::ImageRGB32F());
			composed->fromOpenCV(_linDiffs[im]->toOpenCV() + _linVdeps[im]->toOpenCV());
			sibr::ImageFloat1 mask(composed->w(), composed->h(), 0);

#pragma omp parallel for
			for (int j = 0; j < composed->h(); j++) {
				for (int i = 0; i < composed->w(); i++) {
					if (composed(i, j).x() == 0 && composed(i, j).y() == 0 && composed(i, j).z() == 0) {
						mask(i, j).x() = 1.0;
					}
				}
			}

			composed->fromOpenCV(composed->resized(w, h).toOpenCV());
			mask = mask.resized(w, h, cv::INTER_AREA);

			sibr::RenderTargetRGB32F posNormalRT(w, h, SIBR_GPU_LINEAR_SAMPLING, 2);
			//First we render in UV space the 3D position and normals
			glClearColor(0.0, 0.0, 0.0, 1.0);
			glViewport((GLint)(0), (GLint)(0), (GLsizei)(w), (GLsizei)(h));
			PNR.process(*_proxies->proxyPtr(), *_cams->inputCameras()[im], posNormalRT);

			sibr::ImageRGB32F::Ptr posM(new sibr::ImageRGB32F(w, h, sibr::Vector3f(0, 0, 0)));
			sibr::ImageRGB32F::Ptr normalM(new sibr::ImageRGB32F(w, h, sibr::Vector3f(0, 0, 0)));

			posNormalRT.readBack(*posM, 0);
			posNormalRT.readBack(*normalM, 1);

			//show(*normalM);

			sibr::ImageFloat1::Ptr dm(new sibr::ImageFloat1(w, h, -2.0));
			sibr::DepthRenderer DR(w, h);
			DR.render(*_cams->inputCameras()[im], _proxies->proxy());
			DR._depth_RT->readBack(*dm);

			if (!_synthetic) {

#pragma omp parallel for
				for (int j = 0; j < posM->h(); j++) {
					for (int i = 0; i < posM->w(); i++) {
						if (mask(i, j).x() != 0) {
							posM(i, j) = sibr::Vector3f(0, 0, 0);
							dm(i, j).x() = -2.0; //-2 for undefined depth
						}
					}
				}
			}


			//Compute a map with an occlusion score, when the 3d position gradient is big, we are probably at an occlusion, and should not sample these pixels.
			sibr::ImageL32F::Ptr laplacianDist(new sibr::ImageL32F(posM->w(), posM->h(), 0.0f));
#pragma omp parallel for
			for (int j = 1; j < laplacianDist->h() - 1; j++) {
				for (int i = 1; i < laplacianDist->w() - 1; i++) {
					laplacianDist(i, j).x() =
						(posM(i, j) - posM(i - 1, j)).norm() +
						(posM(i, j) - posM(i + 1, j)).norm() +
						(posM(i, j) - posM(i, j + 1)).norm() +
						(posM(i, j) - posM(i, j - 1)).norm();
				}
			}

			cv::dilate(
				laplacianDist->toOpenCV(),
				laplacianDist->toOpenCVnonConst(),
				cv::getStructuringElement(cv::MORPH_ELLIPSE, cv::Size(7, 7))
			);

			//showFloat(laplacianDist,false,0,0.4*_scale);

			_dmTex.push_back(dm);
			_normals.push_back(normalM);
			_occs.push_back(laplacianDist);

		}

		for (auto& im : _occs) {
			imData1.push_back(im->clone());
		}
		_TexturesOcclusion->createCompressedFromImages(imData1, GL_COMPRESSED_RGB_BPTC_SIGNED_FLOAT_ARB);
		imData1.clear();

		for (auto& im : _dmTex) {
			imData1.push_back(im->clone());
		}
		_TexturesDm->createFromImages(imData1);// , SIBR_GPU_LINEAR_SAMPLING);
		imData1.clear();

		for (auto& im : _normals) {
			imData3.push_back(im->clone());
		}
		_TexturesNormals->createCompressedFromImages(imData3, GL_COMPRESSED_RGB_BPTC_SIGNED_FLOAT_ARB);
		imData3.clear();

		//Compute or get the radiance images
		std::cout << "Getting or computing radiance Images " << std::endl;
		std::vector<sibr::ImageRGB32F::Ptr> linImgsRad = getOrComputeRadData(_dmTex, _normals);
		for (auto& im : linImgsRad) {
			imData3.push_back(im->clone());
		}
		_TexturesRadiance->createCompressedFromImages(imData3, GL_COMPRESSED_RGB_BPTC_SIGNED_FLOAT_ARB);
		imData3.clear();

		for (auto& im : linImgsRad) {
			_linRadAdds.push_back(sibr::ImageRGB32F(im->w(), im->h(), sibr::Vector3f(0, 0, 0)));
			_linRadRms.push_back(sibr::ImageRGB32F(im->w(), im->h(), sibr::Vector3f(0, 0, 0)));
		}
		//_TexturesRadianceAdd->createCompressedFromImages(_linRadAdds, GL_COMPRESSED_RGB_BPTC_SIGNED_FLOAT_ARB);
		_TexturesRadianceAdd->createFromImages(_linRadAdds, SIBR_GPU_AUTOGEN_MIPMAP);

		//Upload the RGB after the radiance to account for radiance modification
		for (int im = 0; im < _cams->inputCameras().size(); im++)
		{
			int w = _args.texture_width;
			int h = w * _cams->inputCameras()[im]->h() / _cams->inputCameras()[im]->w();

			//Upload the lin images
			sibr::ImageRGB32F::Ptr composed(new sibr::ImageRGB32F());
			composed->fromOpenCV(_linDiffs[im]->toOpenCV() + _linVdeps[im]->toOpenCV());
			sibr::ImageFloat1 mask(composed->w(), composed->h(), 0);

			composed->fromOpenCV(composed->resized(w, h).toOpenCV());
			imData3.push_back(composed->clone());
		}
		_TexturesRGB->createCompressedFromImages(imData3, GL_COMPRESSED_RGB_BPTC_SIGNED_FLOAT_ARB);
		imData3.clear();


		//Compute or get the mirror images
		std::cout << "Getting or computing mirror Images " << std::endl;
		std::vector<std::vector<sibr::ImageRGB32F::Ptr>> linImgsSpec = getDataMultiScale(
			&sibr::indoorRelighterIBRScene::renderReflexionIm, "spec", _dmTex, _normals);
		for (int s = 0; s < 1; s++) {
			for (auto& im : linImgsSpec[s]) {
				imData3.push_back(im->clone());
			}
			_TexturesSpec->createCompressedFromImages(imData3, GL_COMPRESSED_RGB_BPTC_SIGNED_FLOAT_ARB, SIBR_GPU_LINEAR_SAMPLING);
			imData3.clear();
		}


		//Compute or get the albedo images
		std::cout << "Getting or computing albedo Images " << std::endl;

		_linImgsAlbedo = std::vector<sibr::ImageRGB32F::Ptr>(_cams->inputCameras().size());
#pragma omp parallel for
		for (int im = 0; im < _cams->inputCameras().size(); im++) {
			sibr::ImageRGB32F imgF;
			if (imgF.load(_args.dataset_path.get() + "/16bitData/" + _lighting.name + "/" + std::to_string(im) + "_" + "albedo" + ".png", false)
				) {
				sibr::ImageRGB32F::Ptr imgRender(new sibr::ImageRGB32F(imgF.w(), imgF.h()));
				imgRender->fromOpenCV(imgF.toOpenCV());
				_linImgsAlbedo[im] = imgRender;
			}
			else {
				/*	std::cout << imgF.w();
					std::cout << "Rendering albedo for im " << im << std::endl;
					_OR->process(_cams->inputCameras()[im]);
					sibr::ImageRGB32F::Ptr imgRender(new sibr::ImageRGB32F(_pos3D[im]->w(), _pos3D[im]->h()));
					_OR->_radAlbedo->readBack(*imgRender, 1);

					int w = _args.texture_width;
					int h = w * _cams->inputCameras()[im].h() / _cams->inputCameras()[im].w();
					*imgRender = imgRender->resized(w, h);*/

				int w = _args.texture_width;
				int h = w * _cams->inputCameras()[im]->h() / _cams->inputCameras()[im]->w();

				sibr::ImageRGB32F::Ptr imgRender(new sibr::ImageRGB32F(_dmTex[im]->w(), _dmTex[im]->h()));
				imgRender->fromOpenCV(
					(_linDiffs[im]->resized(w, h).toOpenCV() + _linVdeps[im]->resized(w, h).toOpenCV()) /
					(linImgsRad[im]->toOpenCV() + 1e-7)
				);
				_linImgsAlbedo[im] = imgRender;
				sibr::ImageRGB16 img16;
				//Saving
				imgF.fromOpenCV(imgRender->toOpenCV());
				sibr::ImageRGB16 toSave;
				imgF.toOpenCV().convertTo(toSave.toOpenCVnonConst(), CV_16UC3, 65535.0f);
				cv::imwrite(_args.dataset_path.get() + "/16bitData/" + _lighting.name + "/" + std::to_string(im) + "_" + "albedo" + ".png", toSave.toOpenCVBGR());

			}
		}

		exportMesh2Mitsuba();

		//Compute or get the light radiance images
		if (_args.genTrainData && !_args.preprocessOnly) {
			if (_lighting.type == "sphere") {
				std::cout << "Getting light radiance Images " << std::endl;
				if (_args.gtGeom) {
					for (auto& im : linImgsRad) {
						imData3.push_back(im->clone());
					}
				}
				else {
					for (int im = 0; im < cameras()->inputCameras().size(); im++) {

						sibr::ImageRGB32F::Ptr imgIrdc(new sibr::ImageRGB32F());

						bool loaded = imFrom16BitOrRender(imgIrdc, im, "_irdc", "_irdc");
						if (!loaded)
							return false;

						imData3.push_back(imgIrdc->clone());
					}
				}
				_TexturesRadianceAdd->updateFromImages(imData3);
				imData3.clear();
			}
		}

		if (!_args.preprocessOnly) {
			_OR = std::make_shared<sibr::OrderRenderer>(_width, _height,
				_cams->inputCameras(),
				_proxies->proxyPtr(),
				_TexturesRGB,
				_TexturesSpec,
				_TexturesDm,
				_TexturesNormals,
				_TexturesRadiance,
				_TexturesRadianceAdd,
				_TexturesOcclusion,
				_scale
				);
		}

		return true;

	}


	sibr::Mesh sibr::indoorRelighterIBRScene::loadGTMesh()
	{
		//We need the GT mesh for the flow
		sibr::Mesh GTMesh;
		// load proxy
		GTMesh.loadMtsXML(_args.dataset_path.get() + "/mitsuba/scene.xml");
		if (_lighting.type == "sphere") {
			sibr::Mesh::Ptr lightMesh = sibr::Mesh::getSphereMesh(_lighting.pos, _lighting.radius, false, 72);
			lightMesh->makeWhole();
			lightMesh->generateSmoothNormals(1);
			GTMesh.merge(*lightMesh);
		}
		else if (_lighting.type == "default") {
			sibr::Vector3f center;
			float radius;
			GTMesh.getBoundingSphere(center, radius, true);
			sibr::Mesh::Ptr envMesh = sibr::Mesh::getSphereMesh(center, 2.0 * radius, false, 72);
			envMesh->makeWhole();
			GTMesh.merge(*envMesh);
		}

		return GTMesh;

	}

	void sibr::indoorRelighterIBRScene::generateDataset()
	{

		std::cout << "Generating data for Image: ";
		std::string trainDirName = "/train_" + std::to_string(_width) + "x" + std::to_string(_height) + "/";
		sibr::makeDirectory(_args.dataset_path.get() + trainDirName + _lighting.name + "/");
		sibr::makeDirectory(_args.dataset_path.get() + trainDirName + "/indep/");

		sibr::Mesh GTMesh = loadGTMesh();
		sibr::PosNormalRenderer PNR;


		int globalNum = 0;
		std::vector<std::future<void>> results;
		std::map<int, std::vector<std::string>> imNameSce;
		std::map<int, std::vector<std::string>> imNameTar;
		std::map<int, std::vector<std::string>> imNameSce1ch;
		std::map<int, std::vector<std::string>> imNameTar1ch;
		std::map<int, std::vector<std::string>> imNameIndep;

		std::map<int, std::vector<sibr::ImageRGB32F>> mosaicsSce;
		std::map<int, std::vector<sibr::ImageRGB32F>> mosaicsTar;
		std::map<int, std::vector<sibr::ImageL16>> mosaicsSce1ch;
		std::map<int, std::vector<sibr::ImageFloat1>> mosaicsTar1ch;
		std::map<int, std::vector<sibr::ImageRGB32F>> mosaicsIndep;

		for (int im = 0; im < _cams->inputCameras().size(); im++) {
			std::cout << im << ", ";
			int offsetSce = 0;
			int offsetTar = 0;
			int offsetSce1ch = 0;
			int offsetTar1ch = 0;
			int num = 0;

			sibr::InputCamera camCurr = _cams->inputCameras()[im]->resizedH(_height);
			sibr::InputCamera camPrev = _cams->inputCameras()[std::max(0, im - 1)]->resizedH(_height);
			sibr::InputCamera camNext = _cams->inputCameras()[std::min(im + 1, (int)_cams->inputCameras().size() - 1)]->resizedH(_height);

			//Generate flow data
			sibr::RenderTargetRGB32F posNormalCurr(camCurr.w(), camCurr.h(), SIBR_GPU_LINEAR_SAMPLING, 2);
			sibr::RenderTargetRGB32F posNormalPrev(camCurr.w(), camCurr.h(), SIBR_GPU_LINEAR_SAMPLING, 2);
			sibr::RenderTargetRGB32F posNormalNext(camCurr.w(), camCurr.h(), SIBR_GPU_LINEAR_SAMPLING, 2);
			glClearColor(0.0, 0.0, 0.0, 1.0);
			glViewport((GLint)(0), (GLint)(0), (GLsizei)(camCurr.w()), (GLsizei)(camCurr.h()));
			PNR.process(GTMesh, camCurr, posNormalCurr);
			if (im > 0) {
				PNR.process(GTMesh, camPrev, posNormalPrev);
			}
			if (im < _cams->inputCameras().size() - 1) {
				PNR.process(GTMesh, camNext, posNormalNext);
			}

			sibr::ImageRGB32F posMCurr(camCurr.w(), camCurr.h(), sibr::Vector3f(0, 0, 0));
			sibr::ImageRGB32F posMPrev(camCurr.w(), camCurr.h(), sibr::Vector3f(0, 0, 0));
			sibr::ImageRGB32F posMNext(camCurr.w(), camCurr.h(), sibr::Vector3f(0, 0, 0));
			posNormalCurr.readBack(posMCurr, 0);
			posNormalPrev.readBack(posMPrev, 0);
			posNormalNext.readBack(posMNext, 0);


			sibr::ImageL16 flowPrevW(camCurr.w(), camCurr.h(), 65535);
			sibr::ImageL16 flowPrevH(camCurr.w(), camCurr.h(), 65535);
			sibr::ImageL16 flowNextW(camCurr.w(), camCurr.h(), 65535);
			sibr::ImageL16 flowNextH(camCurr.w(), camCurr.h(), 65535);

#pragma omp parallel for
			for (int j = 0; j < posMCurr.h(); j++) {
				for (int i = 0; i < posMCurr.w(); i++) {

					sibr::Vector3f posC = posMCurr(i, j);

					if (im > 0) {

						sibr::Vector2f coordP = camPrev.projectImgSpaceInvertY(posC).xy();
						sibr::Vector2i coordPInt(floor(coordP.x()), floor(coordP.y()));
						if (posMPrev.isInRange(coordPInt) && (posMPrev(coordPInt) - posC).norm() < _scale * 0.05f) {
							flowPrevW(i, j).x() = (ushort)std::max(0, std::min((int)floor(128 * (coordP.x() - i)) + 32768, 65535));
							flowPrevH(i, j).x() = (ushort)std::max(0, std::min((int)floor(128 * (coordP.y() - j)) + 32768, 65535));
						}
					}
					if (im < _cams->inputCameras().size() - 1) {

						sibr::Vector2f coordN = camNext.projectImgSpaceInvertY(posC).xy();
						sibr::Vector2i coordNInt(floor(coordN.x()), floor(coordN.y()));
						if (posMNext.isInRange(coordNInt) && (posMNext(coordNInt) - posC).norm() < _scale * 0.05f) {
							flowNextW(i, j).x() = (ushort)std::max(0, std::min((int)floor(128 * (coordN.x() - i)) + 32768, 65535));
							flowNextH(i, j).x() = (ushort)std::max(0, std::min((int)floor(128 * (coordN.y() - j)) + 32768, 65535));
						}

					}


				}
			}



			//Generate image buffers
			generateInputBuffers(camCurr, im);

			sibr::ImageRGB emptyIm(_width, _height, sibr::Vector3ub(0, 0, 0));

			numMosaic(emptyIm, num, _tileSizeW, _tileSizeH);
			mosaicsSce[im] = std::vector<sibr::ImageRGB32F>();
			mosaicsTar[im] = std::vector<sibr::ImageRGB32F>();
			mosaicsSce1ch[im] = std::vector<sibr::ImageL16>();
			mosaicsTar1ch[im] = std::vector<sibr::ImageFloat1>();
			mosaicsIndep[im] = std::vector<sibr::ImageRGB32F>();

			for (int mos = 0; mos < num; mos++) {
				mosaicsSce[im].push_back(sibr::ImageRGB32F(1 * _tileSizeW, (4 + 2 * _maxInput) * _tileSizeH, sibr::Vector3f(0, 0, 0)));
				mosaicsTar[im].push_back(sibr::ImageRGB32F(1 * _tileSizeW, (4 + 0 * _maxInput) * _tileSizeH, sibr::Vector3f(0, 0, 0)));
				mosaicsSce1ch[im].push_back(sibr::ImageL16(1 * _tileSizeW, (4 + 0 * _maxInput) * _tileSizeH, 0));
				mosaicsTar1ch[im].push_back(sibr::ImageFloat1(1 * _tileSizeW, (3 + 0 * _maxInput) * _tileSizeH, 0));
				mosaicsIndep[im].push_back(sibr::ImageRGB32F(1 * _tileSizeW, (1 + 0 * _maxInput) * _tileSizeH, sibr::Vector3f(0, 0, 0)));
			}

			//Fill flow mosaics
			num = 0;
			fillMosaic(flowPrevW, num, mosaicsSce1ch[im], offsetSce1ch++, _tileSizeW, _tileSizeH);
			num = 0;
			fillMosaic(flowPrevH, num, mosaicsSce1ch[im], offsetSce1ch++, _tileSizeW, _tileSizeH);
			num = 0;
			fillMosaic(flowNextW, num, mosaicsSce1ch[im], offsetSce1ch++, _tileSizeW, _tileSizeH);
			num = 0;
			fillMosaic(flowNextH, num, mosaicsSce1ch[im], offsetSce1ch++, _tileSizeW, _tileSizeH);

			//Fill mosaics for rgb

#pragma omp parallel for
			for (int j = 0; j < _linDiffs[im]->h(); j++) {
				for (int i = 0; i < _linDiffs[im]->w(); i++) {

					float dC = (float)(sibr::Vector2f(i, j) - sibr::Vector2f(_linDiffs[im]->w() / 2, _linDiffs[im]->h() / 2)).norm() / (float)sibr::Vector2f(_linDiffs[im]->w() / 2, _linDiffs[im]->h() / 2).norm();
					float cos4 = pow(cos(dC / 2.0f), 4.0f);

					_linDiffs[im](i, j).x() = _linDiffs[im](i, j).x() / cos4;
					_linDiffs[im](i, j).y() = _linDiffs[im](i, j).y() / cos4;
					_linDiffs[im](i, j).z() = _linDiffs[im](i, j).z() / cos4;

					_linVdeps[im](i, j).x() = _linVdeps[im](i, j).x() / cos4;
					_linVdeps[im](i, j).y() = _linVdeps[im](i, j).y() / cos4;
					_linVdeps[im](i, j).z() = _linVdeps[im](i, j).z() / cos4;

				}
			}


			num = 0;
			fillMosaic(_linDiffs[im]->resized(_width, _height), num, mosaicsTar[im], offsetTar++, _tileSizeW, _tileSizeH);
			num = 0;
			fillMosaic(_linVdeps[im]->resized(_width, _height), num, mosaicsTar[im], offsetTar++, _tileSizeW, _tileSizeH);

			sibr::ImageRGB32F img(_width, _height, sibr::Vector3f(0, 0, 0));
			sibr::ImageFloat1 img1ch(_width, _height, 0);

			//TARGET DATA
			//Fill target rad and mirror target rad
			_OR->_tarData->readBack(img, 0);
			num = 0;
			fillMosaic(img, num, mosaicsTar[im], offsetTar++, _tileSizeW, _tileSizeH);
			_OR->_tarData->readBack(img, 5);
			num = 0;
			fillMosaic(img, num, mosaicsTar[im], offsetTar++, _tileSizeW, _tileSizeH);
			// Angle maps
			_OR->_posNormTarRT->readBack(img1ch, 3);
			num = 0;
			fillMosaic(img1ch, num, mosaicsTar1ch[im], offsetTar1ch++, _tileSizeW, _tileSizeH);
			// reflexion Depthmaps
			_OR->_tarData->readBack(img1ch, 6);
			num = 0;
			fillMosaic(img1ch, num, mosaicsTar1ch[im], offsetTar1ch++, _tileSizeW, _tileSizeH);
			// disparity
			_OR->_posNormRT->readBack(img1ch, 5);
			num = 0;
			fillMosaic(img1ch, num, mosaicsTar1ch[im], offsetTar1ch++, _tileSizeW, _tileSizeH);

			//Indep DATA
			//Fill normal
			_OR->_posNormRT->readBack(img, 4);
			num = 0;
			fillMosaic(img, num, mosaicsSce[im], offsetSce++, _tileSizeW, _tileSizeH);

			//SOURCE DATA
			// radiance maps
			_OR->_radAlbedo->readBack(img, 0);
			num = 0;
			fillMosaic(img, num, mosaicsSce[im], offsetSce++, _tileSizeW, _tileSizeH);
			//Mirror rgb
			_OR->_tarData->readBack(img, 4);
			num = 0;
			fillMosaic(img, num, mosaicsSce[im], offsetSce++, _tileSizeW, _tileSizeH);
			//Mirror radiance
			_OR->_tarData->readBack(img, 3);
			num = 0;
			fillMosaic(img, num, mosaicsSce[im], offsetSce++, _tileSizeW, _tileSizeH);

			for (int k = 0; k < 4; k++) {
				_OR->_out_rgb_spec_IBR->readBack(img, k);
				num = 0;
				fillMosaic(img, num, mosaicsSce[im], offsetSce++, _tileSizeW, _tileSizeH);

				_OR->_out_rgb_spec_IBR->readBack(img, k + 4);
				num = 0;
				fillMosaic(img, num, mosaicsSce[im], offsetSce++, _tileSizeW, _tileSizeH);
			}

			for (int k = 0; k < 4; k++) {
				_OR->_out_rgb_spec_Lum->readBack(img, k);
				num = 0;
				fillMosaic(img, num, mosaicsSce[im], offsetSce++, _tileSizeW, _tileSizeH);

				_OR->_out_rgb_spec_Lum->readBack(img, k + 4);
				num = 0;
				fillMosaic(img, num, mosaicsSce[im], offsetSce++, _tileSizeW, _tileSizeH);
			}

			imNameSce[im] = std::vector<std::string>();
			imNameTar[im] = std::vector<std::string>();
			imNameSce1ch[im] = std::vector<std::string>();
			imNameTar1ch[im] = std::vector<std::string>();
			imNameIndep[im] = std::vector<std::string>();

			for (int mos = 0; mos < mosaicsSce[im].size(); mos++) {

				std::ostringstream out;
				out << std::internal << std::setfill('0') << std::setw(8) << globalNum;
				imNameSce[im].push_back(_args.dataset_path.get() + trainDirName + _lighting.name + "/" + out.str() + "_sce.png");
				imNameTar[im].push_back(_args.dataset_path.get() + trainDirName + _lighting.name + "/" + out.str() + "_tar.png");
				imNameSce1ch[im].push_back(_args.dataset_path.get() + trainDirName + _lighting.name + "/" + out.str() + "_sce_1ch.png");
				imNameTar1ch[im].push_back(_args.dataset_path.get() + trainDirName + _lighting.name + "/" + out.str() + "_tar_1ch.png");
				imNameIndep[im].push_back(_args.dataset_path.get() + trainDirName + "/indep/" + out.str() + "_indep.png");
				globalNum++;
			}


			for (int mos = 0; mos < mosaicsSce[im].size(); mos++) {
				results.push_back(_pool.push(
					[&, mos, im](int id) {
						sibr::ImageRGB16 toSave;

						encodeImLog(mosaicsSce[im][mos]);
						mosaicsSce[im][mos].toOpenCV().convertTo(toSave.toOpenCVnonConst(), CV_16UC3, 65535.0f);
						cv::imwrite(imNameSce[im][mos], toSave.toOpenCVBGR());
						mosaicsSce[im][mos] = sibr::ImageRGB32F();

					}));
				results.push_back(_pool.push(
					[&, mos, im](int id) {
						sibr::ImageRGB16 toSave;

						encodeImLog(mosaicsTar[im][mos]);
						mosaicsTar[im][mos].toOpenCV().convertTo(toSave.toOpenCVnonConst(), CV_16UC3, 65535.0f);
						cv::imwrite(imNameTar[im][mos], toSave.toOpenCVBGR());
						mosaicsTar[im][mos] = sibr::ImageRGB32F();

					}));
				results.push_back(_pool.push(
					[&, mos, im](int id) {

						cv::imwrite(imNameSce1ch[im][mos], mosaicsSce1ch[im][mos].toOpenCVBGR());
						mosaicsSce1ch[im][mos] = sibr::ImageL16();
					}));
				results.push_back(_pool.push(
					[&, mos, im](int id) {
						sibr::ImageL16 toSave1ch;

						encodeImLog(mosaicsTar1ch[im][mos], 1.0f);
						mosaicsTar1ch[im][mos].toOpenCV().convertTo(toSave1ch.toOpenCVnonConst(), CV_16UC1, 65535.0f);
						cv::imwrite(imNameTar1ch[im][mos], toSave1ch.toOpenCVBGR());
						mosaicsTar1ch[im][mos] = sibr::ImageFloat1();
					}));

			}
			if (im % 16 == 0 && im > 0) {
				for (auto& res : results) {
					res.get();
				}
				results.clear();
			}
		}
		for (auto& res : results) {
			res.get();
		}
		std::cout << std::endl;
		return;

	}

	void sibr::indoorRelighterIBRScene::dumpTestData(std::vector<sibr::InputCamera::Ptr> cams, float exp, std::string folderName, std::string prefix)
	{

		std::cout << "Generating data for Image: ";
		sibr::makeDirectory(_args.dataset_path.get() + "/" + folderName + "/");

		int globalNum = 0;
		int chunk = 24;
		for (int im_i = 0; im_i < cams.size(); im_i += chunk) {
			int num = 0;

			//Generate image buffers
			sibr::ImageRGB emptyIm(_width, _height);
			numMosaic(emptyIm, num, _width, _height);
			std::vector<sibr::ImageRGB32F> mosaics;
			std::vector<std::string> imName;

			for (int im_j = 0; im_j < chunk; im_j++) {
				int offset = 0;
				int im = im_i + im_j;
				if (im >= cams.size())
					break;
				std::cout << im << ", ";
				generateInputBuffers(*cams[im], -1, exp);

				mosaics.push_back(sibr::ImageRGB32F(_width, 24 * _height));

				sibr::ImageRGB32F img;
				for (int k = 0; k < 4; k++) {
					_OR->_out_rgb_spec_IBR->readBack(img, k);
					num = im_j;
					fillMosaic(img, num, mosaics, offset++, _width, _height);
					_OR->_out_rgb_spec_IBR->readBack(img, k + 4);
					num = im_j;
					fillMosaic(img, num, mosaics, offset++, _width, _height);
				}
				for (int k = 0; k < 4; k++) {
					_OR->_out_rgb_spec_Lum->readBack(img, k);
					num = im_j;
					fillMosaic(img, num, mosaics, offset++, _width, _height);
					_OR->_out_rgb_spec_Lum->readBack(img, k + 4);
					num = im_j;
					fillMosaic(img, num, mosaics, offset++, _width, _height);
				}


				// radiance maps
				_OR->_radAlbedo->readBack(img, 0);
				num = im_j;
				fillMosaic(img, num, mosaics, offset++, _width, _height);
				//Fill normal
				num = im_j;
				_OR->_posNormRT->readBack(img, 4);
				img.fromOpenCV(img.toOpenCV());
				fillMosaic(img, num, mosaics, offset++, _width, _height);
				// disparity
				_OR->_posNormRT->readBack(img, 5);
				num = im_j;
				fillMosaic(img, num, mosaics, offset++, _width, _height);
				// tar rad add
				_OR->_tarData->readBack(img, 0);
				num = im_j;
				fillMosaic(img, num, mosaics, offset++, _width, _height);
				// tar rad rm
				_OR->_tarData->readBack(img, 1);
				num = im_j;
				fillMosaic(img, num, mosaics, offset++, _width, _height);
				// tar spec
				_OR->_tarData->readBack(img, 2);
				num = im_j;
				fillMosaic(img, num, mosaics, offset++, _width, _height);
				// Angle map
				_OR->_posNormTarRT->readBack(img, 3);
				num = im_j;
				fillMosaic(img, num, mosaics, offset++, _width, _height);
				// reflexion Depthmaps
				_OR->_tarData->readBack(img, 6);
				num = im_j;
				fillMosaic(img, num, mosaics, offset++, _width, _height);

				std::ostringstream out;
				out << std::internal << std::setfill('0') << std::setw(8) << globalNum;
				imName.push_back(_args.dataset_path.get() + "/" + folderName + "/" + prefix + out.str() + ".png");
				globalNum++;
			}


			std::vector<std::future<void>> results;
			for (int mos = 0; mos < mosaics.size(); mos++) {
				results.push_back(_pool.push(
					[&, mos](int id) {
						sibr::ImageRGB16 toSave;

						//encodeImLog(mosaics[mos]);
						mosaics[mos].toOpenCV().convertTo(toSave.toOpenCVnonConst(), CV_16UC3, 65535.0f);
						cv::imwrite(imName[mos], toSave.toOpenCVBGR());

					}));

			}
			for (int mos = 0; mos < mosaics.size(); mos++) {
				results[mos].get();
			}
		}
		std::cout << std::endl;
		return;

	}




	void indoorRelighterIBRScene::loadLayout(std::string path, int numEntry) {

		//loading the layout
		if (!boost::filesystem::exists(path)) {
			std::cout << "Error : layout.txt file not found at " << path << ", you need to have a layout file" << std::endl;
			SIBR_ERR;
		}
		std::ifstream inLayout(path);
		std::string line;

		int l = 0;
		while (std::getline(inLayout, line))
		{
			std::stringstream check1(line);
			std::string intermediate;
			std::vector<int> input_layout;
			while (getline(check1, intermediate, ' '))
			{
				input_layout.push_back(std::strtof(intermediate.c_str(), 0));
			}

			if (l < numEntry)
				_input_texture_channels.push_back(input_layout);
			else
				_input_texture_mapping.push_back(input_layout);

			l++;
		}

	}

	void sibr::indoorRelighterIBRScene::runSaveSession(std::vector<sibr::InputCamera::Ptr>& cams, std::string outputFolder) {

		sibr::ImageRGB32F im;
		int i = 0;

		for (auto& cam : cams) {

			std::ostringstream out;
			out << std::internal << std::setfill('0') << std::setw(8) << i;

			sibr::indoorRelighterIBRScene::generateInputBuffers(*cam, -1, pow(2.0f, 7.0));

			_network->run(std::vector<at::Tensor>() = {});

			im = _outTex->readBack();
			im.save(outputFolder + "/" + out.str() + ".png");
			i++;
		}

	}

	struct interactiveSession {

		sibr::GLShader renderShader;
		sibr::GLuniform<float> expo_rdr;
		sibr::GLuniform<float> tempK_rdr;
		sibr::GLuniform<float> gamma_rdr;
		sibr::GLuniform<float> sat_rdr;
		sibr::GLShader quadShadersRGB;

		std::vector<sibr::Viewport> vpIms;
		sibr::Viewport vpInRad;
		sibr::Viewport vpTarRadAdd;
		sibr::Viewport vpTarRadRm;
		sibr::Viewport vpReflexion;
		sibr::Viewport vpDebug;
		sibr::Viewport vpOut;

		interactiveSession::interactiveSession(
			sibr::Window::Ptr _window,
			int _maxInput,
			int _width,
			int _height
		) {

			float ratio = static_cast<float>(_width) / static_cast<float>(_height);
			if (ratio < 1.0) {
				ratio = 1 / ratio;
			}
			std::cout << "The ratio is " << ratio << std::endl;

			float thbW = 256.0f;
			float thbH = thbW / ratio;

			//Resize window after selection
			_window->size(_width + (2 * ceil(_maxInput / 4.0) + 2) * thbW, std::max(4 * thbH + thbW / 2, _height + thbW / 2));

			float offsetTop = (_window->h() - 4 * thbH) / 2;

			renderShader.init("Texture",
				sibr::loadFile(sibr::Resources::Instance()->getResourceFilePathName("texture.vert")),
				sibr::loadFile(sibr::Resources::Instance()->getResourceFilePathName("texturesTM.frag")));
			expo_rdr.init(renderShader, "expo");
			tempK_rdr.init(renderShader, "tempK");
			gamma_rdr.init(renderShader, "gamma");
			sat_rdr.init(renderShader, "sat");

			quadShadersRGB.init("Texture",
				sibr::loadFile(sibr::Resources::Instance()->getResourceFilePathName("texture.vert")),
				sibr::loadFile(sibr::Resources::Instance()->getResourceFilePathName("texturesRGB.frag")));

			for (int in = 0; in < _maxInput; in++) {
				vpIms.push_back(sibr::Viewport((2 * floor(in / 4.0)) * thbW, _window->h() - (in % 4 + 1) * thbH - offsetTop, (2 * floor(in / 4.0) + 1) * thbW, _window->h() - (in % 4) * thbH - offsetTop));
				vpIms.push_back(sibr::Viewport((2 * floor(in / 4.0) + 1) * thbW, _window->h() - (in % 4 + 1) * thbH - offsetTop, (2 * floor(in / 4.0) + 2) * thbW, _window->h() - (in % 4) * thbH - offsetTop));
			}

			vpInRad = sibr::Viewport(_window->w() - thbW, _window->h() - thbH - offsetTop, _window->w(), _window->h() - offsetTop);
			vpTarRadAdd = sibr::Viewport(_window->w() - thbW, _window->h() - 2 * thbH - offsetTop, _window->w(), _window->h() - thbH - offsetTop);
			vpTarRadRm = sibr::Viewport(_window->w() - thbW, _window->h() - 3 * thbH - offsetTop, _window->w(), _window->h() - 2 * thbH - offsetTop);
			vpReflexion = sibr::Viewport(_window->w() - thbW, _window->h() - 4 * thbH - offsetTop, _window->w(), _window->h() - 3 * thbH - offsetTop);
			vpDebug = sibr::Viewport(_window->w() - thbW, _window->h() - 5 * thbH - offsetTop, _window->w(), _window->h() - 4 * thbH - offsetTop);

			vpOut = sibr::Viewport(
				(2 * ceil(_maxInput / 4.0) + 0.5) * thbW, _window->h() / 2 - (_height / 2),
				(2 * ceil(_maxInput / 4.0) + 0.5) * thbW + _width, _window->h() / 2 + (_height / 2)
			);
		};

		void renderScreen(sibr::indoorRelighterIBRScene& scene, sibr::indoorRelighterIBRScene::uiData& ui) {

			glDisable(GL_DEPTH_TEST);

			quadShadersRGB.begin();
			glActiveTexture(GL_TEXTURE0);
			for (int in = 0; in < std::min(4, ui.numInput); in++) {
				vpIms[2 * in].bind();
				glBindTexture(GL_TEXTURE_2D, scene._OR->_out_rgb_spec_IBR->handle(in));
				sibr::RenderUtility::renderScreenQuad();
				vpIms[2 * in + 1].bind();
				glBindTexture(GL_TEXTURE_2D, scene._OR->_out_rgb_spec_IBR->handle(in + 4));
				sibr::RenderUtility::renderScreenQuad();
				glFinish();
			}
			for (int in = 4; in < ui.numInput; in++) {

				vpIms[2 * in].bind();
				glBindTexture(GL_TEXTURE_2D, scene._OR->_out_rgb_spec_Lum->handle(in - 4));
				sibr::RenderUtility::renderScreenQuad();
				vpIms[2 * in + 1].bind();
				glBindTexture(GL_TEXTURE_2D, scene._OR->_out_rgb_spec_Lum->handle(in));
				sibr::RenderUtility::renderScreenQuad();
				glFinish();

			}


			vpInRad.bind();
			//glBindTexture(GL_TEXTURE_2D, scene._OR->_posNormRT->handle(4));
			glBindTexture(GL_TEXTURE_2D, scene._OR->_radAlbedo->handle(0));
			sibr::RenderUtility::renderScreenQuad();

			vpTarRadAdd.bind();
			//glBindTexture(GL_TEXTURE_2D, scene._OR->_posNormRT->handle(5));
			glBindTexture(GL_TEXTURE_2D, scene._OR->_tarData->handle(0));
			sibr::RenderUtility::renderScreenQuad();

			vpTarRadRm.bind();
			//glBindTexture(GL_TEXTURE_2D, scene._OR->_posNormRT->handle(3));
			glBindTexture(GL_TEXTURE_2D, scene._OR->_tarData->handle(1));
			sibr::RenderUtility::renderScreenQuad();

			vpReflexion.bind();
			//glBindTexture(GL_TEXTURE_2D, scene._OR->_tarData->handle(6));
			glBindTexture(GL_TEXTURE_2D, scene._OR->_tarData->handle(2));
			sibr::RenderUtility::renderScreenQuad();

			quadShadersRGB.end();
			//Compute the output

			glFinish();
			//	_window->makeContextNull();

			if (ui.networkActive && (ui.imChanged || ui.tarChanged)) {
				//run the model
				scene._network->run(std::vector<at::Tensor>() = {});
			}

			//	_window->makeContextCurrent();

			renderShader.begin();
			expo_rdr.set(ui.expo_rdr);
			tempK_rdr.set(ui.tempK_rdr);
			gamma_rdr.set(ui.gamma_rdr);
			sat_rdr.set(ui.sat_rdr);
			vpOut.bind();

			if (ui.networkActive) {
				glBindTexture(GL_TEXTURE_2D, scene._output_textures[ui.numOutput]->handle);
				sibr::RenderUtility::renderScreenQuad();
			}
			else {
				/*sibr::ImageRGB32F imRGB;
				scene._OR->_out_rgb_spec_IBR->readBack(imRGB, 2);
				sibr::ImageRGB32F imRad;
				scene._OR->_radAlbedo->readBack(imRad, 0);
				sibr::ImageRGB32F imAdd;
				scene._OR->_tarData->readBack(imAdd, 0);

				sibr::ImageRGB32F imAlbedo;
				imAlbedo.fromOpenCV(imRGB.toOpenCV() / imRad.toOpenCV());
				cv::threshold(imAlbedo.toOpenCV(), imAlbedo.toOpenCV(), 1.0, 1.0, cv::THRESH_TRUNC);
				sibr::ImageRGB32F imFinal;
				if (!scene._OR->getTurnOffState()) {
					imAdd.fromOpenCV(imAdd.toOpenCV() + imRad.toOpenCV());
				}
				imFinal.fromOpenCV(imAlbedo.toOpenCV().mul(imAdd.toOpenCV()));
				imFinal.flipH();
				sibr::Texture2DRGB32F texIm(imFinal);
				glBindTexture(GL_TEXTURE_2D, texIm.handle());*/
				glBindTexture(GL_TEXTURE_2D, scene._OR->_out_rgb_spec_IBR->handle(2));
				sibr::RenderUtility::renderScreenQuad();

			}


			renderShader.end();
			glFinish();

		};





	};


	void indoorRelighterIBRScene::runInteractiveSession() {


		//////////////////

		bool needClose = false;

		uiData ui;
		sibr::interactiveSession IS(_window, _maxInput, _width, _height);

		sibr::FPSCamera fpsCam;
		fpsCam.fromCamera(*_cams->inputCameras()[0]);

		sibr::MultiTexturedMeshRenderer MTMR;

		auto timeLastFrame = std::chrono::steady_clock::now();
		while (!needClose) {

			// Elspaed time since last rendering.
			auto timeNow = std::chrono::steady_clock::now();
			float deltaTime = std::chrono::duration<float>(timeNow - timeLastFrame).count();
			timeLastFrame = timeNow;


			sibr::Input::poll();
			_window->makeContextCurrent();

			ui.tarChanged = false;
			ui.imChanged = false;
			ui.lightChanged = false;

			gui(ui, deltaTime, fpsCam, IS);


			auto timePre = std::chrono::steady_clock::now();
			if (ui.imChanged || ui.tarChanged) {
				generateInputBuffers(fpsCam.getCamera(), -1, pow(2.0f, ui.exposure));
			}

			if (sibr::Input::global().key().isPressed(sibr::Key::Escape))
				needClose = true;
			else {

				glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
				_window->bind();
				_window->clear();

				if (ui.imId >= 0) {
					IS.renderScreen(*this, ui);
				}

				_window->swapBuffer();
			}
			if (ui.imChanged || ui.tarChanged) {
				auto timeAft = std::chrono::steady_clock::now();
				float deltaTime = std::chrono::duration<float>(timeAft - timePre).count();
				std::cout << "TOTAL: " << deltaTime << " -- " << 1.0 / deltaTime << " fps" << std::endl;
			}

		}

	}

	void sibr::indoorRelighterIBRScene::gui(
		uiData& uiDt,
		float dlt,
		sibr::FPSCamera& fpsCam,
		sibr::interactiveSession& IS
	)
	{
		ImGui::Begin("Relighter");
		if (ImGui::SliderInt("Im", &(uiDt.imId), 0, _cams->inputCameras().size() - 1)) {
			uiDt.imChanged = true;
		}
		if (ImGui::SliderInt("Number of input", &uiDt.numInput, 1, _maxInput)) {

			uiDt.tarChanged = true;
		}

		ImGui::SliderInt("Output N�", &uiDt.numOutput, 0, 0);

		if (uiDt.imChanged) {
			fpsCam.fromCamera(*_cams->inputCameras()[uiDt.imId]);
		}
		else {
			sibr::InputCamera prev = fpsCam.getCamera();
			fpsCam.update(sibr::Input::global(), dlt);
			if (
				prev.position() != fpsCam.getCamera().position() ||
				prev.dir() != fpsCam.getCamera().dir()
				) {
				uiDt.tarChanged = true;
			}
		}
		if (ImGui::SliderFloat("Exposure", &uiDt.exposure, -3.0f, 14.0f)) {
			uiDt.tarChanged = true;
		}

		if (ImGui::Checkbox("Run Network", &uiDt.networkActive)) {
			uiDt.tarChanged = true;
		}
		if (ImGui::Button("Load new Network")) {

			std::string inpath;
			if (sibr::showFilePicker(inpath, Default, _args.dataset_path.get(), "pt") && !inpath.empty()) {
				_network->loadModel(inpath);
			}

			uiDt.tarChanged = true;
		}


		/*if (_synthetic) {
						if (ImGui::SliderFloat("Gamma", &gamma, 1.0f, 3.0f)) {
				tarChanged = true;
			}
		}*/


		ImGui::End();
		ImGui::Begin("Camera");
		ImGui::SliderFloat("Speed", &uiDt.speedCam, -1.0, 3.0);
		fpsCam.setSpeed(pow(10.0f, uiDt.speedCam));
		if (ImGui::SliderFloat("Fov Y", &uiDt.fovyCam, 1.0f, 180.0f)) {
			sibr::InputCamera currCam = fpsCam.getCamera();
			currCam.fovy(uiDt.fovyCam * float(M_PI) / 180.0f);
			fpsCam.fromCamera(currCam);
			uiDt.tarChanged = true;
		}
		if (ImGui::Button("Dump data")) {
			std::vector<sibr::InputCamera::Ptr> cams;
			if (sibr::fileExists(_args.camPath.get() + "/camerasRender.lookat"))
				cams = sibr::InputCamera::loadLookat(_args.camPath.get() + "/camerasRender.lookat", { sibr::Vector2u(_width,_height) });
			else if (sibr::fileExists(_args.dataset_path.get() + "/camerasRender.lookat"))
				cams = sibr::InputCamera::loadLookat(_args.dataset_path.get() + "/camerasRender.lookat", { sibr::Vector2u(_width,_height) });

			dumpTestData(cams, pow(2.0, uiDt.exposure));
		};
		ImGui::Separator();
		ImGui::Checkbox("Save Playback", &uiDt.savePlayBack);
		ImGui::SameLine();
		ImGui::Checkbox("Path Statistics", &uiDt.computePathStats);

		if (ImGui::Button("Load cam path")) {
			std::string inpath;
			if (sibr::showFilePicker(inpath, Default, _args.dataset_path.get(), "lookat") && !inpath.empty()) {
				std::cout << "Loaded cam path";
				uiDt.camPathFile = inpath;
			}
		}
		ImGui::SameLine();
		if (ImGui::Button("PlayBack cam path")) {

			ImGui::End();
			std::vector<sibr::InputCamera::Ptr> cams;
			if (!uiDt.camPathFile.empty())
				cams = sibr::InputCamera::loadLookat(uiDt.camPathFile, { sibr::Vector2u(_width,_height) });
			else if (sibr::fileExists(_args.dataset_path.get() + "/camerasRender.lookat"))
				cams = sibr::InputCamera::loadLookat(_args.dataset_path.get() + "/camerasRender.lookat", { sibr::Vector2u(_width,_height) });

			//Data for saving case
			sibr::ImageRGB im;
			sibr::GLShader renderShader;
			sibr::GLuniform<float> expo_rdr;
			sibr::GLuniform<float> tempK_rdr;
			sibr::GLuniform<float> gamma_rdr;
			sibr::GLuniform<float> sat_rdr;
			sibr::RenderTargetRGB32F tar(_width, _height);
			std::string dirOut;
			int ci = 0;
			if (uiDt.savePlayBack) {
				auto t = std::time(nullptr);
				auto tm = *std::localtime(&t);
				std::ostringstream oss;
				oss << std::put_time(&tm, "%Y-%m-%d_%H-%M-%S");
				auto str_time = oss.str();
				dirOut = _args.dataset_path.get() + "/saved_output_" + str_time + "/";
				sibr::makeDirectory(dirOut);
			}

			//Initialize vector of distance to cameras, used for statistics
			std::vector<float> closestCamDists;

			for (auto& c : cams) {
				sibr::Input::poll();

				ImGui::Begin("Stop");
				if (ImGui::Button("STOP")) {
					break;
				}
				ImGui::End();

				_window->makeContextCurrent();
				generateInputBuffers(*c, -1, pow(2.0f, uiDt.exposure));
				glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
				_window->bind();
				_window->clear();
				uiDt.tarChanged = true;
				IS.renderScreen(*this, uiDt);
				_window->swapBuffer();

				if (uiDt.savePlayBack) {

					renderShader.init("Texture",
						sibr::loadFile(sibr::Resources::Instance()->getResourceFilePathName("texture.vert")),
						sibr::loadFile(sibr::Resources::Instance()->getResourceFilePathName("texturesTM.frag")));
					expo_rdr.init(renderShader, "expo");
					tempK_rdr.init(renderShader, "tempK");
					gamma_rdr.init(renderShader, "gamma");
					sat_rdr.init(renderShader, "sat");

					glViewport(0, 0, _width, _height);
					renderShader.begin();
					expo_rdr.set(uiDt.expo_rdr);
					tempK_rdr.set(uiDt.tempK_rdr);
					gamma_rdr.set(uiDt.gamma_rdr);
					sat_rdr.set(uiDt.sat_rdr);
					tar.clear();
					tar.bind();
					glBindTexture(GL_TEXTURE_2D, _output_textures[uiDt.numOutput]->handle);
					sibr::RenderUtility::renderScreenQuad();
					renderShader.end();
					tar.readBack(im);
					tar.unbind();

					std::ostringstream outNum;
					outNum << std::internal << std::setfill('0') << std::setw(8) << ci;

					im.save(dirOut + outNum.str() + ".png");
					ci++;
				}

				if (uiDt.computePathStats) {
					float minDist = 1000 / _scale;
					for (const auto& inCam : _cams->inputCameras()) {
						float dist = (c->position() - inCam->position()).norm() / _scale; //Dividing by scale to get a distance in meters
						if (dist < minDist) {
							minDist = dist;
						}
					}
					closestCamDists.push_back(minDist);
				}

			}
			if (uiDt.computePathStats) {
				double meanDist = std::accumulate(closestCamDists.begin(), closestCamDists.end(), 0.0) / closestCamDists.size();
				double sqSum = std::inner_product(closestCamDists.begin(), closestCamDists.end(), closestCamDists.begin(), 0.0);
				double stdev = std::sqrt(sqSum / closestCamDists.size() - meanDist * meanDist);

				float cCount = 0;
				std::ofstream statfile;
				statfile.open(uiDt.camPathFile + "_stat.csv");
				for (float d : closestCamDists) {
					statfile << cCount / (closestCamDists.size() - 1.0) << "," << d << std::endl;
					cCount += 1;
				}
				statfile.close();

				std::cout << "Mean dist along path: " << meanDist << " m -- Standard Deviation: " << stdev << " m" << std::endl;

			}


			ImGui::Begin("Camera");

		}
		if (ImGui::Button("Save Leave One Out")) {

			ImGui::End();
			std::string pathOut = _args.dataset_path.get() + "/Leave1OutIndoorRelighting/";
			sibr::makeDirectory(pathOut);
			sibr::GLShader renderShader;
			sibr::GLuniform<float> expo_rdr;
			sibr::GLuniform<float> tempK_rdr;
			sibr::GLuniform<float> gamma_rdr;
			sibr::GLuniform<float> sat_rdr;
			sibr::RenderTargetRGB32F tar(_width, _height);

			std::cout << "Rendering Leave one Out. Tone Mapping using the input exposure and output gamma ONLY!" << std::endl;

			for (int c = 0; c < cameras()->inputCameras().size(); c++) {
				sibr::Input::poll();

				ImGui::Begin("Stop");
				if (ImGui::Button("STOP")) {
					break;
				}
				ImGui::End();

				_window->makeContextCurrent();
				generateInputBuffers(*cameras()->inputCameras()[c], c, pow(2.0f, uiDt.exposure));
				glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
				_window->bind();
				_window->clear();
				uiDt.tarChanged = true;
				IS.renderScreen(*this, uiDt);
				_window->swapBuffer();

				//ToneMapping
				sibr::ImageRGB32F render;
				renderShader.init("Texture",
					sibr::loadFile(sibr::Resources::Instance()->getResourceFilePathName("texture.vert")),
					sibr::loadFile(sibr::Resources::Instance()->getResourceFilePathName("texturesTM.frag")));
				expo_rdr.init(renderShader, "expo");
				tempK_rdr.init(renderShader, "tempK");
				gamma_rdr.init(renderShader, "gamma");
				sat_rdr.init(renderShader, "sat");

				glViewport(0, 0, _width, _height);
				renderShader.begin();
				expo_rdr.set(uiDt.expo_rdr);
				tempK_rdr.set(uiDt.tempK_rdr);
				gamma_rdr.set(uiDt.gamma_rdr);
				sat_rdr.set(uiDt.sat_rdr);
				tar.clear();
				tar.bind();
				glBindTexture(GL_TEXTURE_2D, _output_textures[uiDt.numOutput]->handle);
				sibr::RenderUtility::renderScreenQuad();
				renderShader.end();
				tar.readBack(render);
				tar.unbind();

				sibr::ImageRGB32F gt = _linDiffs[c]->resized(render.w(), render.h(), cv::INTER_AREA);

				//Tonemapping
				double	imMSE = 0;
				for (int j = 0; j < render.h(); j++)
					for (int i = 0; i < render.w(); i++) {
						for (int ch = 0; ch < 3; ch++) {
							render(i, j)[ch] = std::max(0.0f, std::min(1.0f,render(i, j)[ch]));
							gt(i, j)[ch] = std::max(0.0f, std::min(1.0f, std::pow(std::pow(2.0f, uiDt.exposure) * gt(i, j)[ch], 1 / uiDt.gamma_rdr)));

						}
					}

				std::ostringstream outNum;
				outNum << std::internal << std::setfill('0') << std::setw(8) << c;

				render.save(pathOut + outNum.str() + "_rdr.png");
				gt.save(pathOut + outNum.str() + "_gt.png");


			}

			ImGui::Begin("Camera");

		}

		if (ImGui::Button("Load light path")) {
			std::string inpath;
			if (sibr::showFilePicker(inpath, Directory, _args.dataset_path.get()) && !inpath.empty()) {
				std::cout << "Loaded cam path";
				uiDt.lightPathFolder = inpath;
			}
		}
		ImGui::SameLine();
		ImGui::SliderFloat("Exposure", &uiDt.expo_lp, 0.0, 10.0);
		ImGui::SameLine();
		if (ImGui::Button("PlayBack light path")) {


			ImGui::End();
			//Data for saving case
			sibr::ImageRGB im;
			sibr::GLShader renderShader;
			sibr::GLuniform<float> expo_rdr;
			sibr::GLuniform<float> tempK_rdr;
			sibr::GLuniform<float> gamma_rdr;
			sibr::GLuniform<float> sat_rdr;
			sibr::RenderTargetRGB32F tar(_width, _height);
			std::string dirOut;
			int ci = 0;
			if (uiDt.savePlayBack) {
				auto t = std::time(nullptr);
				auto tm = *std::localtime(&t);
				std::ostringstream oss;
				oss << std::put_time(&tm, "%Y-%m-%d_%H-%M-%S");
				auto str_time = oss.str();
				dirOut = _args.dataset_path.get() + "/saved_light_output_" + str_time + "/";
				sibr::makeDirectory(dirOut);
			}

			generateInputBuffers(fpsCam.getCamera(), -1, pow(2.0f, uiDt.exposure));

			std::vector<std::string> lights;

			int numDynamicLight = sibr::listFiles(uiDt.lightPathFolder).size();

			for (int l = 0; l < numDynamicLight; l++) {

				sibr::Input::poll();
				_window->makeContextCurrent();

				sibr::ImageRGB32F radAdd;
				radAdd.load(uiDt.lightPathFolder + "/outLight" + std::to_string(l) + "/dynamic_00000_irdc.exr");
				radAdd = _denoiser->denoise(radAdd)->clone();
				radAdd.fromOpenCV(pow(2, uiDt.expo_lp) * radAdd.toOpenCV());
				sibr::Texture2DRGB32F radAddTex(radAdd);

				glBindFramebuffer(GL_FRAMEBUFFER, _OR->_tarData->fbo());
				glDrawBuffer(GL_COLOR_ATTACHMENT0);
				sibr::blit_and_flip(radAddTex, *_OR->_tarData);
				_OR->_tarData->bind();
				_OR->_tarData->unbind();

				glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
				_window->bind();
				_window->clear();
				uiDt.tarChanged = true;
				IS.renderScreen(*this, uiDt);
				_window->swapBuffer();

				if (uiDt.savePlayBack) {

					renderShader.init("Texture",
						sibr::loadFile(sibr::Resources::Instance()->getResourceFilePathName("texture.vert")),
						sibr::loadFile(sibr::Resources::Instance()->getResourceFilePathName("texturesTM.frag")));
					expo_rdr.init(renderShader, "expo");
					tempK_rdr.init(renderShader, "tempK");
					gamma_rdr.init(renderShader, "gamma");
					sat_rdr.init(renderShader, "sat");

					glViewport(0, 0, _width, _height);
					renderShader.begin();
					expo_rdr.set(uiDt.expo_rdr);
					tempK_rdr.set(uiDt.tempK_rdr);
					gamma_rdr.set(uiDt.gamma_rdr);
					sat_rdr.set(uiDt.sat_rdr);
					tar.clear();
					tar.bind();
					glBindTexture(GL_TEXTURE_2D, _output_textures[uiDt.numOutput]->handle);
					sibr::RenderUtility::renderScreenQuad();
					renderShader.end();
					tar.readBack(im);
					tar.unbind();

					std::ostringstream outNum;
					outNum << std::internal << std::setfill('0') << std::setw(8) << ci;

					im.save(dirOut + outNum.str() + ".png");
					ci++;
				}

			}

			ImGui::Begin("Camera");


		}
		ImGui::Separator();
		if (ImGui::Button("Save current Cam")) {
			std::vector<sibr::InputCamera::Ptr> cams = { std::make_shared<sibr::InputCamera>(fpsCam.getCamera()) };

			auto t = std::time(nullptr);
			auto tm = *std::localtime(&t);
			std::ostringstream oss;
			oss << std::put_time(&tm, "%Y-%m-%d_%H-%M-%S");

			auto str_time = oss.str();

			InputCamera::saveAsLookat(cams, _args.dataset_path.get() + "/" + str_time + "_cameras.lookat");
		}
		if (ImGui::Button("Load Cam")) {
			std::string inpath;
			if (sibr::showFilePicker(inpath, Default, _args.dataset_path.get(), "lookat") && !inpath.empty()) {
				std::cout << "Loading";
				fpsCam.fromCamera(*sibr::InputCamera::loadLookat(inpath,
					{ sibr::Vector2u(_width,_height) })[0]);
			}
			uiDt.tarChanged = true;
		}

		if (ImGui::Button("Dump current view")) {
			std::vector<sibr::InputCamera::Ptr> cams = { std::make_shared<sibr::InputCamera>(fpsCam.getCamera()) };

			auto t = std::time(nullptr);
			auto tm = *std::localtime(&t);
			std::ostringstream oss;
			oss << std::put_time(&tm, "%Y-%m-%d_%H-%M-%S");

			auto str_time = oss.str();

			dumpTestData(cams, pow(2.0, uiDt.exposure), "test", str_time + "_");
		};
		if (ImGui::Button("Save current output")) {
			sibr::ImageRGB im;

			sibr::GLShader renderShader;
			sibr::GLuniform<float> expo_rdr;
			sibr::GLuniform<float> tempK_rdr;
			sibr::GLuniform<float> gamma_rdr;
			sibr::GLuniform<float> sat_rdr;

			sibr::RenderTargetRGB32F tar(_width, _height);

			renderShader.init("Texture",
				sibr::loadFile(sibr::Resources::Instance()->getResourceFilePathName("texture.vert")),
				sibr::loadFile(sibr::Resources::Instance()->getResourceFilePathName("texturesTM.frag")));
			expo_rdr.init(renderShader, "expo");
			tempK_rdr.init(renderShader, "tempK");
			gamma_rdr.init(renderShader, "gamma");
			sat_rdr.init(renderShader, "sat");

			glViewport(0, 0, _width, _height);
			renderShader.begin();
			expo_rdr.set(uiDt.expo_rdr);
			tempK_rdr.set(uiDt.tempK_rdr);
			gamma_rdr.set(uiDt.gamma_rdr);
			sat_rdr.set(uiDt.sat_rdr);
			tar.clear();
			tar.bind();
			glBindTexture(GL_TEXTURE_2D, _output_textures[uiDt.numOutput]->handle);
			sibr::RenderUtility::renderScreenQuad();
			renderShader.end();
			tar.readBack(im);
			tar.unbind();

			auto t = std::time(nullptr);
			auto tm = *std::localtime(&t);
			std::ostringstream oss;
			oss << std::put_time(&tm, "%Y-%m-%d_%H-%M-%S");
			auto str_time = oss.str();

			im.save(_args.dataset_path.get() + "/snap_" + str_time + ".png");

		};
		ImGui::Separator();

		ImGui::End();

		ImGui::Begin("Render Tonemapping");
		ImGui::SliderFloat("Exposure", &uiDt.expo_rdr, -2.0, 4.0);
		ImGui::SliderFloat("WB temp(K)", &uiDt.tempK_rdr, 1500.0, 15000.0);
		ImGui::SliderFloat("Gamma", &uiDt.gamma_rdr, 0.1, 4.0);
		ImGui::SliderFloat("Saturation", &uiDt.sat_rdr, 1.0, 4.0);
		ImGui::End();

		ImGui::Begin("Object handler");
		if (
			ImGui::Button("Reset Light")
			) {
			resetRelighting();
			uiDt.tarChanged = true;
		}

		ImGui::Separator();

		if (
			ImGui::Button("Switch Input Light")
			) {
			_OR->switchInput();
			uiDt.tarChanged = true;
		}

		ImGui::Separator();

		static char objectName[128] = "no light";
		if (_relightingDeltas.size() > 0) {
			ImGui::SliderInt("Object", &uiDt.objectToEdit, 0, _relightingDeltas.size() - 1);
			ImGui::TextColored(
				_relightingDeltas[uiDt.objectToEdit].emissionColor,
				_relightingDeltas[uiDt.objectToEdit].name.c_str()
			);

			float emissionExposure = _relightingDeltas[uiDt.objectToEdit].emissionExposure;
			ImVec4 color = _relightingDeltas[uiDt.objectToEdit].emissionColor;
			std::string switchBtn;
			if (_relightingDeltas[uiDt.objectToEdit].active) {
				switchBtn = "Switch OFF";
			}
			else {
				switchBtn = "Switch ON";
			}
			if (
				ImGui::ColorPicker3("light Color", &color.x) ||
				ImGui::SliderFloat("Emission Intensity", &emissionExposure, -8.0, 16.0)
				//ImGui::SliderFloat("Transparency", &_relightingDeltas[uiDt.objectToEdit].transparency, 0.0, 1.0)
				)
			{
				updateRelighting(_relightingDeltas[uiDt.objectToEdit].name,
					emissionExposure,
					sibr::Vector4f(color.x, color.y, color.z, 1.0),
					false);
				uiDt.tarChanged = true;
				uiDt.lightChanged = true;
			}

			else if (ImGui::Button(switchBtn.c_str()))
			{
				updateRelighting(
					_relightingDeltas[uiDt.objectToEdit].name,
					_relightingDeltas[uiDt.objectToEdit].emissionExposure,
					sibr::Vector4f(color.x, color.y, color.z, 1.0),
					true);
				uiDt.tarChanged = true;
				uiDt.lightChanged = true;
			}

			if (uiDt.lightChanged) {
			}
			uiDt.lightChanged = false;

		}
		else {
			ImGui::TextColored(
				ImVec4(1.0, 1.0, 1.0, 1.0),
				"No object loaded yet"
			);
		}

		ImGui::Separator();

		ImGui::InputText("Object name", objectName, IM_ARRAYSIZE(objectName));
		if (ImGui::Button("Load new object")) {

			loadRelightingDelta(objectName, ADD_ONLY);
			updateRelighting();

			uiDt.objectLoaded = true;
			uiDt.tarChanged = true;
			uiDt.lightChanged = true;

		}


		ImGui::End();


	}

	void sibr::indoorRelighterIBRScene::resetRelighting(bool update, bool deactivateAll) {

		if (deactivateAll) {
			for (auto& rd : _relightingDeltas) {
				rd.active = false;
			}
			std::cout << "Resetting geometry" << std::endl;
			sibr::Mesh::Ptr proxyTar(new sibr::Mesh(_proxies->proxy()));
			_OR->updateTargetGeometry(proxyTar, _cams->inputCameras());

		}

		_linRadAdds = std::vector<sibr::ImageRGB32F>(_cams->inputCameras().size());

#pragma omp parallel for
		for (int im = 0; im < _linRadAdds.size(); im++) {

			_linRadAdds[im].fromOpenCV(
				sibr::ImageRGB32F(_dmTex[im]->w(),
					_dmTex[im]->h(), sibr::Vector3f(0, 0, 0)).toOpenCV()
			);
		}

		if (update) {
			_TexturesRadianceAdd->updateFromImages(_linRadAdds);
		}

	}

	void sibr::indoorRelighterIBRScene::loadRelightingDelta(std::string lightName, RLOADMODE mode) {

		if (!sibr::fileExists(_args.dataset_path.get() + "/lightings/" + lightName)) {
			std::cout << "WARNING: Could not find the light file skipping" << std::endl;
			return;
		}

		for (auto& rd : _relightingDeltas) {
			if (lightName == rd.name) {
				std::cout << "Object " << std::string(lightName) << " already loaded" << std::endl;
				return;
			}
		}
		//Not found we need to load it
		relightingDelta rd;
		rd.name = lightName;
		rd.active = true;

		std::string pureLightName = boost::filesystem::path(lightName).stem().string();

		if (boost::filesystem::extension(lightName) == ".xml") {
			//Update state to load correct data
			lightingSpec lighting = loadLighting(_args.dataset_path.get() + "/lightings/", lightName);
			rd.mesh = sibr::Mesh::getSphereMesh(lighting.pos, lighting.radius, true, 72);
		}
		else if (boost::filesystem::extension(lightName) == ".ply") {
			/////// Test object
			rd.mesh->load(_args.dataset_path.get() + "/objects/" + lightName);
			//object.rdlt = getRadObject(
			//	boost::filesystem::basename(objectName),
			//	object.mesh,
			//	_pos3D,
			//	_normals);

		}
		else {
			SIBR_WRG << " Object type not handled, not loading anything!" << std::endl;
			return;
		}

		rd.mesh->makeWhole();


		std::vector<sibr::ImageRGB32F> delta(_cams->inputCameras().size());
		for (int im = 0; im < cameras()->inputCameras().size(); im++) {

			if (mode == ADD_ONLY || mode == RM_ONLY) {

				sibr::ImageRGB32F::Ptr imgIrdc(new sibr::ImageRGB32F());
				bool loaded = imFrom16BitOrRender(imgIrdc, im, "_irdc", "_irdc", pureLightName);
				if (!loaded) {
					std::cout << "Could not load irdc file " << im << std::endl;
					SIBR_ERR;
				}

				imgIrdc->fromOpenCV(imgIrdc->resized(_dmTex[im]->w(), _dmTex[im]->h(), cv::INTER_AREA).toOpenCV());
				std::cout << _dmTex[im]->w() << " " << imgIrdc->w() << "\n";
				sibr::ImageRGB32F::Ptr imgIrdcNull(new sibr::ImageRGB32F(imgIrdc->w(), imgIrdc->h()));
				if (mode == ADD_ONLY) {
					rd.rdlt.push_back(radDelta{ imgIrdc,imgIrdcNull });
				}
				else {
					rd.rdlt.push_back(radDelta{ imgIrdcNull,imgIrdc });
				}
			}
			else if (mode == ADD_RM) {

				sibr::ImageRGB32F::Ptr imgIrdcAdd(new sibr::ImageRGB32F());
				sibr::ImageRGB32F::Ptr imgIrdcRm(new sibr::ImageRGB32F());
				bool loadedAdd = imFrom16BitOrRender(imgIrdcAdd, im, "", "_irdc_add", pureLightName);
				bool loadedRm = imFrom16BitOrRender(imgIrdcRm, im, "", "_irdc_rm", pureLightName);
				if (!loadedAdd || !loadedRm) {
					std::cout << "Could not load irdc file " << im << std::endl;
					SIBR_ERR;
				}

				rd.rdlt.push_back(radDelta{ imgIrdcAdd,imgIrdcRm });


			}
		}

		_relightingDeltas.push_back(rd);
	}

	void sibr::indoorRelighterIBRScene::updateRelighting(bool updateGeometry)
	{

		resetRelighting(false, false);
#pragma omp parallel for
		for (int im = 0; im < _linRadAdds.size(); im++) {

			for (auto& rd : _relightingDeltas) {
				if (rd.active) {
					_linRadAdds[im].toOpenCVnonConst() +=
						rd.rdlt[im].add->toOpenCV().mul(
							cv::Scalar(
								pow(2.0, rd.emissionExposure) * rd.emissionColor.x,
								pow(2.0, rd.emissionExposure) * rd.emissionColor.y,
								pow(2.0, rd.emissionExposure) * rd.emissionColor.z
							)
						);
					_linRadRms[im].toOpenCVnonConst() +=
						rd.rdlt[im].rm->toOpenCV().mul(
							cv::Scalar(
								pow(2.0, rd.emissionExposure) * rd.emissionColor.x,
								pow(2.0, rd.emissionExposure) * rd.emissionColor.y,
								pow(2.0, rd.emissionExposure) * rd.emissionColor.z
							)
						);
				}
			}
		}

		_TexturesRadianceAdd->updateFromImages(_linRadAdds);

		if (updateGeometry) {
			std::cout << "Updating geometry" << std::endl;
			sibr::Mesh::Ptr proxyTar(new sibr::Mesh(_proxies->proxy()));
			for (auto& rd : _relightingDeltas) {
				if (rd.active) {

					std::cout << "Adding mesh with " << rd.mesh->vertices().size() << " vertices" << std::endl;
					proxyTar->merge(*rd.mesh);
				}
			}
			_OR->updateTargetGeometry(proxyTar, _cams->inputCameras());
		}
	}

	void sibr::indoorRelighterIBRScene::updateRelighting(std::string lightName, float exposure, sibr::Vector4f emissionColor, bool switchState)
	{

		bool found = false;

		for (auto& rd : _relightingDeltas) {
			std::cout << std::string(lightName) << " " << std::string(rd.name) << std::endl;
		}

		for (auto& rd : _relightingDeltas) {

			if (lightName == rd.name) {
				std::cout << "Modifying " << std::string(lightName) << std::endl;
				rd.emissionColor.x = emissionColor.x();
				rd.emissionColor.y = emissionColor.y();
				rd.emissionColor.z = emissionColor.z();
				rd.emissionExposure = exposure;
				if (switchState)
					rd.active = !rd.active;
				found = true;
				break;
			}
		}

		if (found) {
			updateRelighting(switchState);
		}

	};

	void sibr::indoorRelighterIBRScene::generateInputBuffers(const Camera& renderCam, int deactivate, float exp)
	{

		_window->makeContextCurrent();

		std::cout << "-------------------" << std::endl;
		_OR->process(renderCam, deactivate, exp);
		glFinish();

	}

	void sibr::indoorRelighterIBRScene::renderReflexionIm(
		const Camera& renderCam,
		const sibr::ImageFloat1::Ptr depthMap,
		const sibr::ImageRGB32F::Ptr normalMap,
		std::vector<sibr::ImageRGB32F::Ptr>& mirrorIms,
		float exp
	) {


		if (!_raycaster.isInit()) {
			//Setup the raycaster
			_raycaster.init();
			_raycaster.addMesh(_proxies->proxy());
		}
		if (_faceVisibility.size() == 0)
			initFaceVisibility();

		const sibr::Vector2u resolution(depthMap->w(), depthMap->h());


		Eigen::Matrix3Xf camsPosVec(3, _cams->inputCameras().size());
		for (int im = 0; im < _cams->inputCameras().size(); im++) {
			camsPosVec.col(im) = _cams->inputCameras()[im]->position();
		}

		auto& cams = _cams->inputCameras();

		float noHitVal = 0.0;
		if (_lighting.type == "default") {
			noHitVal = 0.0;
		}

		std::vector<float> s1A = { 0.1,-0.1 };
		std::vector<float> s1C = { 1.0, 0.0 };
		std::vector<float> s2A = { -0.2,0.2,-0.2,0.2 };
		std::vector<float> s2C = { 0.125, 0.375, 0.625 , 0.875 };
		std::vector<std::future<void>> results(resolution.y());
		for (int j = 0; j < resolution.y(); j++) {
			results[j] = _pool.push(
				[&, j](int id) {

					std::random_device rd;
					std::mt19937 gen(rd());
					std::uniform_real_distribution<> distC(0.0, 1.0);

					for (int i = 0; i < resolution.x(); i += 8) {

						std::vector<std::array<sibr::Ray, 8>> raysMirror(3);
						std::vector<std::vector<int>> valid8Mirror(3, std::vector<int>(8, -1));

						for (int i_r = 0; i_r < 8; i_r++) {


							sibr::Vector2i pixelPos(i + i_r, j);
							float d = depthMap(pixelPos).x();
							sibr::Vector2f pos2dGL(2.0f * ((pixelPos.cast<float>() + sibr::Vector2f(0.5, 0.5)).cwiseQuotient(sibr::Vector2f(resolution.x(), resolution.y()))) - sibr::Vector2f(1, 1));  //to [-1,1]
							pos2dGL.y() = -pos2dGL.y();
							sibr::Vector3f hitPos = renderCam.unproject(sibr::Vector3f(pos2dGL.x(), pos2dGL.y(), d));

							sibr::Vector3f dir = (hitPos - renderCam.position()).normalized();
							sibr::Vector3f normal = normalMap(i + i_r, j).normalized();

							hitPos += 0.01f * _scale * normal;
							sibr::Vector3f mirrorDir = dir - 2 * normal.dot(dir) * normal;

							raysMirror[0][i_r] = sibr::Ray(hitPos, mirrorDir);

							sibr::Vector3f u = mirrorDir.cross(normal).normalized();
							sibr::Vector3f w = u.cross(mirrorDir).normalized();

							float theta = acos(normal.dot(mirrorDir));
							if (theta > M_PI_2 || d == -2.0) {
								valid8Mirror[0][i_r] = 0;
								valid8Mirror[1][i_r] = 0;
								valid8Mirror[2][i_r] = 0;
								continue;
							}

							float stdBase = 0.02;
							float stdFsnl = stdBase + (1.0f - normal.dot(mirrorDir)) * 0.08;

							std::normal_distribution<> distAngleU1(0, stdFsnl);
							std::normal_distribution<> distAngleW1(0, stdBase);
							float angleU1;
							do {
								angleU1 = distAngleU1(gen);
							} while (angleU1<theta - M_PI_2 || angleU1>M_PI_2);
							Eigen::AngleAxisf ru1(angleU1, u);
							Eigen::AngleAxisf rw1(distAngleW1(gen), w);

							std::normal_distribution<> distAngleU2(0, 3 * stdFsnl);
							std::normal_distribution<> distAngleW2(0, 3 * stdBase);
							float angleU2;
							do {
								angleU2 = distAngleU2(gen);
							} while (angleU2<theta - M_PI_2 || angleU2>M_PI_2);
							Eigen::AngleAxisf ru2(angleU2, u);
							Eigen::AngleAxisf rw2(distAngleW2(gen), w);


							raysMirror[1][i_r] = sibr::Ray(hitPos, rw1 * (ru1 * mirrorDir));
							raysMirror[2][i_r] = sibr::Ray(hitPos, rw2 * (ru2 * mirrorDir));

						}

						for (int s = 0; s < 3; s++) {
							std::array<RayHit, 8> rayhitsMirror = _raycaster.intersect8(raysMirror[s], valid8Mirror[s]);

							for (int i_r = 0; i_r < 8; i_r++) {
								if (valid8Mirror[s][i_r] != 0 && rayhitsMirror[i_r].hitSomething()) {

									sibr::Vector3f hitPosMirror = rayhitsMirror[i_r].ray().orig() + rayhitsMirror[i_r].ray().dir() * rayhitsMirror[i_r].dist();

									int geomID = rayhitsMirror[i_r].primitive().triID;

									int bestIm = -1;
									sibr::Vector2i mirrorPosImInt;
									chooseSample(cams, hitPosMirror, _nfPrecom, rayhitsMirror[i_r].ray().dir(), bestIm, mirrorPosImInt, _faceVisibility[geomID]);

									if (bestIm >= 0) {
										mirrorIms[s](i + i_r, j) = exp * (_linDiffs[bestIm](mirrorPosImInt) + _linVdeps[bestIm](mirrorPosImInt));
									}
									else {
										mirrorIms[s](i + i_r, j) = sibr::Vector3f(0.0, 0.0, 0.0);
									}
								}
								else {
									mirrorIms[s](i + i_r, j) = exp * sibr::Vector3f(noHitVal, noHitVal, noHitVal);
								}

							}
						}

					}
				});
		}
		for (int j = 0; j < resolution.y(); j++) {
			results[j].get();
		}

	}

	struct directSampler {

		std::function<void(sibr::Vector3f, sibr::Vector3f&, sibr::Vector3f&)> computeBasis =
			[](sibr::Vector3f dir, sibr::Vector3f& u1, sibr::Vector3f& u2) {
			dir = dir.normalized();
			if (abs(dir.z() > 0.01f)) {
				u1 = sibr::Vector3f(dir.z(), dir.z(), -dir.x() - dir.y()).stableNormalized();
			}
			else if (abs(dir.x()) > 0.01f) {
				u1 = sibr::Vector3f(-dir.y() - dir.z(), dir.x(), dir.x()).stableNormalized();
			}
			else {
				u1 = sibr::Vector3f(dir.y(), -dir.z() - dir.x(), dir.y()).stableNormalized();
			}
			u2 = (dir.cross(u1)).stableNormalized();

		};

		directSampler::directSampler(sibr::Vector3f n, bool addGlobalEmitter = true) {
			gen = std::mt19937(rd());
			dist = std::uniform_real_distribution<double>(0.0, 1.0);
			weights = std::vector<float>();
			angles = std::vector<float>();
			conesPD = std::vector<float>();
			dirs = std::vector<sibr::Vector3f>();
			_n = n.normalized();
			//weights for cosine sampling is 1;
			if (addGlobalEmitter) {
				weights.push_back(1.0f);
				dirs.push_back(_n);
				sibr::Vector3f u1, u2;
				computeBasis(_n, u1, u2);
				u1s.push_back(u1);
				u2s.push_back(u2);

				angles.push_back(M_PI_2);
				conesPD.push_back(1.0f / (2.0 * M_PI));
				sumWeights = 1.0f;
			}
			else {
				sumWeights = 0.0f;
			}


		};

		void directSampler::sample(sibr::Vector3f& ray, double& invPdf, float& theta) {

			if (weights.size() == 0) {
				SIBR_ERR;
			}
			//Choose the bin
			double b_r = dist(gen);
			int id_b = 0;
			float accWeights = 0;
			while (accWeights + weights[id_b] < b_r * sumWeights && id_b < weights.size() - 1) {
				accWeights += weights[id_b];
				id_b++;
			}

			float phi_dir = dist(gen) * 2.0f * M_PI;
			//Cosine weighted function
			float cosTheta_dir = 1.0 - (1.0 - cos(angles[id_b])) * dist(gen);
			float sinTheta_dir = sqrt(1.0 - pow(cosTheta_dir, 2));
			// The point is uniformly elected over a sphrical cap

			ray = cos(phi_dir) * sinTheta_dir * u1s[id_b] + sin(phi_dir) * sinTheta_dir * u2s[id_b] + cosTheta_dir * dirs[id_b];

			double pdf = 0;
			for (int c = 0; c < dirs.size(); c++) {
				if (ray.dot(dirs[c]) >= cos(angles[c]) - 0.000001f) {
					pdf += weights[c] * conesPD[c];
				}
			}

			//theta = std::min(M_PI_2, std::max(0.0, (double)acos(ray.dot(_n))));
			theta = std::max(0.0, (double)acos(ray.dot(_n)));

			pdf /= sumWeights;
			pdf = std::max(0.000001, pdf);

			invPdf = 1.0 / pdf;

			/*if (ray.dot(_n) <= 0) {
				invPdf = 0;
			}
			else {
				invPdf = 1.0 / pdf;
			}*/
		}

		void directSampler::addEmitter(float weight, sibr::Vector3f toEmitter, float radius) {
			if (weight > 0.0) {
				if (toEmitter.norm() > 1.05f * radius) {
					//Check if emitter is visible by the surface
					sibr::Vector3f toEmitterNormalized = toEmitter.normalized();
					float angleCone = asin(radius / toEmitter.norm());

					weights.push_back(weight);
					dirs.push_back(toEmitterNormalized);
					sibr::Vector3f u1, u2;
					computeBasis(toEmitterNormalized, u1, u2);
					u1s.push_back(u1);
					u2s.push_back(u2);
					angles.push_back(angleCone);
					/*
										//correcting factor computation, some of the cone may not be visible to the normal.
										float visiblePortion;
										float h = toEmitter.norm() * tan(asin(toEmitterNormalized.dot(_n)));
										if (h >= radius) {
											visiblePortion = 1;
										}
										else if (h >= 0 && h < radius) {
											float alpha = h / radius;
											visiblePortion = 1 - (acos(alpha) / M_PI - sqrt(1 - pow(alpha, 2)) * alpha / M_PI);
										}
										else if (h < 0 && abs(h) <= radius) {
											float alpha = abs(h) / radius;
											visiblePortion = acos(alpha) / M_PI - sqrt(1 - pow(alpha, 2)) * alpha / M_PI;
										}
										else {
											visiblePortion = 0;
										}
										*/

					float area = 2.0 * M_PI * (1.0 - cos(angleCone));
					conesPD.push_back(1.0 / area);
					/*
					if (visiblePortion > 0) {
						//conesPD.push_back(visiblePortion / area);
						//conesPD.push_back(1.0/visiblePortion);
					}
					else {
						//Arbitray value here
						conesPD.push_back(1.0f);
					}*/

					sumWeights += weight;
				}
				else if (weights.size() == 0) {
					weights.push_back(weight);
					sumWeights += weight;
					dirs.push_back(_n);
					sibr::Vector3f u1, u2;
					computeBasis(_n, u1, u2);
					u1s.push_back(u1);
					u2s.push_back(u2);

					angles.push_back(M_PI_2);
					conesPD.push_back(1.0f / (2.0 * M_PI));
				}

			}
			else {
				std::cout << "Error : negative weight" << std::endl;
				SIBR_ERR;
			}

		};

		sibr::Vector3f _n;
		std::vector<float> weights;
		//Direction and basis vector
		std::vector<sibr::Vector3f> dirs;
		std::vector<sibr::Vector3f> u1s;
		std::vector<sibr::Vector3f> u2s;
		std::vector<float> angles;
		std::vector<float> conesPD;
		float sumWeights;
		std::random_device rd;
		std::mt19937 gen;
		std::uniform_real_distribution<double> dist;

	};

	/*struct importanceSampler {


		importanceSampler::importanceSampler(int res) {

			impRes = res;
			impMap = sibr::ImageFloat1(impRes, impRes, 1.0);
			impCount = sibr::ImageInt1(impRes, impRes, 0);
			totalLines = std::vector<float>(impRes, impRes);
			totalProba = impRes * impRes;
			gen = std::mt19937(rd());
			dist = std::uniform_real_distribution<float>(0.0, 1.0);

		};

		void importanceSampler::fromSphere(
			const sibr::Vector3f& lightPos,
			const float& lightRadius,
			const sibr::Vector3f& hitPos,
			const sibr::Vector3f& normal,
			const sibr::Vector3f& u1,
			const sibr::Vector3f& u2) {
			//Here we modify the probability Map
			sibr::Vector3f toLight = (lightPos - hitPos).normalized();
			float d2Center = (lightPos - hitPos).norm();
			if (d2Center > lightRadius) {
				float angleSemiCone = asin(lightRadius / d2Center);
				sibr::Vector3f vecPixel;
				for (int cT_y = 0; cT_y < impRes; cT_y++) {
					for (int phi_x = 0; phi_x < impRes; phi_x++) {
						float thetaPix = M_PI_2 * (cT_y + 0.5) / impRes;
						float cosThetaPix = cos(thetaPix);
						float sinThetaPix = sin(thetaPix);
						float phiPix = 2.0*M_PI*(phi_x + 0.5) / impRes;
						vecPixel = cos(phiPix) * sinThetaPix * u1 + sin(phiPix) * sinThetaPix * u2 + cosThetaPix * normal;
						float std = std::max((float)M_PI / impRes, angleSemiCone);
						//The arccos is the spherical distance between the two vectors
						if (toLight.dot(vecPixel) > 0) {
							impMap(phi_x, cT_y).x() =
								1.f*(1.0 / (sqrt(2.0f*M_PI)*std))*exp(-0.5*pow(acos(std::max(0.0f, toLight.dot(vecPixel))) / std, 2));
						}
					}
				}
			}

			update();

		};

		void importanceSampler::update() {
			totalLines = std::vector<float>(impRes, 0);
			totalProba = 0;
			for (int cT_y = 0; cT_y < impRes; cT_y++) {
				float & lineProba = totalLines[cT_y];
				for (int phi_x = 0; phi_x < impRes; phi_x++) {
					totalProba += impMap(phi_x, cT_y).x();
					lineProba += impMap(phi_x, cT_y).x();
				}
			}
		};

		sibr::Vector2i importanceSampler::sample(
			float & theta,
			float & phi,
			float & invPdf
		)
		{
			float r = dist(gen);
			float sumProbaLines = 0;
			int cT_y = 0;
			while (sumProbaLines + totalLines[cT_y] < r*totalProba && cT_y < impRes - 1) {
				sumProbaLines += totalLines[cT_y];
				cT_y++;
			}
			float sumProba = sumProbaLines;
			theta = M_PI_2 * (cT_y + dist(gen)) / float(impRes);
			int phi_x;
			for (phi_x = 0; phi_x < impRes; phi_x++) {
				float p = impMap(phi_x, cT_y).x();
				if (sumProba + p >= r * totalProba) {
					phi = ((phi_x + (r*totalProba - sumProba) / p) / impRes)*2.0*M_PI;
					invPdf = M_PI * totalProba / (p*impRes*impRes);
					break;
				}
				else {
					sumProba += p;
				}
			}

			return sibr::Vector2i(phi_x, cT_y);

		}

		void update(sibr::Vector2i xy, sibr::Vector3f color) {

			float oldVal = impMap(xy).x();
			float lum = 0.2126*color.x() + 0.7152*color.y() + 0.0722*color.z();
			float newVal;
			if (impCount(xy).x() == 0) {
				newVal = lum;
			}
			else {
				newVal = (oldVal*impCount(xy).x() + lum) / (impCount(xy).x() + 1);
			}
			totalProba += newVal - oldVal;
			totalLines[xy.y()] += newVal - oldVal;
			impMap(xy).x() = newVal;
			impCount(xy).x()++;
		};

		sibr::ImageFloat1 impMap;
		sibr::ImageInt1 impCount;
		std::vector<float> totalLines;
		float totalProba;
		int impRes;
		std::random_device rd;
		std::mt19937 gen;
		std::uniform_real_distribution<float> dist;

	};*/


	void sibr::indoorRelighterIBRScene::renderRadianceIm(
		const Camera& renderCam,
		const sibr::ImageFloat1::Ptr& depthMap,
		const sibr::ImageRGB32F::Ptr& normalMap,
		sibr::ImageRGB32F::Ptr& radIm,
		int lightId
	) {
		if (!_raycaster.isInit()) {
			//Setup the raycaster
			_raycaster.init();
			_raycaster.addMesh(_proxies->proxy());
		}
		if (_camsOrder.size() == 0)
			initCamsOrder();
		if (!_dmInit) {
			initDM();
		}

		float perCent = 0.0;
		int numRayPacket = 8;

		if (lightId < 0 || _args.synthetic)
			numRayPacket = 16;


		auto& cams = _cams->inputCameras();

		const sibr::Vector2u resolution(depthMap->w(), depthMap->h());

		const float incr = 100.0f / resolution.y();

		float noHitVal = 0.0;
		if (_lighting.type == "default") {
			noHitVal = 0.0;
		}

		int bksz = 8;
		//#pragma omp parallel for schedule(dynamic) shared(perCent,radIm)
		std::vector<std::future<void>> results;
		for (int j = 0; j < resolution.y(); j += bksz) {
			for (int i = 0; i < resolution.x(); i += bksz) {
				results.push_back(_pool.push(
					[&, i, j](int id) {


						for (int j_r = 0; j_r < bksz; j_r++) {
							for (int i_r = 0; i_r < bksz; i_r++) {
								if (!(i + i_r < resolution.x()) || !(j + j_r < resolution.y())) { continue; }
								if (depthMap(i + i_r, j + j_r).x() == -2.0) {
									radIm(i + i_r, j + j_r) = sibr::Vector3f(noHitVal, noHitVal, noHitVal);
									continue;
								}

								sibr::Vector2i pixelPos(i + i_r, j + j_r);
								float d = depthMap(pixelPos).x();
								sibr::Vector2f pos2dGL(2.0f * ((pixelPos.cast<float>() + sibr::Vector2f(0.5, 0.5)).cwiseQuotient(sibr::Vector2f(resolution.x(), resolution.y()))) - sibr::Vector2f(1, 1));  //to [-1,1]
								pos2dGL.y() = -pos2dGL.y();
								sibr::Vector3f hitPos = renderCam.unproject(sibr::Vector3f(pos2dGL.x(), pos2dGL.y(), d));

								sibr::Vector3f normal = normalMap(i + i_r, j + j_r).normalized();

								hitPos += 0.005f * _scale * normal;

								bool hemiSampling = lightId < 0;

								directSampler DP(normal, hemiSampling);
								if (_args.synthetic && _lighting.type == "sphere") {
									DP.addEmitter(1.0f, (_lighting.pos - hitPos), _lighting.radius + 0.01f * _scale);
								}
								else if (!_args.synthetic && lightId >= 0) {

									DP.addEmitter(1.0f, (_lights[lightId].xyz() - hitPos), _lights[lightId].w());
								}


								int rayNum = 0;
								float weightRay = 0;
								sibr::Vector3f pixelVal(0, 0, 0);
								int spp = 0;
								int badSpp = 0;

								std::array<float, 8> thetas;
								std::array<double, 8> invPdf;
								std::array<sibr::Vector2i, 8> impPos;
								sibr::Vector3f dirRay;

								for (int rp = 0; rp < numRayPacket; rp++) {

									std::array<sibr::Ray, 8> raysSecond;

									for (int rs = 0; rs < 8; rs++) {

										rayNum++;
										DP.sample(dirRay, invPdf[rs], thetas[rs]);
										raysSecond[rs] = sibr::Ray(hitPos, dirRay);
									}

									std::array<sibr::RayHit, 8> rayHitsSecond = _raycaster.intersect8(raysSecond, std::vector<int>(8, -1), 0.004f * _scale);

									for (int rs = 0; rs < 8; rs++) {


										if (thetas[rs] > M_PI_2) {
											spp++;
											continue;
										}

										if (rayHitsSecond[rs].hitSomething()) {
											sibr::Vector3f hitPosSecond = hitPos + raysSecond[rs].dir() * rayHitsSecond[rs].dist();

											int geomID = rayHitsSecond[rs].primitive().triID;
											sibr::Vector3f interp = rayHitsSecond[rs].interpolateUV();

											int bestIm = -1;
											sibr::Vector2i bestSecondPosImInt(0, 0);
											chooseNormalSample(cams, hitPosSecond, bestIm, bestSecondPosImInt, _camsOrder[geomID]);

											if (bestIm >= 0) {
												if (_args.synthetic) {
													pixelVal += invPdf[rs] *
														std::max(0.0f, cos(thetas[rs])) *
														(_linDiffs[bestIm](bestSecondPosImInt) + _linVdeps[bestIm](bestSecondPosImInt));
												}
												else {
													if (_labelsIm[bestIm](bestSecondPosImInt).x() == lightId) {
														if (lightId >= 0)
															pixelVal += invPdf[rs] * std::max(0.0f, cos(thetas[rs])) * sibr::Vector3f(1.0, 1.0, 1.0);// invPdf[rs] *
														else
															pixelVal += invPdf[rs] *
															std::max(0.0f, cos(thetas[rs])) *
															(_linDiffs[bestIm](bestSecondPosImInt) + _linVdeps[bestIm](bestSecondPosImInt));

													}
												}

											}
											spp++;
										}
										else {
											spp++;
										}
									}
								}

								if (spp > 0) {
									//Don't forget to divide by Pi to take into account the over integral normalization.
									pixelVal /= M_PI * spp;// *(1.0 - (float)badSpp / (numRayPacket * 8));
									//pixelVal /=  spp;// *(1.0 - (float)badSpp / (numRayPacket * 8));
								}

								radIm(i + i_r, j + j_r) = pixelVal;

							}
						}

					}));

			}
			/*#pragma  omp flush(perCent)
			#pragma omp critical
							perCent += incr;
			#pragma omp critical
							std::cout << perCent << "\r" << std::flush;*/

		}
		for (auto& r : results) {
			r.get();
		}
		//radIm = _denoiser->denoise(*radIm);

	}



	void sibr::indoorRelighterIBRScene::initDM() {

		//Render the depth maps
		std::cout << "Generating Depth Maps" << std::endl;
		for (int c = 0; c < _cams->inputCameras().size(); c++) {
			sibr::ImageFloat1::Ptr d(new sibr::ImageFloat1(_linDiffs[c]->w(), _linDiffs[c]->h()));
			sibr::DepthRenderer DR(_linDiffs[c]->w(), _linDiffs[c]->h());
			DR.render(*_cams->inputCameras()[c], _proxies->proxy());
			DR._depth_RT->readBack(*d);
			_dm.push_back(d);
		}

		_dmInit = true;

	};

	void sibr::indoorRelighterIBRScene::colorizeMesh(
		sibr::Mesh::Ptr mesh,
		std::vector<sibr::ImageRGB32F::Ptr> imgs,
		const std::vector<sibr::InputCamera::Ptr>& cams,
		double maxVal
	) {

		if (!_dmInit) {
			initDM();
		}

		sibr::Mesh::Colors colors(mesh->vertices().size());

		std::vector<sibr::ImageL32F> imOcclusions;
		for (int im = 0; im < cams.size(); im++) {

			imOcclusions.push_back(_occs[im]->resized(imgs[im]->w(), imgs[im]->h()));

		}
#pragma omp parallel for
		for (int v_id = 0; v_id < _proxies->proxy().vertices().size(); v_id++) {
			auto& v = _proxies->proxy().vertices()[v_id];
			double w = 0;
			sibr::Vector3d val(0, 0, 0);
			for (int im = 0; im < cams.size(); im++) {

				sibr::Vector3f texPos = cams[im]->projectImgSpaceInvertY(v);
				sibr::Vector3f dmPos = cameras()->inputCameras()[im]->projectImgSpaceInvertY(v);

				if (imgs[im]->isInRange(floor(texPos.x()), floor(texPos.y())) &&
					_dm[im]->isInRange(floor(dmPos.x()), floor(dmPos.y())) &&
					_dmTex[im](floor(texPos.x()), floor(texPos.y())).x() != -2.0
					) {
					//Depth Test

					float dCam = (v - cams[im]->position()).norm();
					float dReproj = (v -
						cameras()->inputCameras()[im]->unprojectImgSpaceInvertY(
							sibr::Vector2i(floor(dmPos.x()), floor(dmPos.y())), _dm[im](floor(dmPos.x()),
								floor(dmPos.y())).x())
						).norm();

					if (dReproj / dCam < 0.005f && imgs[im](floor(texPos.x()), floor(texPos.y())) != sibr::Vector3f(0.0f, 0.0f, 0.0f)) {

						double l = 1.0 / (dCam * (pow(imOcclusions[im](floor(texPos.x()), floor(texPos.y())).x(), 2) + 0.01f));
						if (l > 0.000001f) {
							w += l;
							sibr::Vector3d currVal = imgs[im](floor(texPos.x()), floor(texPos.y())).cast<double>();
							for (int c = 0; c < 3; c++) {
								currVal[c] = std::max(0.0, std::min(maxVal, currVal[c]));
								if (currVal[c] != currVal[c]) {
									currVal[c] = 0;
								}
							}
							val += l * currVal;
						}
					}
				}
			}
			if (w > 0)
				val /= w;
			for (int c = 0; c < 3; c++) {
				val[c] = std::max(0.0, std::min(maxVal, val[c]));
				if (val[c] != val[c]) {
					val[c] = 0;
				}
			}
			colors[v_id] = val.cast<float>();
		}

		mesh->colors(colors);


	}

	void indoorRelighterIBRScene::normalBasedSurfaceSmoothing(sibr::Mesh& mesh)
	{

		auto normalsOri = mesh.normals();
		mesh.generateSmoothNormals(1);
		auto normalsSmooth = mesh.normals();

		for (int it = 0; it < 20; it++) {
			std::vector<sibr::Vector3f> deltas(mesh.vertices().size(), sibr::Vector3f(0, 0, 0));
			std::vector<float> normalsDeltas(mesh.vertices().size(), 0);
			std::vector<int> deltasCount(mesh.vertices().size(), 0);

			for (const auto& f : mesh.triangles()) {

				sibr::Vector3f center = (mesh.vertices()[f[0]] + mesh.vertices()[f[1]] + mesh.vertices()[f[2]]) / 3;
				for (int i = 0; i < 3; i++) {
					deltas[f[i]] += -(mesh.vertices()[f[i]] - center).dot(normalsOri[f[i]]) * normalsOri[f[i]];
					normalsDeltas[f[i]] += 1.0 - normalsSmooth[f[i]].dot(normalsSmooth[f[(i + 1) % 3]]);
					normalsDeltas[f[i]] += 1.0 - normalsSmooth[f[i]].dot(normalsSmooth[f[(i + 2) % 3]]);
					deltasCount[f[i]]++;
				}

			}
			for (int vi = 0; vi < mesh.vertices().size(); vi++) {

				auto& v = mesh.vertices()[vi];
				if (deltasCount[vi] > 0) {

					deltas[vi] = 0.5 * exp(-pow(normalsDeltas[vi] / 0.5, 2)) / deltasCount[vi] * deltas[vi];
				}
			}
			for (const auto& f : mesh.triangles()) {

				sibr::Vector3f bias = (deltas[f[0]] + deltas[f[1]] + deltas[f[2]]) / 3;
				for (int i = 0; i < 3; i++) {
					deltas[f[i]] -= 0.25 * bias;
				}

			}
			for (int vi = 0; vi < mesh.vertices().size(); vi++) {

				auto& v = mesh.vertices()[vi];

				deltas[vi] = deltas[vi] + v;
			}


			mesh.vertices(deltas);
		}

		mesh.normals(normalsOri);


	}

	void sibr::indoorRelighterIBRScene::detectLightSources(float threshold) {


		if (!_dmInit) {
			initDM();
		}


		std::vector<int> numLabels;

		sibr::Mesh testMesh;

		struct Node {
			std::pair<int, int> imLabel;
			std::vector<Node*> connected;
			bool visited;
		};

		std::map<int, std::map<int, Node>> graph;

		for (int im = 0; im < cameras()->inputCameras().size(); im++) {

			sibr::InputCamera cam = *cameras()->inputCameras()[im];
			int h = _linDiffs[im]->h();
			int w = _linDiffs[im]->w();
			sibr::ImageL8 mask(w, h, 0);
			sibr::ImageInt1::Ptr labels = std::make_shared<sibr::ImageInt1>(w, h, 0);

#pragma omp parallel for
			for (int j = 0; j < _linDiffs[im]->h(); j++) {
				for (int i = 0; i < _linDiffs[im]->w(); i++) {

					if (_linDiffs[im](i, j).x() > threshold ||
						_linDiffs[im](i, j).y() > threshold ||
						_linDiffs[im](i, j).z() > threshold) {

						mask(i, j).x() = 255;

					}

				}

			}


			cv::morphologyEx(mask.toOpenCV(), mask.toOpenCVnonConst(), 2,
				cv::getStructuringElement(cv::MORPH_ELLIPSE, cv::Size(3, 3))
			);
			int numLabelIm = cv::connectedComponents(mask.toOpenCV(), labels->toOpenCVnonConst(), 8, CV_32S);
			numLabels.push_back(numLabelIm);
			_labelsIm.push_back(labels);

			//show(coloredClass(_labelsIm[im]));

			for (int l = 1; l < numLabelIm; l++) {
				graph[im][l] = { std::make_pair(im,l),std::vector<Node*>{} ,false };
			}

		}

		sibr::Mesh pc;
		float step = 0.02f * _scale;

		if (!pc.load(_args.dataset_path.get() + "/pc_light.ply")) {
			auto bbox = proxies()->proxy().getBoundingBox();


			int xStep = 0;
			int yStep = 0;
			int zStep = 0;
			for (float x = bbox.min().x(); x < bbox.max().x(); x += step) { xStep++; }
			for (float y = bbox.min().y(); y < bbox.max().y(); y += step) { yStep++; }
			for (float z = bbox.min().z(); z < bbox.max().z(); z += step) { zStep++; }



			std::map<sibr::Vector3i, double> numBurntxp;
			std::map<sibr::Vector3i, double> numBurnt;

#pragma omp parallel for schedule(dynamic)
			for (int im = 0; im < cameras()->inputCameras().size(); im++) {
				std::cout << im << std::endl;

				if (numLabels[im] < 2) {
					continue;
				}

				sibr::InputCamera::Ptr cam = cameras()->inputCameras()[im];
				int h = _linDiffs[im]->h();
				int w = _linDiffs[im]->w();

				sibr::Vector3f camPos = cam->position();

				std::vector<bool> visited(xStep * yStep * zStep, false);

				for (int j = 0; j < _linDiffs[im]->h(); j++) {
					for (int i = 0; i < _linDiffs[im]->w(); i++) {

						if (_labelsIm[im](i, j).x() > 0) {


							sibr::Vector3f unproj = cam->unprojectImgSpaceInvertY(
								sibr::Vector2i(i, j),
								_dm[im](i, j).x()
							);
							sibr::Vector3f dir = (unproj - camPos).normalized();

							int ceilStep = ceil((camPos - unproj).norm() / (0.5 * step));

							for (float t = 0; t < (camPos - unproj).norm(); t += (camPos - unproj).norm() / ceilStep) {

								sibr::Vector3f point = camPos + t * dir;

								int xc = floor((point.x() - bbox.min().x()) / step);
								int yc = floor((point.y() - bbox.min().y()) / step);
								int zc = floor((point.z() - bbox.min().z()) / step);

								for (int dx = 0; dx <= 1; dx++)
									if (xc + dx < xStep && xc >= 0)
										for (int dy = 0; dy <= 1; dy++)
											if (yc + dy < yStep && yc >= 0)
												for (int dz = 0; dz <= 1; dz++)
													if (zc + dz < zStep && zc >= 0) {


														int p_id = (xc + dx) * zStep * yStep + (yc + dy) * zStep + (zc + dz);

														sibr::Vector3i gridCoord(xc + dx, yc + dy, zc + dz);

														if (visited[p_id]) {
															continue;
														}

														sibr::Vector3f p = bbox.min() + step * gridCoord.cast<float>();

														sibr::Vector3f proj = cam->projectImgSpaceInvertY(p);
														sibr::Vector2i projPos(floor(proj.x()), floor(proj.y()));


														if (_dm[im]->isInRange(projPos)) {

															//sibr::Vector3f meshPos = cam->unprojectImgSpaceInvertY(projPos, _dm[im](projPos).x());
															if (_dm[im](projPos).x() > proj.z()) {
																float distCam = (p - camPos).dot(dir);
																if (distCam > 0) {
																	sibr::Vector3f projLine = camPos + distCam * dir;

																	if ((p - projLine).norm() < 0.708 * step) {
																		visited[p_id] = true;
																	}
																}
															}

														}

													}

							}

						}

					}

				}

				int v_id = 0;
				for (const bool& v : visited) {

					if (v) {
						int zc = v_id % zStep;
						int yc = (v_id - zc) / zStep % yStep;
						int xc = (v_id - yc * zStep - zc) / (zStep * yStep);
						sibr::Vector3i gridCoord(xc, yc, zc);

#pragma omp critical
						{
							numBurnt[gridCoord]++;
						}
					}

					v_id++;

				}

			}

			sibr::Mesh::Vertices pointsFinal;
			sibr::Mesh::Colors colorsFinal;
			for (std::pair < sibr::Vector3i, double > element : numBurnt) {
				if (element.second > 4) {

					sibr::Vector3f p = bbox.min() + step * element.first.cast<float>();

					float numVisible = 0;
					for (int im = 0; im < cameras()->inputCameras().size(); im++) {

						const auto& cam = cameras()->inputCameras()[im];
						sibr::Vector3f proj = cam->projectImgSpaceInvertY(p);
						sibr::Vector2i projPos(floor(proj.x()), floor(proj.y()));
						if (_dm[im]->isInRange(projPos)) {
							//sibr::Vector3f meshPos = cam->unprojectImgSpaceInvertY(projPos, _dm[im](projPos).x());
							if (_dm[im](projPos).x() > proj.z())
								numVisible++;
						}

						int p_id = element.first.x() * zStep * yStep + element.first.y() * zStep + element.first.z();


					}

					if (numVisible - element.second <= std::max(8.0, 0.5 * numVisible)) {

						if (numVisible < element.second) {
							int p_id = element.first.x() * zStep * yStep + element.first.y() * zStep + element.first.z();
							std::cout << "Error " << p_id << " - " << numVisible << " & " << element.second << std::endl;
						}

						pointsFinal.push_back(p);
						colorsFinal.push_back(sibr::Vector3f(0, std::min(element.second / numVisible, 1.0), 0));
					}
				}
			}

			pc.vertices(pointsFinal);
			pc.colors(colorsFinal);
			pc.save(_args.dataset_path.get() + "/pc_light.ply", true);
		}

		struct lightToMerge {
			sibr::Vector3f pos;
			float radius;
			std::vector<sibr::Vector3f> points;
		};

		std::vector<lightToMerge> lightsToMerge;
		for (const auto& p : pc.vertices()) {
			lightsToMerge.push_back({ p,step,{p} });
		}

		bool foundMerge = true;
		int count = 0;
		while (foundMerge) {
			foundMerge = false;
			for (int li = 0; li < lightsToMerge.size() - 1; li++) {
				for (int lj = li + 1; lj < lightsToMerge.size(); lj++) {

					auto& li2m = lightsToMerge[li];
					auto& lj2m = lightsToMerge[lj];

					if ((li2m.pos - lj2m.pos).norm() < li2m.radius + lj2m.radius) {
						//Merge
						sibr::Vector3f mergedPos;
						int numPoint = 0;
						for (const auto& p : li2m.points) {
							mergedPos += p;
							numPoint++;
						}
						for (const auto& p : lj2m.points) {
							mergedPos += p;
							numPoint++;
						}
						mergedPos /= numPoint;


						float mergedRadius = step;
						for (const auto& p : li2m.points) {
							mergedRadius = std::max((p - mergedPos).norm() + step, mergedRadius);
						}
						for (const auto& p : lj2m.points) {
							mergedRadius = std::max((p - mergedPos).norm() + step, mergedRadius);
						}

						std::vector<sibr::Vector3f> mergedPoints;
						mergedPoints.reserve(li2m.points.size() + lj2m.points.size()); // preallocate memory
						mergedPoints.insert(mergedPoints.end(), li2m.points.begin(), li2m.points.end());
						mergedPoints.insert(mergedPoints.end(), lj2m.points.begin(), lj2m.points.end());

						lightToMerge merged = { mergedPos,mergedRadius,mergedPoints };
						lightsToMerge.erase(lightsToMerge.begin() + lj);
						lightsToMerge.erase(lightsToMerge.begin() + li);
						lightsToMerge.push_back(merged);

						foundMerge = true;
						count++;
						break;
					}
				}
				if (foundMerge)
					break;
			}
		}


		sibr::Mesh lightsVol;
		for (const auto& l : lightsToMerge) {
			if (l.points.size() > 3) {
				lightsVol.merge(*sibr::Mesh::getSphereMesh(l.pos, l.radius, false, 20));
				_lights.push_back(sibr::Vector4f(l.pos.x(), l.pos.y(), l.pos.z(), l.radius));
			}
		}
		lightsVol.save(_args.dataset_path.get() + "/volumetric_lights.ply", true);


		//#pragma omp parallel for schedule(dynamic)
		for (int im = 0; im < cameras()->inputCameras().size(); im++) {
			std::cout << im << std::endl;

			sibr::InputCamera::Ptr cam = cameras()->inputCameras()[im];
			int h = _linDiffs[im]->h();
			int w = _linDiffs[im]->w();

			sibr::Vector3f camPos = cam->position();

			for (int j = 0; j < _linDiffs[im]->h(); j++) {
				for (int i = 0; i < _linDiffs[im]->w(); i++) {

					if (_labelsIm[im](i, j).x() > 0) {


						sibr::Vector3f unproj = cam->unprojectImgSpaceInvertY(
							sibr::Vector2i(i, j),
							_dm[im](i, j).x()
						);

						int l_id = 0;
						bool belongsToLight = false;
						for (sibr::Vector4f l : _lights) {
							if ((unproj - l.xyz()).norm() < l.w()) {

								_labelsIm[im](i, j).x() = l_id;
								belongsToLight = true;
								break;

							}
							l_id++;
						}

						if (!belongsToLight)
							_labelsIm[im](i, j).x() = -1;

					}
					else {
						_labelsIm[im](i, j).x() = -1;
					}

				}
			}

			//show(coloredClass(_labelsIm[im]));
		}

		/*
				int imDone = 0;
				//Iterate over pairs of images to compute link between labels
		#pragma omp parallel for
				for (int im0 = 0; im0 < cameras()->inputCameras().size(); im0++) {

					if (numLabels[im0] < 2) {
		#pragma omp atomic
						imDone++;
						continue;
					}

					sibr::InputCamera cam0 = cameras()->inputCameras()[im0];

					for (int im1 = 0; im1 < cameras()->inputCameras().size(); im1++) {
						if (numLabels[im1] < 2)
							continue;

						//std::cout << "im0 " << im0 << " im1 " << im1 << "\n";

						sibr::InputCamera cam1 = cameras()->inputCameras()[im1];

						std::vector<std::vector<int>> connectFwd(numLabels[im0], std::vector<int>(numLabels[im1], 0));
						std::vector<int> labelCount0(numLabels[im0], 0);
						//forward projection im0->im1
						for (int j = 0; j < _linDiffs[im0]->h(); j++) {
							for (int i = 0; i < _linDiffs[im0]->w(); i++) {
								//Test if pixel belongs to burnt area
								if (_labelsIm[im0](i, j).x() > 0) {

									labelCount0[_labelsIm[im0](i, j).x()]++;

									sibr::Vector3f unproj = cam0.unprojectImgSpaceInvertY(
										sibr::Vector2i(i, j),
										_dm[im0](i, j).x()
									);

									sibr::Vector2f reproj = cam1.projectImgSpaceInvertY(unproj).xy();
									sibr::Vector2i reprojInt(std::floor(reproj.x()), std::floor(reproj.y()));

									if (_labelsIm[im1]->isInRange(reprojInt) && _labelsIm[im1](reprojInt).x() > 0) {

										connectFwd[_labelsIm[im0](i, j).x()][_labelsIm[im1](reprojInt).x()] ++;


									}




								}

							}

						}

						std::vector<std::vector<int>> connectBwd(numLabels[im1], std::vector<int>(numLabels[im0], 0));
						std::vector<int> labelCount1(numLabels[im1], 0);
						//Backward projection im1->im0
						for (int j = 0; j < _linDiffs[im1]->h(); j++) {
							for (int i = 0; i < _linDiffs[im1]->w(); i++) {
								//Test if pixel belongs to burnt area
								if (_labelsIm[im1](i, j).x() > 0) {

									labelCount1[_labelsIm[im1](i, j).x()]++;

									sibr::Vector3f unproj = cam1.unprojectImgSpaceInvertY(
										sibr::Vector2i(i, j),
										_dm[im1](i, j).x()
									);

									sibr::Vector2f reproj = cam0.projectImgSpaceInvertY(unproj).xy();
									sibr::Vector2i reprojInt(floor(reproj.x()), floor(reproj.y()));

									if (_labelsIm[im0]->isInRange(reprojInt) && _labelsIm[im0](reprojInt).x() > 0) {

										connectBwd[_labelsIm[im1](i, j).x()][_labelsIm[im0](reprojInt).x()] ++;

									}



								}

							}

						}
		#pragma omp critical
						{
							for (int l0 = 1; l0 < numLabels[im0]; l0++) {
								for (int l1 = 1; l1 < numLabels[im1]; l1++) {


									//std::cout << "      l0 " << l0 << " (" << connectFwd[l0][l1] << "/" << labelCount0[l0] << ")" <<
									//	" l1 " << l1 << " (" << connectBwd[l1][l0] << "/" << labelCount1[l1] << ")" << "\n";

									if (connectFwd[l0][l1] > 0.1 * labelCount0[l0] ||
										connectBwd[l1][l0] > 0.1 * labelCount1[l1]
										) {

										graph[im0][l0].connected.push_back(&graph[im1][l1]);
										graph[im1][l1].connected.push_back(&graph[im0][l0]);

									}
								}
							}

						}


					}

		#pragma omp atomic
					imDone++;

					std::cout << imDone << "/" << cameras()->inputCameras().size() << std::endl;

				}

				int mvLabel = 0;
				std::map<int, std::map<int, int>> connectedLabel;

				std::function<void(Node&)> visitCycle = [&mvLabel, &connectedLabel, &visitCycle](Node& node) {

					connectedLabel[node.imLabel.first][node.imLabel.second] = mvLabel;
					node.visited = true;
					for (Node* nc : node.connected) {

						if (!nc->visited)
							visitCycle(*nc);
					}

				};

				for (int im = 0; im < cameras()->inputCameras().size(); im++) {
					for (int l = 1; l < numLabels[im]; l++) {

						if (!graph[im][l].visited) {
							visitCycle(graph[im][l]);
							mvLabel++;
						}

					}
				}

				std::vector<std::vector<sibr::Vector3f>> lightPoints(mvLabel);
				for (int im = 0; im < cameras()->inputCameras().size(); im++) {

					if (numLabels[im] > 1) {
						//std::cout << "im " << im << "\n";

						sibr::InputCamera cam = cameras()->inputCameras()[im];
						int h = _linDiffs[im]->h();
						int w = _linDiffs[im]->w();
						sibr::ImageL8 mask(w, h);

						for (int j = 0; j < _linDiffs[im]->h(); j++) {
							for (int i = 0; i < _linDiffs[im]->w(); i++) {

								if (_labelsIm[im](i, j).x() > 0) {

									int pixLabel = connectedLabel[im][_labelsIm[im](i, j).x()];
									_labelsIm[im](i, j).x() = pixLabel;

									lightPoints[pixLabel].push_back(
										cam->unprojectImgSpaceInvertY(
											sibr::Vector2i(i, j),
											_dm[im](i, j).x()
										)
									);

								}
								else {
									_labelsIm[im](i, j).x() = -1;
								}

							}

						}

						show(coloredClass(_labelsIm[im]));
					}
					else {

						for (int j = 0; j < _linDiffs[im]->h(); j++) {
							for (int i = 0; i < _linDiffs[im]->w(); i++) {

								_labelsIm[im](i, j).x() = -1;

							}
						}


					}


				}

				sibr::Vector3f center;
				float radius;
				testMesh.vertices(lightPoints[0]);
				testMesh.getBoundingSphere(center, radius, false, true);
				_lights.push_back(sibr::Vector4f(center.x(), center.y(), center.z(), radius));

				testMesh.save(_args.dataset_path.get() + "/lightTest.ply");*/

	};

	std::vector<sibr::Vector4f> sibr::indoorRelighterIBRScene::estimateLight()
	{

		if (!_dmInit) {
			initDM();
		}

		auto& mesh = _proxies->proxyPtr();
		const auto& cams = _cams->inputCameras();

		sibr::Mesh::Colors colors(mesh->vertices().size());

#pragma omp parallel for
		for (int v_id = 0; v_id < _proxies->proxy().vertices().size(); v_id++) {
			auto& v = _proxies->proxy().vertices()[v_id];
			double w = 0;
			sibr::Vector3d val(0, 0, 0);
			for (int im = 0; im < cams.size(); im++) {

				sibr::Vector3f texPos = cams[im]->projectImgSpaceInvertY(v);
				sibr::Vector3f dmPos = cameras()->inputCameras()[im]->projectImgSpaceInvertY(v);

				if (_linDiffs[im]->isInRange(floor(texPos.x()), floor(texPos.y())) &&
					_dm[im]->isInRange(floor(dmPos.x()), floor(dmPos.y()))
					) {
					//Depth Test

					float dCam = (v - cams[im]->position()).norm();
					float dReproj = (v -
						cameras()->inputCameras()[im]->unprojectImgSpaceInvertY(
							sibr::Vector2i(floor(dmPos.x()), floor(dmPos.y())), _dm[im](floor(dmPos.x()),
								floor(dmPos.y())).x())
						).norm();

					if (dReproj / dCam < 0.005f && _linDiffs[im](floor(texPos.x()), floor(texPos.y())) != sibr::Vector3f(0.0f, 0.0f, 0.0f)) {

						double l = 1.0 / (dCam + 0.01f);
						w += l;
						val += l * _linDiffs[im](floor(texPos.x()), floor(texPos.y())).cast<double>();
					}
				}
			}
			if (w > 0)
				val /= w;
			colors[v_id] = val.cast<float>();
		}

		mesh->colors(colors);

		std::vector<sibr::Vector4f> lights;

		int num_l = 0;
		float w_sum = 0;
		for (int v = 0; v < mesh->vertices().size(); v++) {
			if (colors[v].norm() > 10.0) {
				bool tooClose = false;
				for (const auto& l : lights) {
					if ((mesh->vertices()[v] - l.xyz()).norm() < 0.03 * _scale) {
						tooClose = true;
						break;
					}

				}
				if (!tooClose) {
					std::cout << v << " -> ";
					std::cout << mesh->vertices()[v] << '\n';
					num_l++;
					lights.push_back(sibr::Vector4f(
						mesh->vertices()[v].x(),
						mesh->vertices()[v].y(),
						mesh->vertices()[v].z(),
						colors[v].norm())
					);
					w_sum += colors[v].norm();
				}
			}
		}

		if (w_sum > 0) {
			for (auto& l : lights) {
				l.w() /= w_sum;
			}
		}

		return lights;
	}

	void sibr::indoorRelighterIBRScene::exportMesh2Mitsuba()
	{
		if (sibr::fileExists(_args.dataset_path.get() + "/coloredMesh/" + _lighting.name + "/mesh.ply") &&
			sibr::fileExists(_args.dataset_path.get() + "/coloredMesh/" + _lighting.name + "/scene.xml") &&
			sibr::fileExists(_args.dataset_path.get() + "/coloredMesh/" + _lighting.name + "/cameras.lookat") &&
			sibr::fileExists(_args.dataset_path.get() + "/coloredMesh/" + _lighting.name + "/imageSizes.txt")

			) {
			return;
		}

		sibr::makeDirectory(_args.dataset_path.get() + "/coloredMesh");
		sibr::makeDirectory(_args.dataset_path.get() + "/coloredMesh/" + _lighting.name);
		//First we need to compute colors based on albedo.
		//We load the cameras with the right resolution
		std::vector<sibr::InputCamera::Ptr> camTexture;

		for (const auto& cam : cameras()->inputCameras()) {
			camTexture.push_back(std::make_shared<sibr::InputCamera>(cam->resizedW(_args.texture_width)));
		}

		if (!sibr::fileExists(_args.dataset_path.get() + "/coloredMesh/" + _lighting.name + "/cameras.lookat") ||
			!sibr::fileExists(_args.dataset_path.get() + "/coloredMesh/" + _lighting.name + "/imageSizes.txt")) {
			InputCamera::saveAsLookat(camTexture, _args.dataset_path.get() + "/coloredMesh/" + _lighting.name + "/cameras.lookat");
			InputCamera::saveImageSizes(camTexture, _args.dataset_path.get() + "/coloredMesh/" + _lighting.name + "/imageSizes.txt");
		}

		if (!(sibr::fileExists(_args.dataset_path.get() + "/coloredMesh/" + _lighting.name + "/mesh.ply") &&
			sibr::fileExists(_args.dataset_path.get() + "/coloredMesh/" + _lighting.name + "/scene.xml"))) {
			colorizeMesh(proxies()->proxyPtr(), _linImgsAlbedo, camTexture, 0.995);


			proxies()->proxy().save(_args.dataset_path.get() + "/coloredMesh/" + _lighting.name + "/mesh.ply", true);

			std::ofstream outScene;
			outScene.open(_args.dataset_path.get() + "/coloredMesh/" + _lighting.name + "/scene.xml");
			outScene <<
				"<?xml version=\"1.0\" encoding=\"utf-8\" ?>\n\n" <<
				"<scene version=\"0.6.0\">\n" <<
				"\t<shape type=\"ply\">\n" <<
				"\t\t<boolean name=\"srgb\" value=\"false\" />\n" <<
				"\t\t<string name=\"filename\" value=\"mesh.ply\" />\n" <<
				"\t\t\t<bsdf type=\"twosided\">\n" <<
				"\t\t\t\t<bsdf type=\"diffuse\">\n" <<
				"\t\t\t\t\t<texture type=\"vertexcolors\" name=\"reflectance\" />\n" <<
				"\t\t\t\t</bsdf>\n" <<
				"\t\t\t</bsdf>\n" <<
				"\t</shape>\n" <<
				"\t<include filename=\"$lightsFile.xml\" />\n" <<
				"</scene>";
		}


	}

	void sibr::indoorRelighterIBRScene::computeNormals(Mesh& mesh)
	{
		mesh.generateSmoothNormals(8);

		PlaneEstimator PE(mesh.vertices());
		PE.computePlanes(20, _scale * 0.05, 10000);

		std::vector<sibr::Vector3f> normalsPlanar;
		for (int v_id = 0; v_id < mesh.normals().size(); v_id++) {

			sibr::Vector3f n = mesh.normals()[v_id];
			sibr::Vector3f v = mesh.vertices()[v_id];

			float bestVal = 0.0f;
			float bestDist = 0.1 * _scale;
			sibr::Vector3f bestN;
			for (const auto& p : PE._planes) {
				if (abs(n.dot(p.xyz())) > 0.98) {
					if (abs(v.dot(p.xyz()) - p.w()) < bestDist) {
						bestVal = abs(n.dot(p.xyz()));
						bestN = p.xyz();
					}
				}
			}
			if (bestVal > 0.98) {
				if (n.dot(bestN) > 0.0) {
					normalsPlanar.push_back(bestN);
				}
				else {
					normalsPlanar.push_back(-bestN);
				}
			}
			else {
				normalsPlanar.push_back(n);
			}
		}
		mesh.normals(normalsPlanar);

	}

	template<class T>
	void sibr::indoorRelighterIBRScene::encodeImLog(T& im)
	{
		im.toOpenCVnonConst() = _hdrF * im.toOpenCV();
		cv::divide(im.toOpenCV(), cv::Scalar(1.0, 1.0, 1.0) + im.toOpenCV(), im.toOpenCVnonConst());
		cv::pow(im.toOpenCV(), 0.4, im.toOpenCVnonConst());
	}
	template<class T>
	void sibr::indoorRelighterIBRScene::encodeImLog(T& im, float scale)
	{
		im.toOpenCVnonConst() = scale * im.toOpenCV();
		cv::divide(im.toOpenCV(), cv::Scalar(1.0, 1.0, 1.0) + im.toOpenCV(), im.toOpenCVnonConst());
		cv::pow(im.toOpenCV(), 0.4, im.toOpenCVnonConst());
	}

	template<class T>
	void sibr::indoorRelighterIBRScene::decodeImLog(T& im)
	{
		// x^(1/0.4)
		cv::pow(im.toOpenCV(), 1.0 / 0.4, im.toOpenCVnonConst());
		cv::divide(im.toOpenCV(), cv::Scalar(1.0, 1.0, 1.0) - im.toOpenCV(), im.toOpenCVnonConst());
		im.toOpenCVnonConst() = im.toOpenCV() / _hdrF;
	}


	void sibr::indoorRelighterIBRScene::saveMosaic(const sibr::ImageRGB& im, int& num, int globalNum, std::string path, std::string name, int sideIm) {
		if (im.w() < sideIm || im.h() < sideIm) {
			return;
		}
		else {
			int wC = ceil(im.w() / (float)sideIm);
			int wShift = floor((im.w() - sideIm) / ((float)wC - 1.0));

			int hC = ceil(im.h() / (float)sideIm);
			int hShift = floor((im.h() - sideIm) / ((float)hC - 1.0));

			for (int bW = 0; bW < wC; bW++) {
				for (int bH = 0; bH < hC; bH++) {

					cv::Mat subIMcv = im.toOpenCV()(cv::Rect(bW * wShift, bH * hShift, sideIm, sideIm));
					sibr::ImageRGB subIm;
					subIm.fromOpenCV(subIMcv);

					std::ostringstream out;
					out << std::internal << std::setfill('0') << std::setw(8) << (globalNum + num);
					subIm.save(path + "/" + out.str() + "_" + name, false);
					num++;

				}
			}

			sibr::ImageRGB contract = im.resized(im.w() / 2, im.h() / 2);
			saveMosaic(contract, num, globalNum, path, name, sideIm);

		}
	}

	template <class T>
	void sibr::indoorRelighterIBRScene::numMosaic(const T& im, int& num, int sideImW, int sideImH) {
		if (sideImH < 0)
			sideImH = sideImW;
		if (im.w() < sideImW || im.h() < sideImH) {
			return;
		}
		else {
			if (std::max(im.w(), im.h()) < 2048) {

				int wC = ceil(im.w() / (float)sideImW);
				int wShift = 0;
				if (wC > 1.0f)
					wShift = floor((im.w() - sideImW) / ((float)wC - 1.0));

				int hC = ceil(im.h() / (float)sideImH);
				int hShift = 0;
				if (hC > 1.0f)
					hShift = floor((im.h() - sideImH) / ((float)hC - 1.0));

				for (int bW = 0; bW < wC; bW++) {
					for (int bH = 0; bH < hC; bH++) {

						num++;

					}
				}
			}
			//T contract = im.resized(im.w() / 2, im.h() / 2);
			//numMosaic(contract, num, sideImW, sideImH);
		}
	}

	template <class T>
	void sibr::indoorRelighterIBRScene::fillMosaic(const T& im, int& num, std::vector<T>& mosaics, int pos, int sideImW, int sideImH) {
		if (sideImH < 0)
			sideImH = sideImW;
		int wM = 1;
		if (im.w() < sideImW || im.h() < sideImH) {
			return;
		}
		else {
			if (std::max(im.w(), im.h()) <= 2048) {
				int wC = ceil(im.w() / (float)sideImW);
				int wShift = 0;
				if (wC > 1.0f)
					wShift = floor((im.w() - sideImW) / ((float)wC - 1.0));

				int hC = ceil(im.h() / (float)sideImH);
				int hShift = 0;
				if (hC > 1.0f)
					hShift = floor((im.h() - sideImH) / ((float)hC - 1.0));

				for (int bW = 0; bW < wC; bW++) {
					for (int bH = 0; bH < hC; bH++) {

						cv::Mat subIMcv = im.toOpenCV()(cv::Rect(bW * wShift, bH * hShift, sideImW, sideImH));
						subIMcv.copyTo(mosaics[num].toOpenCVnonConst()(cv::Rect(sideImW * (pos % wM), sideImH * (int)floor(pos / wM), sideImW, sideImH)));
						num++;

					}
				}
			}

			//T contract = im.resized(im.w() / 2, im.h() / 2);
			//fillMosaic(contract, num, mosaics, pos, sideImW, sideImH);

		}
	}

	lightingSpec sibr::indoorRelighterIBRScene::loadLighting(std::string pathLights, std::string filename)
	{

		std::cout << filename << std::endl;
		if (filename.find("Env") != std::string::npos) {
			return lightingSpec{ filename.substr(0, filename.find(".")),"default" };
		}
		else {
			sibr::XMLTree doc(pathLights + "/" + filename);

			sibr::Vector3f center;
			rapidxml::xml_node<>* root_node = doc.first_node("scene");
			rapidxml::xml_node<>* shape_node = root_node->first_node("shape");
			if (shape_node) {
				rapidxml::xml_node<>* nodeCenter = shape_node->first_node("point");
				if (nodeCenter) {
					center.x() = std::stof(nodeCenter->first_attribute("x")->value());
					center.y() = std::stof(nodeCenter->first_attribute("y")->value());
					center.z() = std::stof(nodeCenter->first_attribute("z")->value());

					rapidxml::xml_node<>* nodeRadius = shape_node->first_node("float");
					float radius = 1.00f * std::stof(nodeRadius->first_attribute("value")->value());

					return lightingSpec{ filename.substr(0, filename.find(".")), "sphere", center, radius };
				}
			}
			return lightingSpec{ filename.substr(0, filename.find(".")),"default" };

		}

	}

	void sibr::indoorRelighterIBRScene::chooseNormalSample(
		const std::vector<sibr::InputCamera::Ptr>& cams,
		const sibr::Vector3f& point,
		int& bestIm,
		sibr::Vector2i& pos,
		const std::vector<int>& camsOrder
	)
	{
		for (const auto& im : camsOrder) {
			sibr::Vector3f proj = cams[im]->project(point);

			if (

				abs(proj.x()) < 0.99 &&
				abs(proj.y()) < 0.99

				) {

				proj.y() = -proj.y();
				sibr::Vector2f pos2dImg = (0.5f * (proj.xy() + sibr::Vector2f(1, 1))).cwiseProduct(sibr::Vector2f(cams[im]->w(), cams[im]->h()));
				sibr::Vector2i secondPosImInt(floor(pos2dImg.x()), floor(pos2dImg.y()));

				sibr::Vector2i secondPosTexInt(floor(pos2dImg.x() * _dmTex[im]->w() / cams[im]->w()), floor(pos2dImg.y() * _dmTex[im]->h() / cams[im]->h()));


				float d = _dmTex[im](secondPosTexInt).x();
				sibr::Vector2f pos2dGL(2.0f * ((secondPosTexInt.cast<float>() + sibr::Vector2f(0.5, 0.5)).cwiseQuotient(sibr::Vector2f(_dmTex[im]->w(), _dmTex[im]->h()))) - sibr::Vector2f(1, 1));  //to [-1,1]
				pos2dGL.y() = -pos2dGL.y();
				sibr::Vector3f hitPos = cams[im]->unproject(sibr::Vector3f(pos2dGL.x(), pos2dGL.y(), d));

				if ((hitPos - point).norm() < 0.05 * _scale) {

					bestIm = im;
					pos = secondPosImInt;
					break;
				}
			}


		}

	}



	void sibr::indoorRelighterIBRScene::chooseSample(
		const std::vector<sibr::InputCamera::Ptr>& cams,
		const sibr::Vector3f& point,
		const std::vector<sibr::Vector3f>& nfPrecom,
		const sibr::Vector3f& rayDir,
		int& bestIm,
		sibr::Vector2i& pos,
		std::vector<bool>& visibility)
	{
		float bestW = -1.0f;
		bool useVisibility = false;
		if (visibility.size() == cams.size())
			useVisibility = true;

		for (int im = 0; im < cams.size(); im++) {
			if (useVisibility && !visibility[im]) {
				continue;
			}
			else if (useVisibility && visibility[im]) {
				sibr::Vector3f proj = cams[im]->project(point);
				proj.y() = -proj.y();
				sibr::Vector2f pos2dImg = (0.5f * (proj.xy() + sibr::Vector2f(1, 1))).cwiseProduct(sibr::Vector2f(cams[im]->w(), cams[im]->h()));
				sibr::Vector2i secondPosImInt(floor(pos2dImg.x()), floor(pos2dImg.y()));

				float w = rayDir.dot((point - cams[im]->position()).normalized());
				if (w > bestW) {
					bestIm = im;
					bestW = w;
					pos = secondPosImInt;
					if (w > 0.9)
						break;
				};
			}
			else {
				sibr::Vector3f proj = cams[im]->project(point);
				if (

					abs(proj.x()) < 0.99 &&
					abs(proj.y()) < 0.99

					) {
					proj.y() = -proj.y();
					sibr::Vector2f pos2dImg = (0.5f * (proj.xy() + sibr::Vector2f(1, 1))).cwiseProduct(sibr::Vector2f(cams[im]->w(), cams[im]->h()));
					sibr::Vector2i secondPosImInt(floor(pos2dImg.x()), floor(pos2dImg.y()));
					float& z = _dm[im](secondPosImInt).x(); // back to NDC
					float zLin = nfPrecom[im].x() / (nfPrecom[im].y() - z * nfPrecom[im].z());
					float projzLin = nfPrecom[im].x() / (nfPrecom[im].y() - proj.z() * nfPrecom[im].z());
					if (abs(zLin - projzLin) / abs(zLin) < 0.01) {

						float w = rayDir.dot((point - cams[im]->position()).normalized());
						if (w > bestW) {
							bestIm = im;
							bestW = w;
							pos = secondPosImInt;
						}
					}
				}
			}
		}

	}

	void sibr::indoorRelighterIBRScene::vibilityTest(
		const std::vector<sibr::InputCamera::Ptr>& cams,
		const std::vector<sibr::Vector3f>& points,
		const std::vector<sibr::Vector3f>& nfPrecom,
		std::vector<bool>& vibility)
	{
		for (int im = 0; im < cams.size(); im++) {

			bool vis = true;

			for (const auto& point : points) {
				sibr::Vector3f proj = cams[im]->project(point);
				if (

					abs(proj.x()) < 0.99 &&
					abs(proj.y()) < 0.99

					) {
					proj.y() = -proj.y();
					sibr::Vector2f pos2dImg = (0.5f * (proj.xy() + sibr::Vector2f(1, 1))).cwiseProduct(sibr::Vector2f(cams[im]->w(), cams[im]->h()));
					sibr::Vector2i secondPosImInt(floor(pos2dImg.x()), floor(pos2dImg.y()));
					float& z = _dm[im](secondPosImInt).x(); // back to NDC
					float zLin = nfPrecom[im].x() / (nfPrecom[im].y() - z * nfPrecom[im].z());
					float projzLin = nfPrecom[im].x() / (nfPrecom[im].y() - proj.z() * nfPrecom[im].z());
					if (!(abs(zLin - projzLin) / abs(zLin) < 0.01)) {
						vis = false;
						break;
					}
				}
				else {
					vis = false;
					break;
				}
			}


			vibility[im] = vis;
		}


	}

	sibr::Vector3f indoorRelighterIBRScene::tempToRGB(float kelvin)
	{
		float temp = kelvin / 100;
		float r, g, b;
		if (temp <= 66) {
			r = 1.0f;
			g = temp;
			g = 0.39008157876 * log(g) - 0.63184144378;
			if (temp <= 19) {
				b = 0;
			}
			else {
				b = (temp - 10.0f);
				b = 0.54320678911 * log(b) - 1.19625408914;
			}
		}
		else {
			r = temp - 60;
			r = 1.29293618606 * pow(r, -0.1332047592);

			g = temp - 60;
			g = 1.1298908609 * pow(g, -0.0755148492);

			b = 1;
		}

		r = std::max(0.0f, std::min(1.0f, r));
		g = std::max(0.0f, std::min(1.0f, g));
		b = std::max(0.0f, std::min(1.0f, b));

		return sibr::Vector3f(r, g, b);
	}



}