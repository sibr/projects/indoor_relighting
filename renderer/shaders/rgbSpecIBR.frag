#version 420

#define NUM_CAMS (12)
#define K (8)
#define scale (1.0)

uniform vec3 camPos;
uniform vec3 camDir;

out vec4 out_rgb_spec[8];
in vec2 vertex_coord;
uniform float exposure;

// 2D proxy texture.
layout(binding=0) uniform sampler2D proxy;
// normal images tex
layout(binding=1) uniform sampler2D normals;

// pos images for depth test
layout(binding=2) uniform sampler2DArray input_depths;

// normal images for normal test
layout(binding=3) uniform sampler2DArray input_normals;

// images
layout(binding=4) uniform sampler2DArray input_rgbs;

// spec
layout(binding=5) uniform sampler2DArray input_specs;

// occlusion Boudaries
layout(binding=6) uniform sampler2DArray input_occs;


// Input cameras.
struct CameraInfos
{
  mat4 vp;
  mat4 inv_vp;
  vec3 pos;
  int selected;
  vec3 dir;
};
// They are stored in a contiguous buffer (UBO), lifting most limitations on the number of uniforms.
layout(std140, binding=7) uniform InputCameras
{
  CameraInfos cameras[NUM_CAMS];
};

vec3 project(vec3 point, mat4 proj) {
  vec4 p1 = proj * vec4(point, 1.0);
  vec3 p2 = (p1.xyz/p1.w);
  return (p2.xyz*0.5 + 0.5);
}

bool frustumTest(vec3 p, vec2 ndc, int i) {
  vec3 d1 = cameras[i].dir;
  vec3 d2 = p - cameras[i].pos;
  return !any(greaterThan(ndc, vec2(1.0))) && dot(d1,d2)>0.0;
}

vec3 unproject(vec2 ndc, float d, mat4 inv_proj) {
    vec4 pxl = vec4(ndc,d,1.0); //[-1,1]
    vec4 obj = inv_proj * pxl;                   
    return (obj.xyz/obj.w);
}



void main(void) {

	vec4 point = texture(proxy, vertex_coord);
	vec3 normal = texture(normals, vertex_coord).xyz;

	vec4 sumWs = vec4(0,0,0,0);

	for(int i = 0; i < NUM_CAMS; i++){

		if(cameras[i].selected == 0){
			continue;
		}

		vec3 uvd = project(point.xyz, cameras[i].vp);
		uvd.y= 1.0 - uvd.y;
		vec2 ndc = abs(2.0*uvd.xy-1.0);
		
		if (frustumTest(point.xyz, ndc, i)){
		
			float d = texture(input_depths, vec3(uvd.xy,i)).x;
			vec2 reproc =2.0*uvd.xy-1.0;
			reproc.y= - reproc.y;
			vec3 proj_point = unproject(reproc,d,cameras[i].inv_vp);
			
			vec3 proj_normal = normalize(texture(input_normals, vec3(uvd.xy,i)).xyz);
		
			float cosTheta = dot(normalize(cameras[i].pos-point.xyz),normal);
			float dD=0.005+0.015*sin(acos(cosTheta));
	
			float depthThrld = length(cameras[i].pos - point.xyz) * dD;
			if(length(proj_point-point.xyz)<depthThrld && dot(normal,proj_normal)>0){

				float cosThetaCam = max(0.001f,dot(normalize(cameras[i].pos-point.xyz),normal));
				vec3 proj_rgb = max(texture(input_rgbs, vec3(uvd.xy,i)).xyz,0);
				vec3 proj_spec = max(texture(input_specs, vec3(uvd.xy,i)).xyz,0);
				vec4 weights = vec4(0,0,0,0);

				
				float proj_occ = max(texture(input_occs, vec3(uvd.xy,i)).x,0);
				float occWeight = (0.1*scale)/max(0.1*scale,proj_occ);
				//Cam Dist
				weights[0] = 1.0f/(max(length(cameras[i].pos-camPos),0.2f*scale));
				//Angle Dist
				weights[1] = pow(max(0.001f,dot(normalize(cameras[i].pos-point.xyz),normalize(camPos-point.xyz))),4);
				
				//These are viewpoint independent
				//Closest view
				weights[2] = pow(1.0f/(length(cameras[i].pos - point.xyz)+0.01f*scale),4);
				//Othogonality
				weights[3] = exp(10.0*pow(cosThetaCam,2))-1.0;
			
				for(int k=0;k<4;k++){
					out_rgb_spec[k] += occWeight*weights[k]*vec4(proj_rgb,1);
					out_rgb_spec[k+4] +=occWeight*weights[k]*vec4(proj_spec,1);
					sumWs[k]+= occWeight*weights[k];
				}
				
			}
		}


	}

	for(int k=0;k<4;k++){
		if(sumWs[k]>0){
			out_rgb_spec[k] *= exposure/sumWs[k];
			out_rgb_spec[k+4] *= exposure/sumWs[k];
		}
		else{
			out_rgb_spec[k] = vec4(0,0,0,1);
			out_rgb_spec[k+4] = vec4(0,0,0,1);
		}
	}

	vec3 uvd = project(point.xyz, cameras[0].vp);
	uvd.y= 1.0 - uvd.y;
	float d = texture(input_depths, vec3(uvd.xy,0)).x;
	
	vec2 ndc = 2.0*uvd.xy-1.0;
	ndc.y= - ndc.y;
	vec3 proj_point = unproject(ndc,d,cameras[0].vp);

	
}


