#version 420

#define NUM_CAMS (12)
#define K (8)

uniform vec3 camPos;
out vec4 out_tarData[7];

in vec2 vertex_coord;

uniform bool turnOff;
uniform float exposure;


// 2D target proxy texture.
layout(binding=0) uniform sampler2D proxyTar;
// 2D target normal texture.
layout(binding=1) uniform sampler2D normalTar;
// 2D target mirror position texture.
layout(binding=2) uniform sampler2D mirrorTar;

// pos images for depth test for target
layout(binding=3) uniform sampler2DArray input_tarDepths;
// radiance images
layout(binding=4) uniform sampler2DArray input_rads;
// radiance images
layout(binding=5) uniform sampler2DArray input_tarRadAdds;
// radiance image computed before
layout(binding=6) uniform sampler2D input_rad;
//Rgb images
layout(binding=7) uniform sampler2DArray input_rgbs;
// occlusion indicator 
layout(binding=8) uniform sampler2DArray input_occs;


// Input cameras.
struct CameraInfos
{
  mat4 vp;
  mat4 inv_vp;
  vec3 pos;
  int selected;
  vec3 dir;
};
// They are stored in a contiguous buffer (UBO), lifting most limitations on the number of uniforms.
layout(std140, binding=9) uniform InputCameras
{
  CameraInfos cameras[NUM_CAMS];
};

vec3 project(vec3 point, mat4 proj) {
  vec4 p1 = proj * vec4(point, 1.0);
  vec3 p2 = (p1.xyz/p1.w);
  return (p2.xyz*0.5 + 0.5);
}

bool frustumTest(vec3 p, vec2 ndc, int i) {
  vec3 d1 = cameras[i].dir;
  vec3 d2 = p - cameras[i].pos;
  return !any(greaterThan(ndc, vec2(1.0))) && dot(d1,d2)>0.0;
}

vec3 unproject(vec2 ndc, float d, mat4 inv_proj) {
    vec4 pxl = vec4(ndc,d,1.0); //[-1,1]
    vec4 obj = inv_proj * pxl;                   
    return (obj.xyz/obj.w);
}



float lum(vec3 rgb){
	return 0.2126*rgb.x + 0.7152*rgb.y + 0.0722*rgb.z;
}

void main(void) {

	vec3 normal = texture(normalTar, vertex_coord).xyz;
	vec4 pointTar = texture(proxyTar, vertex_coord);
	vec4 mirrorTar = texture(mirrorTar, vertex_coord);

	vec3 mirrorRGB = vec3(0.0,0.0,0.0);
	vec3 mirrorRad = vec3(0.0,0.0,0.0);
	vec3 mirrorTarRadAdd = vec3(0.0,0.0,0.0);

	vec3 tarRadAdd = vec3(0.0,0.0,0.0);
	vec3 tarRadRm = vec3(0.0,0.0,0.0);

	if(turnOff){
		tarRadRm = texture(input_rad, vertex_coord).xyz;
	};

	vec3 reflectDepth = vec3(length(mirrorTar.xyz-pointTar.xyz)/(length(pointTar.xyz-camPos)+0.00001f),0.0,0.0);
	if(isnan(reflectDepth.x) || isinf(reflectDepth.x))
		reflectDepth.x=0.0;

	float weightTar=0;

	float weightMirror=0;

	for(int i = 0; i < NUM_CAMS; i++){

		if(cameras[i].selected == 0){
			continue;
		}


			// Target radiance
		{	
			float cosTheta = dot(normalize(cameras[i].pos-pointTar.xyz),normal);
			float dD=0.005+0.015*sin(acos(cosTheta));

			vec3 uvdTar = project(pointTar.xyz,cameras[i].vp);
			uvdTar.y=1.0 - uvdTar.y;
			vec2 ndcTar = abs(2.0*uvdTar.xy-1.0);
			
			if (frustumTest(pointTar.xyz, ndcTar, i)){
				
				float dTar = texture(input_tarDepths, vec3(uvdTar.xy,i)).x;
				vec2 reproc =2.0*uvdTar.xy-1.0;
				reproc.y= - reproc.y;
				vec3 proj_point = unproject(reproc,dTar,cameras[i].inv_vp);

				float inv_w = length(cameras[i].pos - pointTar.xyz);
				float depthThrld = inv_w * dD;
				float w = 1.0f/pow(inv_w,2.0);
				
				if(length(proj_point-pointTar.xyz)<depthThrld){
	
					float occ = 0.001f + texture(input_occs, vec3(uvdTar.xy,i)).x;
					w*=1.0f/occ;

					float mipmapLevel = textureQueryLod(input_tarRadAdds, uvdTar.xy).x;
					vec3 lvl0 =textureLod(input_tarRadAdds, vec3(uvdTar.xy,i),0).rgb;
					vec3 lvl1 =textureLod(input_tarRadAdds, vec3(uvdTar.xy,i),1).rgb;
					w*=1/(0.001f+length(lvl0-lvl1));

					tarRadAdd += w*max(textureLod(input_tarRadAdds, vec3(uvdTar.xy,i),0).rgb,0.0);
					//tarRadRm += w*max(texture(input_tarRadRms, vec3(uvdTar.xy,i)).rgb,0.0);
					weightTar += w;
	
				}
			}
		}
		
		
			// Target mirror radiance and albedo
		{	
			float dD=0.01;
			
			vec3 uvdTar = project(mirrorTar.xyz,cameras[i].vp);
			uvdTar.y=1.0 - uvdTar.y;
			vec2 ndcTar = abs(2.0*uvdTar.xy-1.0);
			
			if (frustumTest(mirrorTar.xyz, ndcTar, i)){
				
				float dTar = texture(input_tarDepths, vec3(uvdTar.xy,i)).x;
				vec2 reproc =2.0*uvdTar.xy-1.0;
				reproc.y= - reproc.y;
				vec3 proj_point = unproject(reproc,dTar,cameras[i].inv_vp);
	
				float inv_w = length(cameras[i].pos - mirrorTar.xyz); 
				float depthThrld = inv_w * dD;
				float w = 1.0f/(pow(inv_w,2.0)+0.0001f);
				if(length(proj_point-mirrorTar.xyz)<depthThrld){
	
					mirrorRad += w*max(texture(input_rads, vec3(uvdTar.xy,i)).rgb,0.0);
					mirrorRGB += w*max(texture(input_rgbs, vec3(uvdTar.xy,i)).rgb,0.0);
					mirrorTarRadAdd += w*max(textureLod(input_tarRadAdds, vec3(uvdTar.xy,i),0).rgb,0.0);
					weightMirror += w;
	
				}
			}
		}



		// Mirror Radiance and albedo

	}

	if(weightTar>0){
		tarRadAdd /= weightTar;
	}
	if(weightMirror>0){
		mirrorRGB /= weightMirror;
		mirrorRad /= weightMirror;
		mirrorTarRadAdd /= weightMirror;
	}

	vec3 tarSpec = clamp(mirrorRGB/(0.001f+mirrorRad),0.0,1.0)*mirrorTarRadAdd;
	if(!turnOff){
		tarSpec+=exposure*mirrorRGB; 
	}

	//Target radiance to add	
	out_tarData[0] = vec4(tarRadAdd,1.0);
	//Target radiance to remove	
	out_tarData[1] = vec4(tarRadRm,1.0);
	//Target Spec
	out_tarData[2] = vec4(tarSpec,1.0);
	//This data is use for training data generation
	out_tarData[3] = vec4(exposure*mirrorRad,1.0);
	out_tarData[4] = vec4(exposure*mirrorRGB,1.0);
	out_tarData[5] = vec4(exposure*mirrorTarRadAdd,1.0);
	out_tarData[6] = vec4(reflectDepth,1.0);
}
