#version 420

layout(binding = 0) uniform sampler2D tex;
layout(location= 0) out vec4 out_color;

in vec2 tex_coord;

float lin2sRGBF(float inF){
        
        if(inF<0.0031308){
                return 12.92*inF;
        }
        else{
                return 1.055*pow(inF,1.0/2.4)-0.055;
        }
        
}

vec4 lin2sRGB(vec4 inVec){
        return vec4(lin2sRGBF(inVec.x),lin2sRGBF(inVec.y),lin2sRGBF(inVec.z),inVec.w);
}


void main(void) {
    vec2 texcoord = tex_coord ;
    out_color = lin2sRGB(texture(tex,texcoord));
}
