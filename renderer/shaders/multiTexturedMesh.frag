#version 420

layout (binding=0) uniform sampler2D tex[5];
uniform int numTex;
uniform float exposure;
uniform float gamma;

out vec4 out_color[5];

in vec2 vertUV;


float sRGB2LinF(float inF){
        if(inF<0.04045){
                return inF/12.92;
        }
        else{
                return pow((inF+0.055)/(1.055),2.4);
        }
}

float lin2sRGBF(float inF){
        
        if(inF<0.0031308){
                return 12.92*inF;
        }
        else{
                return 1.055*pow(inF,1.0/2.4)-0.055;
        }
        
}

vec4 sRGB2Lin(vec4 inVec){
        return vec4(sRGB2LinF(inVec.x),sRGB2LinF(inVec.y),sRGB2LinF(inVec.z),inVec.w);
}

vec4 lin2sRGB(vec4 inVec){
        return vec4(lin2sRGBF(inVec.x),lin2sRGBF(inVec.y),lin2sRGBF(inVec.z),inVec.w);
}

void main(void) {

	vec2 uv = vertUV;
	uv.y = 1.0 - uv.y; /// \todo TODO: Why Texture are flipped in y ?

	for(int t=0;t<numTex;t++){
		out_color[t] = texture(tex[t], uv);
		/*if(gamma != 1.0){
			out_color[t] = lin2sRGB(out_color[t]);
		}*/
		if(exposure != 0.0){
			out_color[t] = pow(2.0f,exposure)*out_color[t];
		}
	}
	
}
