#version 420

out vec4 out_color[2];

in vec3 vertPos;
in vec3 vertNormal;

void main(void) {

	out_color[0] = vec4(vertPos,1.0);
	out_color[1] = vec4(vertNormal,1.0);
	
}
