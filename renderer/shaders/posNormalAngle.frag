#version 420

uniform vec3 camPos;
uniform vec3 camDir;
uniform float scaleFocalHeight;
uniform mat4 M;
uniform mat4 V;
//uniform int compressNormals;
out vec4 out_color[6];

in vec3 vertPos;
in vec3 vertNormal;

void main(void) {

	out_color[0] = vec4(vertPos,1.0);
	out_color[1] = vec4(vertNormal,1.0);

	vec3 di=normalize(camPos-vertPos);
	vec3 dn=normalize(vertNormal);
	vec3 normalCam = normalize((inverse(transpose(V*M)) * vec4(vertNormal,0.0)).xyz);
	
	float disp = scaleFocalHeight / dot(vertPos - camPos,camDir);

	float cosAngle = dot(dn,di);
	vec3 reflDir = 2.0f*cosAngle*dn-di;
	
	out_color[2] = vec4(reflDir,1.0);
	out_color[3] = vec4(acos(cosAngle)/1.5708,0.0,0.0,1.0);
	if(isnan(out_color[3].x))
		out_color[3].x=0.0;

	out_color[4] = vec4(vec3(0.5)+0.5*normalCam,1.0);
	//out_color[4] = vec4(normalCam,1.0);

	out_color[5] = vec4(disp,0.0,0.0,1.0);
	
}
