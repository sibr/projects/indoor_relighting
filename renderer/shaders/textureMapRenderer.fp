#version 420

in vec4 projected;
layout(binding=0) uniform sampler2D im;
layout(binding=1) uniform isampler2D id;

layout(location = 0) out vec4 out_color;

void main(void) {
	vec2 uv=projected.xy/projected.w;
	uv.xy=0.5*uv.xy+0.5;

	//depth texture is y inverted
	if(
	texture(id,uv).r!=gl_PrimitiveID
	)
		discard;

	uv.y=1.0-uv.y;

    out_color = texture(im,uv);
}

