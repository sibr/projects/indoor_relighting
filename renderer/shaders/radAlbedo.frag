#version 420

#define NUM_CAMS (12)
#define K (8)

out vec4 out_radAlbedo;

in vec2 vertex_coord;

uniform float exposure;

// 2D proxy texture.
layout(binding=0) uniform sampler2D proxy;
// 2D normal texture.
layout(binding=1) uniform sampler2D normalTex;

// pos images for depth test
layout(binding=2) uniform sampler2DArray input_depths;

// normal images for normal test
layout(binding=3) uniform sampler2DArray input_normals;

// rgbs images
layout(binding=4) uniform sampler2DArray input_rgbs;

// radiance images
layout(binding=5) uniform sampler2DArray input_rads;

// occlusion Boudaries
layout(binding=6) uniform sampler2DArray input_occs;

// Input cameras.
struct CameraInfos
{
  mat4 vp;
  mat4 inv_vp;
  vec3 pos;
  int selected;
  vec3 dir;
};
// They are stored in a contiguous buffer (UBO), lifting most limitations on the number of uniforms.
layout(std140, binding=7) uniform InputCameras
{
  CameraInfos cameras[NUM_CAMS];
};

vec3 project(vec3 point, mat4 proj) {
  vec4 p1 = proj * vec4(point, 1.0);
  vec3 p2 = (p1.xyz/p1.w);
  return (p2.xyz*0.5 + 0.5);
}

bool frustumTest(vec3 p, vec2 ndc, int i) {
  vec3 d1 = cameras[i].dir;
  vec3 d2 = p - cameras[i].pos;
  return !any(greaterThan(ndc, vec2(1.0))) && dot(d1,d2)>0.0;
}

float lum(vec3 rgb){
	return 0.2126*rgb.x + 0.7152*rgb.y + 0.0722*rgb.z;
}

void insert(inout float[NUM_CAMS] vals,inout int[NUM_CAMS] idx, float x, int id){
	int j=0;
	while(j<NUM_CAMS && id >= 0 ){
		float oldVal = vals[j];
		int oldId = idx[j];
		if(x>oldVal){
			vals[j] = x;
			idx[j] = id;
			x = oldVal;
			id = oldId;
		}
		j++;
	}
}

vec3 unproject(vec2 ndc, float d, mat4 inv_proj) {
    vec4 pxl = vec4(ndc,d,1.0); //[-1,1]
    vec4 obj = inv_proj * pxl;                   
    return (obj.xyz/obj.w);
}


void main(void) {

	vec4 point = texture(proxy, vertex_coord);
	vec3 normal = texture(normalTex, vertex_coord).xyz;

	vec3 rad = vec3(0.0,0.0,0.0);

	int numVal=0;
	float weightRad=0;
	for(int i = 0; i < NUM_CAMS; i++){

		if(cameras[i].selected == 0){
			continue;
		}

		float cosTheta = dot(normalize(cameras[i].pos-point.xyz),normal);
		float dD=0.005+0.015*sin(acos(cosTheta));
	
		/////// Order and sce radiance
		vec3 uvd = project(point.xyz, cameras[i].vp);
		uvd.y= 1.0 - uvd.y;
		vec2 ndc = abs(2.0*uvd.xy-1.0);
	
		if (frustumTest(point.xyz, ndc, i)){
			float d = texture(input_depths, vec3(uvd.xy,i)).x;
			vec2 reproc =2.0*uvd.xy-1.0;
			reproc.y= - reproc.y;
			vec3 proj_point = unproject(reproc,d,cameras[i].inv_vp);

			vec3 proj_normal = texture(input_normals, vec3(uvd.xy,i)).xyz;
			
			float inv_w =  length(cameras[i].pos - point.xyz); 
			float depthThrld = inv_w * dD;
			float w =  1.0f/pow(inv_w,2); 
			if(length(proj_point-point.xyz)<depthThrld && dot(normal,proj_normal)>0){

					float occ = 0.001f + texture(input_occs, vec3(uvd.xy,i)).x;
					w*=1.0f/occ;
					
					rad += w*max(texture(input_rads, vec3(uvd.xy,i)).rgb,0.0);

					weightRad+= w;
					numVal++;

			}
		}

	}

	vec4 albedo=vec4(0.0,0.0,0.0,1.0);
	if(weightRad>0){
		rad /= weightRad;	
	}

	out_radAlbedo = vec4(exposure*rad,1.0);

}
