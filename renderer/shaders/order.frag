#version 420

#define NUM_CAMS (12)
#define K (8)

uniform vec3 camPos;
uniform vec3 camDir;

out vec4 out_order[8];
in vec2 vertex_coord;

uniform int[NUM_CAMS] orderCam;

// 2D proxy texture.
layout(binding=0) uniform sampler2D proxy;
// normal images tex
layout(binding=1) uniform sampler2D normals;

// pos images for depth test
layout(binding=2) uniform sampler2DArray input_pos;

// normal images for normal test
layout(binding=3) uniform sampler2DArray input_normals;

// images
layout(binding=4) uniform sampler2DArray input_rgbs;


// Input cameras.
struct CameraInfos
{
  mat4 vp;
  mat4 inv_vp;
  vec3 pos;
  int selected;
  vec3 dir;
};
// They are stored in a contiguous buffer (UBO), lifting most limitations on the number of uniforms.
layout(std140, binding=5) uniform InputCameras
{
  CameraInfos cameras[NUM_CAMS];
};

vec3 project(vec3 point, mat4 proj) {
  vec4 p1 = proj * vec4(point, 1.0);
  vec3 p2 = (p1.xyz/p1.w);
  return (p2.xyz*0.5 + 0.5);
}

bool frustumTest(vec3 p, vec2 ndc, int i) {
  vec3 d1 = cameras[i].dir;
  vec3 d2 = p - cameras[i].pos;
  return !any(greaterThan(ndc, vec2(1.0))) && dot(d1,d2)>0.0;
}

void main(void) {

	vec4 point = texture(proxy, vertex_coord);
	vec3 normal = texture(normals, vertex_coord).xyz;

	float vals[NUM_CAMS];
	int idx[NUM_CAMS];
	for(int i = 0; i < NUM_CAMS; i++){
		vals[i] = -1.0;
		idx[i] = -1;
	}

	bool validCam[NUM_CAMS];
	for(int i = 0; i < NUM_CAMS; i++){
		validCam[i] = false;
	}

	int numVal=0;

	ivec4 idTop4 = ivec4(-1,-1,-1,-1);
	vec4 scoreTop4 = vec4(100000.0f,100000.0f,100000.0f,100000.0f);
	float sumW = 0;
	out_order[0]=vec4(0,0,0,1);
	for(int i = 0; i < NUM_CAMS; i++){

		if(cameras[i].selected == 0){
			continue;
		}

		/////// Order
		vec3 uvd = project(point.xyz, cameras[i].vp);
		uvd.y= 1.0 - uvd.y;
		vec2 ndc = abs(2.0*uvd.xy-1.0);
		
		if (frustumTest(point.xyz, ndc, i)){
			vec3 proj_point = texture(input_pos, vec3(uvd.xy,i)).xyz;
			vec3 proj_normal = texture(input_normals, vec3(uvd.xy,i)).xyz;
		
			float cosTheta = dot(normalize(cameras[i].pos-point.xyz),normal);
			float dD=0.005+0.015*sin(acos(cosTheta));
	
			float depthThrld = length(cameras[i].pos - point.xyz) * dD;
			if(length(proj_point-point.xyz)<depthThrld && dot(normal,proj_normal)>0 && cosTheta>0.1f){

				validCam[i] = true;
				numVal++;

				float h = dot(cameras[i].pos-point.xyz,normal);
				float cosThetaCam = max(0.001f,dot(normalize(cameras[i].pos-point.xyz),normal));
				
				float dirFact = pow(dot(normalize(point.xyz-cameras[i].pos),cameras[i].dir),4) ;
				float score = h/(cosThetaCam);

				vec3 proj_rgb = texture(input_rgbs, vec3(uvd.xy,i)).xyz;


				float wCamDist = 1.0f/(length(cameras[i].pos-camPos)+0.0001f);
				float wCamAngle = pow(dot(normalize(cameras[i].pos-point.xyz),normalize(camPos-point.xyz)),2);
				float wTexture = pow(pow(cosThetaCam,2)/length(cameras[i].pos - point.xyz),2);
				float wDist = pow(cosThetaCam,2);
				
				float w = wDist;
				
				out_order[0] += w*vec4(proj_rgb,1);
				sumW+=w;


				if(score < scoreTop4[3]){
					scoreTop4[3] = score;
					idTop4[3] = i;
				}
				
				for(int k = 2;k>=0;k--){

					if(score < scoreTop4[k]){
						int prevId = idTop4[k];
						float prevScore = scoreTop4[k];
						scoreTop4[k]=score;
						idTop4[k]=i;
						scoreTop4[k+1]=prevScore;
						idTop4[k+1]=prevId;
						
					}
					
				}

			}
		}


	}

	out_order[0] /= sumW;
	
/*
out_order[0] = vec4(
		idTop4[0],
		idTop4[1],
		idTop4[2],
		idTop4[3]
	);
	
	int idFinal[K];
	for(int i = 0; i < K; i++){

		int j=i;
		while(j<NUM_CAMS && !validCam[orderCam[j]]){
			j += K;
		}
		if(j >= NUM_CAMS){
			idFinal[i]=-1;
		}
		else{
			idFinal[i] = orderCam[j];
		}
	}
	//To avoid having empty zones
	for(int i = 0; i < K; i++){
		int j=0;
		while(idFinal[i] == -1 && j<K)	{
			if(idFinal[j]>=0){
				idFinal[i]=idFinal[j];
			}
			j++;
		}
	}

	for(int k=0;k<K;k+=4){	
		out_order[k/4] = vec4(
			idFinal[k+0],
			idFinal[k+1],
			idFinal[k+2],
			idFinal[k+3]
		);
	}

*/

}





///////// K means
int[K] firstIdMod(	inout int[NUM_CAMS] idx, int numVal){
	
	int ids[K];
	bool found[K];
	for(int i=0; i<K ;i++){
		ids[i] = -1;
		found[i]=false;
	}

	for(int it=0; it<numVal ;it++){
		for(int i=0; i<K ;i++){
			if(!found[i]){
				if(idx[it]%K == i){
					ids[i] = idx[it];
					found[i] = true;
					break;
				}
			}
		}	
	}

	return ids;

}

vec3[K] kbest(
	inout vec3[NUM_CAMS] rgbs,
	inout int[NUM_CAMS] idx,
	int numVal
	){
	vec3 m[K];
	int initIds[K] = firstIdMod(idx,numVal);
	for(int i=0; i<K ;i++){
		m[i] = rgbs[initIds[i]];
	}
	return m;
}

int closestCluster(vec3 m[K], vec3 x){
	
	float minDist = length(m[0]-x);
	int minId = 0;

	for(int i=1; i<K ;i++){
		if(length(m[i]-x) < minDist){
			minDist=length(m[i]-x);
			minId=i;
		}
	}
	return minId;
};

vec3[K] kmeans(
	inout vec3[NUM_CAMS] rgbs,
	inout int[NUM_CAMS] idx,
	int numVal){
//Init the means
	vec3 m[K];
	vec3 m_update[K];
	int m_count[K];
	int initIds[K] = firstIdMod(idx,numVal);
	for(int i=0; i<K ;i++){
		m[i] = rgbs[initIds[i]];
	}
//Iterate
	for(int it=0; it<10 ;it++){

		for(int i=0; i<K ;i++){
			m_update[i] = vec3(0.0,0.0,0.0);
			m_count[i] = 0;
		}

		for(int id=0; id<numVal ;id++){
			int cc = closestCluster(m,rgbs[idx[id]]);
			m_update[cc] += rgbs[idx[id]];
			//m_count[cc] ++;
		}

		for(int i=0; i<K ;i++){
			if(m_count[i] > 0){
				m_update[i] /= m_count[i];
			}
			else{
				m_update[i] =  m[i];
			}
		}
		m=m_update;
	}

	return m;
	
};


