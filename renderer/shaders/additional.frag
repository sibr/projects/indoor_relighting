#version 420

#define NUM_CAMS (12)

out float out_color[8];

in vec2 vertex_coord;

uniform float exp;

// 2D proxy texture.
layout(binding=0) uniform sampler2D proxy;
// normal images tex
layout(binding=1) uniform sampler2D normals;
// indices to sample
layout(binding=2) uniform sampler2D ids0;
// indices to sample
layout(binding=3) uniform sampler2D ids1;

struct CameraInfos
{
  mat4 vp;
  mat4 inv_vp;
  vec3 pos;
  int selected;
  vec3 dir;
};
// They are stored in a contiguous buffer (UBO), lifting most limitations on the number of uniforms.
layout(std140, binding=4) uniform InputCameras
{
  CameraInfos cameras[NUM_CAMS];
};

vec3 project(vec3 point, mat4 proj) {
  vec4 p1 = proj * vec4(point, 1.0);
  vec3 p2 = (p1.xyz/p1.w);
  return (p2.xyz*0.5 + 0.5);
}

void main(void) {

	vec4 point = texture(proxy, vertex_coord);
	vec3 normal = texture(normals, vertex_coord).xyz;
	ivec4 id0 = ivec4(texture(ids0,vertex_coord));
	ivec4 id1 = ivec4(texture(ids1,vertex_coord));
	for(int i=0; i<8 ;i++){
		int id=-1;
		if(i<4){
			id=id0[i];
		}
		else{
			id=id1[i%4];
		}
		if(id>=0){	
			out_color[i] = acos(dot(normalize(cameras[id].pos-point.xyz),normalize(normal)))/1.5708;
		}
	}

}
