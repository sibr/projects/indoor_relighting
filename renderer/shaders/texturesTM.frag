#version 420

layout(binding = 0) uniform sampler2D tex;
layout(location= 0) out vec4 out_color;

uniform float expo;
uniform float gamma;
uniform float sat;
uniform float tempK;

in vec2 tex_coord;

// All components are in the range [0�1], including hue.
vec3 rgb2hsv(vec3 c)
{
    vec4 K = vec4(0.0, -1.0 / 3.0, 2.0 / 3.0, -1.0);
    vec4 p = mix(vec4(c.bg, K.wz), vec4(c.gb, K.xy), step(c.b, c.g));
    vec4 q = mix(vec4(p.xyw, c.r), vec4(c.r, p.yzx), step(p.x, c.r));

    float d = q.x - min(q.w, q.y);
    float e = 1.0e-10;
    return vec3(abs(q.z + (q.w - q.y) / (6.0 * d + e)), d / (q.x + e), q.x);
}

// All components are in the range [0�1], including hue.
vec3 hsv2rgb(vec3 c)
{
    vec4 K = vec4(1.0, 2.0 / 3.0, 1.0 / 3.0, 3.0);
    vec3 p = abs(fract(c.xxx + K.xyz) * 6.0 - K.www);
    return c.z * mix(K.xxx, clamp(p - K.xxx, 0.0, 1.0), c.y);
}


float gam(float inF,float g){
     return pow(inF,1.0/g);
}

// Color temperature 
// Copyright (C) 2014 by Benjamin 'BeRo' Rosseaux
// Because the german law knows no public domain in the usual sense,
// this code is licensed under the CC0 license 
// http://creativecommons.org/publicdomain/zero/1.0/
// Valid from 1000 to 40000 K (and additionally 0 for pure full white)
vec3 colorTemperatureToRGB(float temperature){
  // Values from: http://blenderartists.org/forum/showthread.php?270332-OSL-Goodness&p=2268693&viewfull=1#post2268693
  mat3 m = (temperature <= 6500.0) ? mat3(vec3(0.0, -2902.1955373783176, -8257.7997278925690),
	                                      vec3(0.0, 1669.5803561666639, 2575.2827530017594),
	                                      vec3(1.0, 1.3302673723350029, 1.8993753891711275)) :
	 								 mat3(vec3(1745.0425298314172, 1216.6168361476490, -8257.7997278925690),
   	                                      vec3(-2666.3474220535695, -2173.1012343082230, 2575.2827530017594),
	                                      vec3(0.55995389139931482, 0.70381203140554553, 1.8993753891711275));
  return mix(clamp(vec3(m[0] / (vec3(clamp(temperature, 1000.0, 40000.0)) + m[1]) + m[2]), vec3(0.0), vec3(1.0)), vec3(1.0), smoothstep(1000.0, 0.0, temperature));
}


vec4 lin2tm(vec4 inVec){

	vec4 tm = pow(2.0,expo)*inVec;
	//Temp balance
	vec3 badLum =tm.rgb*colorTemperatureToRGB(tempK);
	//luminance conservation
	// vec3(0.2126, 0.7152, 0.0722) is the weight vector to convert from rgb to luminance
	tm.rgb = badLum*dot(tm.rgb, vec3(0.2126, 0.7152, 0.0722)) / max(dot(badLum, vec3(0.2126, 0.7152, 0.0722)), 1e-5);

	tm = vec4(gam(tm.x,gamma),gam(tm.y,gamma),gam(tm.z,gamma),inVec.w);

	if(sat != 1.0){
		vec3 hsv = rgb2hsv(tm.rgb);
		hsv[1]=min(1.0,hsv[1]*sat);
		tm.rgb = hsv2rgb(hsv);
	}
	return tm;
}


void main(void) {
    vec2 texcoord = tex_coord ;
    out_color = lin2tm(texture(tex,texcoord));
}
