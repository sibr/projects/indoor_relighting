# include "IDRenderer.hpp"
# include "core/graphics/RenderUtility.hpp"


namespace sibr
{

	IDRenderer::~IDRenderer() {};

	IDRenderer::IDRenderer(int w,int h) 
	{
		_idShader.init("IDShader", 
			sibr::loadFile(sibr::Resources::Instance()->getResourceFilePathName("idRenderer.vp")), 
			sibr::loadFile(sibr::Resources::Instance()->getResourceFilePathName("idRenderer.fp")));

		_idShader_MVP.init(_idShader,"MVP");
		_id_RT.reset(new sibr::RenderTargetInt1(w,h));

	}

	void IDRenderer::render( const sibr::InputCamera& cam, const Mesh& mesh, bool backFaceCulling, bool frontFaceCulling)
	{

		//sibr::Vector1f cc(1.0);
		//_id_RT->clear(cc);
		
		glViewport(0, 0, _id_RT->w(), _id_RT->h());
		_id_RT->bind();
		glClearColor(-1,-1,-1, 1.0);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		_idShader.begin();
		_idShader_MVP.set(cam.viewproj());

		mesh.render(true, backFaceCulling, sibr::Mesh::FillRenderMode, frontFaceCulling);

		_idShader.end();

	}

} // namespace