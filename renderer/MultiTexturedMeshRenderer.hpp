#ifndef MULTITEXTUREDMESHRENDERER_HPP
#define MULTITEXTUREDMESHRENDERER_HPP

# include <core/graphics/Shader.hpp>
# include <core/graphics/Mesh.hpp>
# include <core/graphics/Texture.hpp>
# include <core/graphics/Camera.hpp>

# include "Config.hpp"

namespace sibr { 
	class SIBR_INDOORRELIGHTING_EXPORT MultiTexturedMeshRenderer
	{
	public:
		typedef std::shared_ptr<MultiTexturedMeshRenderer>	Ptr;

	public:
		MultiTexturedMeshRenderer();
		~MultiTexturedMeshRenderer();

		void	process(
			/*input*/	const Mesh& mesh,
			/*input*/	const Camera& eye,
			/*input*/	int numTex,
			/*output*/	IRenderTarget& dst,
			/*opt input*/ float exposure=0.0f,
			/*opt input*/ float gamma=1.0f);

	private:
		GLShader			_shader;
		GLParameter			_paramMVP;
		GLParameter			_paramExp;
		GLParameter			_paramGam;
		GLParameter			_paramNumTex;
	};

} /*namespace sibr*/ 

#endif // __SIBR_EXP_RENDERER_TEXTUREDMESHRENDERER_HPP__
