#include "OrderRendering.hpp"
#include <core/renderer/DepthRenderer.hpp>

extern "C" char embedded_ptx_code_mirror[];

namespace sibr {
	OrderRenderer::OrderRenderer(int w, int h,
		const std::vector<sibr::InputCamera::Ptr>& cameras,
		const sibr::Mesh::Ptr& proxy,
		sibr::Texture2DArrayRGB32F::Ptr& rgbs,
		sibr::Texture2DArrayRGB32F::Ptr& specs,
		sibr::Texture2DArrayLum32F::Ptr& depths,
		sibr::Texture2DArrayRGB32F::Ptr& normals,
		sibr::Texture2DArrayRGB32F::Ptr& radiance,
		sibr::Texture2DArrayRGB32F::Ptr& radianceAdd,
		sibr::Texture2DArrayLum32F::Ptr& occs,
		float scale
	) : _w(w), _h(h), _proxy(proxy), _scale(scale) {
		_rtMax = SIBR_MAX_SHADER_ATTACHMENTS;
		_numRT = 1;
		//Textures
		_rgbs = rgbs;
		_rads = radiance;
		_tarRadAdds = radianceAdd;
		_specs = specs;
		_depths = depths;
		_normals = normals;
		_occs = occs;

		_maxNumCams = cameras.size();
		_camsCount = int(_maxNumCams);

		// Populate the cameraInfos array (will be uploaded to the GPU).
		_cameraInfos.clear();
		_cameraInfos.resize(_maxNumCams);
		for (size_t i = 0; i < _maxNumCams; ++i) {
			const auto& cam = cameras[i];
			_cameraInfos[i].vp = cam->viewproj();
			_cameraInfos[i].inv_vp = cam->invViewproj();
			_cameraInfos[i].pos = cam->position();
			_cameraInfos[i].dir = cam->dir();
			_cameraInfos[i].selected = cam->isActive();
		}

		// Compute the max number of cameras allowed.
		GLint maxBlockSize = 0, maxSlicesSize = 0;
		glGetIntegerv(GL_MAX_UNIFORM_BLOCK_SIZE, &maxBlockSize);
		glGetIntegerv(GL_MAX_ARRAY_TEXTURE_LAYERS, &maxSlicesSize);
		// For each camera we store two matrix, 2 vecs3, 2 floats (including padding).
		const unsigned int bytesPerCamera = 4 * (2 * 16 + 2 * 3 + 2);
		const unsigned int maxCamerasAllowed = std::min((unsigned int)maxSlicesSize, (unsigned int)(maxBlockSize / bytesPerCamera));
		std::cout << "[OrderRenderer] " << "MAX_UNIFORM_BLOCK_SIZE: " << maxBlockSize << ", MAX_ARRAY_TEXTURE_LAYERS: " << maxSlicesSize << ", meaning at most " << maxCamerasAllowed << " cameras." << std::endl;

		// Create UBO.
		_uboIndex = 0;
		glGenBuffers(1, &_uboIndex);
		glBindBuffer(GL_UNIFORM_BUFFER, _uboIndex);
		glBufferData(GL_UNIFORM_BUFFER, sizeof(CameraUBOInfos) * _maxNumCams, &_cameraInfos[0], GL_DYNAMIC_DRAW);
		glBindBuffer(GL_UNIFORM_BUFFER, 0);

		//Setting up 
		//POSITION and NORMALS
		GLShader::Define::List defines;
		defines.emplace_back("NUM_CAMS", _maxNumCams);
		defines.emplace_back("K", 4);
		defines.emplace_back("scale", _scale);

		_posNormRT.reset(new sibr::RenderTargetRGBA32F(_w, _h, 0U, 6));
		_posNormTarRT.reset(new sibr::RenderTargetRGBA32F(_w, _h, 0U, 6));

		_shaderPosNorm.init("PosNorm",
			sibr::loadFile(sibr::getShadersDirectory("indoor_relighting") + "/posNormal.vert"),
			sibr::loadFile(sibr::getShadersDirectory("indoor_relighting") + "/posNormalAngle.frag", defines));

		_paramMVP.init(_shaderPosNorm, "MVP");
		_paramV.init(_shaderPosNorm, "V");
		_paramM.init(_shaderPosNorm, "M");
		_uCamPos.init(_shaderPosNorm, "camPos");
		_uCamDir.init(_shaderPosNorm, "camDir");
		_uScaling.init(_shaderPosNorm, "scaleFocalHeight");


		//RGB and SPEC with IBR type rendering
		_out_rgb_spec_IBR.reset(new sibr::RenderTargetRGB32F(_w, _h, 0U, 8));
		_shaderRgbSpecIBR.init("RgbSpecIBR",
			sibr::loadFile(sibr::getShadersDirectory("indoor_relighting") + "/repro.vert"),
			sibr::loadFile(sibr::getShadersDirectory("indoor_relighting") + "/rgbSpecIBR.frag", defines));
		_orderCam.init(_shaderRgbSpecIBR, "orderCam");
		_uCamPosIBR.init(_shaderRgbSpecIBR, "camPos");
		_uCamDirIBR.init(_shaderRgbSpecIBR, "camDir");
		_expIBR.init(_shaderRgbSpecIBR, "exposure");

		//RGB and SPEC with LUMINANCE ordering
		_out_rgb_spec_Lum.reset(new sibr::RenderTargetRGB32F(_w, _h, 0U, 8));
		_shaderRgbSpecLum.init("RgbSpecLum",
			sibr::loadFile(sibr::getShadersDirectory("indoor_relighting") + "/repro.vert"),
			sibr::loadFile(sibr::getShadersDirectory("indoor_relighting") + "/rgbSpecLum.frag", defines));
		_orderCam.init(_shaderRgbSpecLum, "orderCam");
		_uCamPosLum.init(_shaderRgbSpecLum, "camPos");
		_uCamDirLum.init(_shaderRgbSpecLum, "camDir");
		_expLum.init(_shaderRgbSpecLum, "exposure");


		//RADIANCE SOURCE and ALBEDO shader
		_radAlbedo.reset(new sibr::RenderTargetRGBA32F(_w, _h, 0U, 2));
		_shaderRadAlbedo.init("Radiance",
			sibr::loadFile(sibr::getShadersDirectory("indoor_relighting") + "/repro.vert"),
			sibr::loadFile(sibr::getShadersDirectory("indoor_relighting") + "/radAlbedo.frag", defines));
		_expO.init(_shaderRadAlbedo, "exposure");

		//TARGET DATA: MIRROR IM and TARGET RADIANCE shader and raycaster
		_tarData.reset(new sibr::RenderTargetRGBA32F(_w, _h, 0U, 7));
		_shaderTarData.init("TarData",
			sibr::loadFile(sibr::getShadersDirectory("indoor_relighting") + "/repro.vert"),
			sibr::loadFile(sibr::getShadersDirectory("indoor_relighting") + "/tarRadMirror.frag", defines));
		_expTarSce.init(_shaderTarData, "exposure");
		_uCamPosTar.init(_shaderTarData, "camPos");
		_turnOff.init(_shaderTarData, "turnOff");

		updateTargetGeometry(nullptr, std::vector<sibr::InputCamera::Ptr>());

		_posMirrorRT.reset(new sibr::RenderTargetRGBA32F(_w, _h, 0U, 3));

		SIBR_LOG << "Creating Optix module and programs." << std::endl;

		OptixRaycaster::Options options;
		options.missName = "__miss__radiance";
		options.closestName = "__closesthit__pos";
		options.anyName = "__anyhit__radiance";
		options.ptxCode = std::string(embedded_ptx_code_mirror);
		options.debug = true;
		options.entryName = "__raygen__renderPosFromBuffer";

		raycaster.createPrograms(options);
		SIBR_LOG << "Adding mesh." << std::endl;
		raycaster.createScene(*proxy);
		SIBR_LOG << "Binding program records." << std::endl;
		raycaster.bindProgramsAndScene();
		raycaster.registerSource(*_posNormTarRT, 0);
		raycaster.registerDirection(*_posNormTarRT, 2);
		raycaster.registerDestination(_posMirrorRT);

	}

	sibr::OrderRenderer::~OrderRenderer()
	{
	}

	void OrderRenderer::updateCameras(const std::vector<uint>& camIds) {
		// Reset all cameras.
		for (auto& caminfos : _cameraInfos) {
			caminfos.selected = 0;
		}
		// Enabled the ones passed as indices.
		for (const auto& camId : camIds) {
			_cameraInfos[camId].selected = 1;
		}

		// Update the content of the UBO.
		glBindBuffer(GL_UNIFORM_BUFFER, _uboIndex);
		glBufferSubData(GL_UNIFORM_BUFFER, 0, sizeof(CameraUBOInfos) * _maxNumCams, &_cameraInfos[0]);
		glBindBuffer(GL_UNIFORM_BUFFER, 0);
	}

	void OrderRenderer::updateTargetGeometry(sibr::Mesh::Ptr proxyTar, const std::vector<sibr::InputCamera::Ptr>& cameras) {


		if (!proxyTar) {
			_proxyTar = _proxy;
			_depthsTar = _depths;
			return;
		}

		_proxyTar = proxyTar;
		bool createNeeded = false;
		if (!_depthsTar || _depthsTar == _depths) {
			_depthsTar = std::make_shared<sibr::Texture2DArrayLum32F>();
			std::cout << "creating pos3DTar" << std::endl;
			createNeeded = true;
		}

		std::vector<sibr::ImageFloat1> imData;

		for (int im = 0; im < cameras.size(); im++) {

			int w = _depths->w();
			int h = _depths->h();
			sibr::ImageFloat1::Ptr dm(new sibr::ImageFloat1(w, h));
			sibr::DepthRenderer DR(w, h);
			DR.render(*cameras[im], *_proxyTar);
			DR._depth_RT->readBack(*dm);
			imData.push_back(dm->clone());

		}

		if (createNeeded) {
			_depthsTar->createFromImages(imData);
		}
		else {
			_depthsTar->updateFromImages(imData);
		}

		SIBR_LOG << "Creating Optix module and programs." << std::endl;

		OptixRaycaster::Options options;
		options.missName = "__miss__radiance";
		options.closestName = "__closesthit__pos";
		options.anyName = "__anyhit__radiance";
		options.ptxCode = std::string(embedded_ptx_code_mirror);
		options.debug = true;
		options.entryName = "__raygen__renderPosFromBuffer";

		raycaster.createPrograms(options);
		SIBR_LOG << "Adding mesh." << std::endl;
		raycaster.createScene(*_proxyTar);
		SIBR_LOG << "Binding program records." << std::endl;
		raycaster.bindProgramsAndScene();
		raycaster.registerSource(*_posNormTarRT, 0);
		raycaster.registerDirection(*_posNormTarRT, 2);
		raycaster.registerDestination(_posMirrorRT);


	}


	void	OrderRenderer::process(const Camera& eye, int deactivate, float exp)
	{
		//Number of pixels for a 1mm delta at the given distance	
		float scaleFocalHeight = 0.001f * _scale * _h / (2.0 * tan(0.5 * eye.fovy()));

		auto timeLastFrame = std::chrono::steady_clock::now();

		int oldState = -1;
		if (deactivate >= 0) {
			oldState = _cameraInfos[deactivate].selected;
			_cameraInfos[deactivate].selected = 0;

			// Update the content of the UBO.
			glBindBuffer(GL_UNIFORM_BUFFER, _uboIndex);
			glBufferSubData(GL_UNIFORM_BUFFER, 0, sizeof(CameraUBOInfos) * _maxNumCams, &_cameraInfos[0]);
			glBindBuffer(GL_UNIFORM_BUFFER, 0);
		}



		//PASS 1 :
		//First render the 3D position
		renderPosNormals(eye, scaleFocalHeight);
		auto timeNow = std::chrono::steady_clock::now();
		float deltaTime = std::chrono::duration<float>(timeNow - timeLastFrame).count();
		std::cout << "PASS 1 " << deltaTime << std::endl;
		timeLastFrame = timeNow;

		//PASS 2 : We render the RGB ad Spec IBR like data
		renderRgbSpecIBR(eye, exp);
		timeNow = std::chrono::steady_clock::now();
		deltaTime = std::chrono::duration<float>(timeNow - timeLastFrame).count();
		std::cout << "PASS 2 " << deltaTime << std::endl;
		timeLastFrame = timeNow;

		//PASS 3 : We render the RGB ad Spec Lum ranked data
		renderRgbSpecLum(eye, exp);
		timeNow = std::chrono::steady_clock::now();
		deltaTime = std::chrono::duration<float>(timeNow - timeLastFrame).count();
		std::cout << "PASS 3 " << deltaTime << std::endl;
		timeLastFrame = timeNow;

		//PASS 4 : We render the RADIANCE and ALBEDO
		renderRadAlbedo(eye, exp);
		timeNow = std::chrono::steady_clock::now();
		deltaTime = std::chrono::duration<float>(timeNow - timeLastFrame).count();
		std::cout << "PASS 4 " << deltaTime << std::endl;
		timeLastFrame = timeNow;

		//PASS 5 : Target Data
		//Raycast Mirror dir
		raycaster.onRender();

		//Render the data from it
		_shaderTarData.begin();
		_uCamPosTar.set(eye.position());
		_expTarSce.set(exp);
		_turnOff.set(_turnOffState);
		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, _posNormTarRT->handle(0));
		glActiveTexture(GL_TEXTURE1);
		glBindTexture(GL_TEXTURE_2D, _posNormTarRT->handle(1));
		glActiveTexture(GL_TEXTURE2);
		glBindTexture(GL_TEXTURE_2D, _posMirrorRT->handle(0));
		glActiveTexture(GL_TEXTURE3);
		glBindTexture(GL_TEXTURE_2D_ARRAY, _depthsTar->handle());
		glActiveTexture(GL_TEXTURE4);
		glBindTexture(GL_TEXTURE_2D_ARRAY, _rads->handle());
		glActiveTexture(GL_TEXTURE5);
		glBindTexture(GL_TEXTURE_2D_ARRAY, _tarRadAdds->handle());
		glActiveTexture(GL_TEXTURE6);
		glBindTexture(GL_TEXTURE_2D, _radAlbedo->handle(0));
		glActiveTexture(GL_TEXTURE7);
		glBindTexture(GL_TEXTURE_2D_ARRAY, _rgbs->handle());
		glActiveTexture(GL_TEXTURE8);
		glBindTexture(GL_TEXTURE_2D_ARRAY, _occs->handle());

		// Bind UBO to shader, after all possible textures.
		glBindBuffer(GL_UNIFORM_BUFFER, _uboIndex);
		glBindBufferBase(GL_UNIFORM_BUFFER, 9, _uboIndex);
		glBindBuffer(GL_UNIFORM_BUFFER, 0);

		_tarData->clear();
		_tarData->bind();
		RenderUtility::renderScreenQuad();
		_tarData->unbind();

		_shaderTarData.end();

		glFinish();
		timeNow = std::chrono::steady_clock::now();
		deltaTime = std::chrono::duration<float>(timeNow - timeLastFrame).count();
		std::cout << "PASS 5 " << deltaTime << std::endl;
		timeLastFrame = timeNow;

		if (deactivate >= 0) {
			_cameraInfos[deactivate].selected = oldState;
			// Update the content of the UBO.
			glBindBuffer(GL_UNIFORM_BUFFER, _uboIndex);
			glBufferSubData(GL_UNIFORM_BUFFER, 0, sizeof(CameraUBOInfos) * _maxNumCams, &_cameraInfos[0]);
			glBindBuffer(GL_UNIFORM_BUFFER, 0);
		}
	}

	void sibr::OrderRenderer::renderPosNormals(const Camera& eye, float scaleFocalHeight) {

		glViewport(0, 0, _w, _h);
		_shaderPosNorm.begin();
		_paramMVP.set(eye.viewproj());
		_paramV.set(eye.view());
		Matrix4f M = Matrix4f::Identity();
		_paramM.set(M);
		_uCamPos.set(eye.position());
		_uCamDir.set(eye.dir());
		_uScaling.set(scaleFocalHeight);
		//_compressNormals.set(0);

		_posNormRT->clear();
		_posNormRT->bind();
		_proxy->render(true, false);

		_posNormTarRT->clear();
		_posNormTarRT->bind();
		_proxyTar->render(true, false);

		_shaderPosNorm.end();

		glFinish();
	}

	void sibr::OrderRenderer::renderRgbSpecIBR(const Camera& eye, float exp) {
		auto timeLastFrame = std::chrono::steady_clock::now();

		glViewport(0, 0, _w, _h);
		_out_rgb_spec_IBR->clear();
		_out_rgb_spec_IBR->bind();

		_shaderRgbSpecIBR.begin();
		_uCamPosIBR.set(eye.position());
		_uCamDirIBR.set(eye.dir());
		_expIBR.set(exp);

		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, _posNormRT->handle(0));
		glActiveTexture(GL_TEXTURE1);
		glBindTexture(GL_TEXTURE_2D, _posNormRT->handle(1));

		glActiveTexture(GL_TEXTURE2);
		glBindTexture(GL_TEXTURE_2D_ARRAY, _depths->handle());

		glActiveTexture(GL_TEXTURE3);
		glBindTexture(GL_TEXTURE_2D_ARRAY, _normals->handle());

		glActiveTexture(GL_TEXTURE4);
		glBindTexture(GL_TEXTURE_2D_ARRAY, _rgbs->handle());

		glActiveTexture(GL_TEXTURE5);
		glBindTexture(GL_TEXTURE_2D_ARRAY, _specs->handle());

		glActiveTexture(GL_TEXTURE6);
		glBindTexture(GL_TEXTURE_2D_ARRAY, _occs->handle());

		// Bind UBO to shader, after all possible textures.
		glBindBuffer(GL_UNIFORM_BUFFER, _uboIndex);
		glBindBufferBase(GL_UNIFORM_BUFFER, 7, _uboIndex);
		glBindBuffer(GL_UNIFORM_BUFFER, 0);

		glFinish();

		auto timeNow = std::chrono::steady_clock::now();
		float deltaTime = std::chrono::duration<float>(timeNow - timeLastFrame).count();
		std::cout << "PASS IBR Bind " << deltaTime << std::endl;
		// Perform rendering.
		RenderUtility::renderScreenQuad();
		_shaderRgbSpecIBR.end();
		_out_rgb_spec_IBR->unbind();

		glFinish();

		timeNow = std::chrono::steady_clock::now();
		deltaTime = std::chrono::duration<float>(timeNow - timeLastFrame).count();
		std::cout << "PASS IBR Render " << deltaTime << std::endl;


	}

	void sibr::OrderRenderer::renderRgbSpecLum(const Camera& eye, float exp) {

		glViewport(0, 0, _w, _h);
		_out_rgb_spec_Lum->clear();
		_out_rgb_spec_Lum->bind();

		_shaderRgbSpecLum.begin();
		_uCamPosLum.set(eye.position());
		_uCamDirLum.set(eye.dir());
		_expLum.set(exp);

		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, _posNormRT->handle(0));
		glActiveTexture(GL_TEXTURE1);
		glBindTexture(GL_TEXTURE_2D, _posNormRT->handle(1));

		glActiveTexture(GL_TEXTURE2);
		glBindTexture(GL_TEXTURE_2D_ARRAY, _depths->handle());

		glActiveTexture(GL_TEXTURE3);
		glBindTexture(GL_TEXTURE_2D_ARRAY, _normals->handle());

		glActiveTexture(GL_TEXTURE4);
		glBindTexture(GL_TEXTURE_2D_ARRAY, _rgbs->handle());

		glActiveTexture(GL_TEXTURE5);
		glBindTexture(GL_TEXTURE_2D_ARRAY, _specs->handle());

		glActiveTexture(GL_TEXTURE6);
		glBindTexture(GL_TEXTURE_2D_ARRAY, _occs->handle());

		// Bind UBO to shader, after all possible textures.
		glBindBuffer(GL_UNIFORM_BUFFER, _uboIndex);
		glBindBufferBase(GL_UNIFORM_BUFFER, 7, _uboIndex);
		glBindBuffer(GL_UNIFORM_BUFFER, 0);

		// Perform rendering.
		RenderUtility::renderScreenQuad();
		_shaderRgbSpecLum.end();
		_out_rgb_spec_Lum->unbind();

		glFinish();

	}

	void sibr::OrderRenderer::renderRadAlbedo(const Camera& eye, float exp) {
		glViewport(0, 0, _w, _h);
		_radAlbedo->clear();
		_radAlbedo->bind();

		_shaderRadAlbedo.begin();
		_expO.set(exp);

		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, _posNormRT->handle(0));

		glActiveTexture(GL_TEXTURE1);
		glBindTexture(GL_TEXTURE_2D, _posNormRT->handle(1));

		glActiveTexture(GL_TEXTURE2);
		glBindTexture(GL_TEXTURE_2D_ARRAY, _depths->handle());

		glActiveTexture(GL_TEXTURE3);
		glBindTexture(GL_TEXTURE_2D_ARRAY, _normals->handle());

		glActiveTexture(GL_TEXTURE4);
		glBindTexture(GL_TEXTURE_2D_ARRAY, _rgbs->handle());

		glActiveTexture(GL_TEXTURE5);
		glBindTexture(GL_TEXTURE_2D_ARRAY, _rads->handle());

		glActiveTexture(GL_TEXTURE6);
		glBindTexture(GL_TEXTURE_2D_ARRAY, _occs->handle());

		// Bind UBO to shader, after all possible textures.
		glBindBuffer(GL_UNIFORM_BUFFER, _uboIndex);
		glBindBufferBase(GL_UNIFORM_BUFFER, 7, _uboIndex);
		glBindBuffer(GL_UNIFORM_BUFFER, 0);

		// Perform rendering.
		RenderUtility::renderScreenQuad();
		_shaderRadAlbedo.end();
		_radAlbedo->unbind();

		glFinish();
	}





	void OrderRenderer::switchInput() {
		_turnOffState = !_turnOffState;
	}

} /*namespace sibr*/
