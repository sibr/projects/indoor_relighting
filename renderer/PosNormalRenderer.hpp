#ifndef POSNORMALTEXMAPRENDERER_HPP
#define POSNORMALTEXMAPRENDERER_HPP

# include <core/graphics/Shader.hpp>
# include <core/graphics/Mesh.hpp>
# include <core/graphics/Texture.hpp>
# include <core/graphics/Camera.hpp>

# include "Config.hpp"

namespace sibr {
	class SIBR_INDOORRELIGHTING_EXPORT PosNormalRenderer
	{
	public:
		typedef std::shared_ptr<PosNormalRenderer>	Ptr;

	public:
		PosNormalRenderer();
		~PosNormalRenderer();

		void	process(
			/*input*/	const Mesh& mesh,
			/*input*/	const Camera& eye,
			/*output*/	IRenderTarget& dst
		);

	private:
		GLShader			_shader;

		GLParameter			_paramMVP;
	};

} /*namespace sibr*/

#endif // __SIBR_EXP_RENDERER_TEXTUREDMESHRENDERER_HPP__
