# include "TextureMapRenderer.hpp"
# include "core/graphics/RenderUtility.hpp"

# define USE_PIXELART_MODEN 0 // just for fun (and e-art!)

namespace sibr
{

	TextureMapRenderer::~TextureMapRenderer() {};

	TextureMapRenderer::TextureMapRenderer(int w, int h, bool useFloat) : _useFloat(useFloat)
	{

		_textureMapShader.init("TextureMapShader",
			sibr::loadFile(sibr::Resources::Instance()->getResourceFilePathName("textureMapRenderer.vp")),
			sibr::loadFile(sibr::Resources::Instance()->getResourceFilePathName("textureMapRenderer.fp")));

		_textureMapShader_proj.init(_textureMapShader, "MVP");

		if (_useFloat)
			_textureMap_RT32F.reset(new sibr::RenderTargetRGB32F(w, h));
		else
			_textureMap_RT.reset(new sibr::RenderTargetRGB(w, h));


	}

	void TextureMapRenderer::setWH(int w, int h) {
		if (_useFloat)
			_textureMap_RT32F.reset(new sibr::RenderTargetRGB32F(w, h));
		else
			_textureMap_RT.reset(new sibr::RenderTargetRGB(w, h));
	}

	void TextureMapRenderer::render(const sibr::InputCamera& cam, const Mesh& mesh)
	{
#if USE_PIXELART_MODEN
		glPointSize(10.f);
#else
		glPointSize(2.f);
#endif
		if (_useFloat)
			_textureMap_RT32F->bind();
		else
			_textureMap_RT->bind();

		glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		if (_useFloat) {
			glViewport(0, 0, _textureMap_RT32F->w(), _textureMap_RT32F->h());
			glScissor(0, 0, _textureMap_RT32F->w(), _textureMap_RT32F->h());
		}
		else {
			glViewport(0, 0, _textureMap_RT->w(), _textureMap_RT->h());
			glScissor(0, 0, _textureMap_RT->w(), _textureMap_RT->h());
		}

		_textureMapShader.begin();
		_textureMapShader_proj.set(cam.viewproj());

		//std::cout << cam.znear() << " " << cam.zfar() << " " << cam.viewproj() << std::endl;
		mesh.render(true, false, sibr::Mesh::FillRenderMode);

		_textureMapShader.end();

	}

} // namespace