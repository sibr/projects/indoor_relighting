#define _DISABLE_EXTENDED_ALIGNED_STORAGE

//Relighting app related
#include "core/system/CommandLineArgs.hpp"
#include "core/raycaster/Raycaster.hpp"
#include "projects/indoor_relighting/renderer/indoorRelighterIBRScene.hpp"
#include "core/scene/BasicIBRScene.hpp"
#include "projects/indoor_relighting/renderer/OfflineRenderer.hpp"
//#include "core/graphics/PlaneEstimator.hpp"

#define PROGRAM_NAME "sibr_indoorRelighting_app"
//------------------------------------------------------------------------------

int main(int argc, char** argv) {

	sibr::CommandLineArgs::parseMainArgs(argc, argv);
	sibr::indoorRelighterAppArgs myArgs;

	//const std::string inPath = "D:/Users/jphilip/Relighting/RelightingScene/stonehenge/sibr_scene";
	const std::string inPath = myArgs.dataset_path;
	// /!\ do not put the model.pb at the end we now read the layout too
	const std::string model_path = myArgs.model;
	bool interactive = myArgs.interactive;
	bool video = false;
	int numFrames = myArgs.numFrames;
	bool synthetic = myArgs.synthetic;
	if (!interactive && myArgs.video) {
		video = true;
	}

	std::string outPath = inPath;
	if (myArgs.outPath.get() != std::string("")) {
		outPath = myArgs.outPath;
	}

	//We need to do that for tensorflow to work correctly
	for (int i = 1; i < argc; ++i)
		argv[i] = "";
	argc = 1;

	const bool logging_enabled = false;

	sibr::Window::Ptr			window = std::make_shared<sibr::Window>(256, 256, PROGRAM_NAME);

	if (myArgs.runTest) {
		sibr::OfflineRenderer OR(myArgs.outW.get(), myArgs.outH.get(), myArgs.model.get(),window);
		OR.render(myArgs.dataset_path.get() + "/test");
		return 0;
	}

	sibr::indoorRelighterIBRScene RL(
		myArgs,
		window,
		true);

	if ((myArgs.genTrainData && myArgs.synthetic) || myArgs.preprocessOnly || (myArgs.genTestData && !myArgs.synthetic) )
		return 0;

	if (interactive) {
		RL.runInteractiveSession();
	}
	else {
		std::vector<sibr::InputCamera::Ptr> camPath = 
			sibr::InputCamera::loadLookat(
				myArgs.camPath.get() + "/camerasRender.lookat"
				, { sibr::Vector2u(myArgs.outW,myArgs.outH) });
		RL.runSaveSession(camPath, outPath);
	}
	//else {
	//	std::cout << inPath << std::endl;

	//	std::vector<sibr::OutdoorRelighter::lightingCondition> lightConds = sibr::OutdoorRelighter::loadLightingConditions(inPath + "/lightingConditions.txt", scene->_zenith, scene->_north, outPath);
	//	if (lightConds.empty()) {
	//		std::cout << "Creating lighting conditions" << std::endl;
	//		if (video) {
	//			lightConds = RL.createLightingConditionsVideo(outPath + "/VideoFrames/", exr);
	//		}
	//		else {
	//			lightConds = RL.createLightingConditions(outPath, numFrames, exr);
	//		}
	//	}
	//	//Copy input
	//	for (auto lc : lightConds) {
	//		std::string ext;

	//		std::string oriImg = inPath + "/" + scene->data()->imgInfos()[lc.imId].filename;
	//		boost::filesystem::path outdir = boost::filesystem::path(lc.path).parent_path();
	//		if (outdir.empty() == false)
	//			boost::filesystem::create_directories(outdir);
	//		boost::filesystem::copy_file(oriImg, outdir.append("/input.png"), boost::filesystem::copy_option::overwrite_if_exists);
	//	}
	//	std::vector<sibr::OutdoorRelighter::lightingCondition> lightCondsIncr;
	//	for (int lc_num = 0; lc_num < lightConds.size(); lc_num += incrlc) {
	//		sibr::OutdoorRelighter::lightingCondition lc = lightConds[lc_num];
	//		lightCondsIncr.push_back(lc);
	//	}
	//	RL.runSaveSession(window, lightCondsIncr, false);
	//}



	return 0;

}
