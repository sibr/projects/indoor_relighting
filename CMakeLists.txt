
project(sibr_indoor_relighting_all)

add_subdirectory(apps)
add_subdirectory(preprocess)
add_subdirectory(renderer)

include(install_runtime)
subdirectory_target(${PROJECT_NAME} ${CMAKE_CURRENT_LIST_DIR} "projects/indoor_relighting")
