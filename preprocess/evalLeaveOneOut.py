import os
import argparse
import glob
import cv2
from skimage.metrics import structural_similarity as ssimFn
import multiprocessing
import lpips
import torch
from torchvision import transforms

loss_fn_alex = lpips.LPIPS(net='alex').cpu()
tran = transforms.ToTensor()
parser = argparse.ArgumentParser()
parser.add_argument("folders", nargs='+')

args = parser.parse_args()

folders=args.folders

def measure(f):
	num = int(os.path.splitext(os.path.basename(f))[0].split("_")[0])

	testFile = os.path.join(folder,str(num).zfill(8)+"_rdr.png")

	imTest=cv2.imread(testFile)
	imGT = cv2.imread(f)

	imTest=cv2.cvtColor(imTest,cv2.COLOR_BGR2RGB)
	imGT=cv2.cvtColor(imGT,cv2.COLOR_BGR2RGB)

	if imTest.shape != imGT.shape:
		imTest = cv2.resize(imTest,(imGT.shape[1],imGT.shape[0]))

	imTest=imTest[32:-32,32:-32]
	imGT=imGT[32:-32,32:-32]

	psnr=cv2.PSNR(imGT,imTest)

	with torch.no_grad():
		lpips_score = loss_fn_alex(2*tran(imTest)-1,2*tran(imGT)-1)
	
	(ssim, diff) = ssimFn(imGT, imTest ,multichannel=True, full=True)
	return (psnr,lpips_score.item(),ssim)

if __name__ == '__main__':
	

	for folder in folders:
		PSNR=0
		LPIPS=0
		SSIM=0
		numIm=0

		for f in glob.glob(folder+"\\*_gt.png")[0:32]:
			(psnr,lpips_score,ssim) = measure(f)

			PSNR +=psnr
			SSIM += ssim
			LPIPS += lpips_score
			numIm += 1

		PSNR/=numIm
		SSIM/=numIm
		LPIPS/=numIm
		print(folder)
		print("PSNR:",PSNR, " - LPIPS:",LPIPS ," - SSIM:",SSIM, " - DSSIM:", 0.5*(1-SSIM))


	
