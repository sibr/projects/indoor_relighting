import math
import os
from sys import flags
from tkinter import W
from typing import List, Tuple
import mitsuba as mi
import drjit as dr
import argparse
# Set the variant of the renderer
mi.set_variant('cuda_ad_rgb')
from mitsuba import ScalarTransform4f as T

class AlbedoIntegrator(mi.SamplingIntegrator):
    def __init__(self, props):
        print(props)
        mi.SamplingIntegrator.__init__(self,props)

    def sample(self: mi.SamplingIntegrator, scene: mi.Scene, sampler, ray: mi.RayDifferential3f, medium: mi.Medium = None, active: bool = True) -> Tuple[mi.Color3f, bool, List[float]]:


        ray                  = mi.Ray3f(ray)

        bsdfContext = mi.BSDFContext()

        si = scene.ray_intersect(ray,True)
        bsdf_val=si.bsdf(ray).eval(bsdfContext,si,si.to_local(si.n))
        return (bsdf_val, False, [])


mi.register_integrator("albedo", lambda props: AlbedoIntegrator(props))

def main(args):

    base_path=args.path
    light_name=args.light
    lookat_file=open(os.path.join(base_path,"coloredMesh","cameras.lookat"),'r')
    image_sizes_file=open(os.path.join(base_path,"coloredMesh","imageSizes.txt"),'r')

    # Load the mesh:
    mesh = mi.load_dict({
        "type": "ply",
        "filename": os.path.join(base_path,"coloredMesh","mesh.ply"),
        'bsdf': {
            'type': 'diffuse',
            'reflectance': {
                'type': 'mesh_attribute',
                'name': 'vertex_color'
            }
        }
    })
    #Rescale the color values
    mesh_params = mi.traverse(mesh)
    mesh_params["vertex_color"]/=255.0
    mesh_params.update()

    light= mi.load_file(os.path.join(base_path,"lightings",light_name+".xml"))

    scene = mi.load_dict({
        'type': 'scene',
        'shape': mesh,
        'emitter': light.shapes()[0]
    })

    integrator_albedo=mi.load_dict({'type': 'albedo'})
    integrator=mi.load_dict({'type': 'path'})

    out_dir = os.path.join(base_path,"exr",light_name)

    if not os.path.exists(out_dir):
        os.makedirs(out_dir)

    for lookat,sizes in zip(lookat_file.readlines(),image_sizes_file.readlines()):
        print("Rendering irradiance for: ",lookat,sizes)
        w=int(sizes.split('x')[0])
        h=int(sizes.split('x')[1])
        split_lookat=lookat.split()
        name=split_lookat[0]
        origin=[float(x) for x in lookat.split()[2].split('=')[1].split(",")]
        target=[float(x) for x in lookat.split()[4].split('=')[1].split(",")]
        up=[float(x) for x in lookat.split()[6].split('=')[1].split(",")]
        fovy=float(lookat.split()[8].split('=')[1])

        fovx=2*math.degrees(math.atan(math.tan(math.radians(fovy)/2)*w/h))

        camera = mi.load_dict({
                'type': 'perspective',
                'fov': fovx,
                'to_world': T.look_at(
                    origin=origin,
                    target=target,
                    up=up
                ),
                'sampler': {
                    'type': 'independent',
                    'sample_count': 64
                },
                'film': {
                    'type': 'hdrfilm',
                    'width': w,
                    'height': h,
                    'rfilter': {
                        'type': 'box',
                    },
                    'pixel_format': 'rgb',
                },
            })
        img_albedo = mi.render(scene,integrator=integrator_albedo,sensor=camera).maximum_(0.01)
        img = mi.render(scene,integrator=integrator,sensor=camera)
        # Write the rendered image to an EXR file
        mi.Bitmap((img/img_albedo).minimum_(1.0)).write(os.path.join(out_dir,name.split(".")[0]+"_irdc.exr"))

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Process some integers.')
    parser.add_argument('path',help='path to the scene folder, this folder should contain the coloredMesh folder')
    parser.add_argument('light',help='light name without the .xml, for instance outLight0')

    args = parser.parse_args()
    main(args)