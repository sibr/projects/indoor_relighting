#!/usr/bin/env python
#! -*- encoding: utf-8 -*-
import argparse
import glob
import subprocess
import os
import cv2
import time
from threading import Thread
import os
import numpy as np
parser = argparse.ArgumentParser()
parser.add_argument("folder")
parser.add_argument('--gamma', default=2.2, type=float,
    help="Gamma for tone mapping")
parser.add_argument('--exposure', default=0, type=float,
    help="Exposure for tone mapping as EV stops")
parser.add_argument('--masks', action='store_true',
    help="Generate masks for black pixels")
parser.add_argument('--textureRC', action='store_true',
    help="Use RC texture naming convention")
parser.add_argument('--width', default=0, type=int,
    help="Exposure for tone mapping as EV stops")
args = parser.parse_args()


files = sorted(glob.glob(args.folder+"/*.exr"))

#Writing paramters
file = open(args.folder+"/tone_mapping.txt", "w") 
file.write("Gamma "+str(args.gamma)+'\n') 
file.write("Exposure "+str(args.exposure)) 
file.close() 

m=0
for f in files:
	if args.textureRC:
		jpgFilename = (f+".texture02.jpg").replace('\\', '/')
	else:
		jpgFilename = (os.path.splitext(f)[0]+".jpg").replace('\\', '/')
	imExr = cv2.imread(f, cv2.IMREAD_ANYCOLOR | cv2.IMREAD_ANYDEPTH)
	im = 255*cv2.pow(pow(2,args.exposure)*imExr,1/args.gamma)
	if args.width>0:
		width = args.width
		height = int(im.shape[0]*float(width)/im.shape[1])
		dim = (width, height)
		im = cv2.resize(im,dim, interpolation = cv2.INTER_AREA)
	cv2.imwrite(jpgFilename, im)
	if args.masks:
		num_mask = str(m).zfill(8)
		path_mask = ("/").join(os.path.splitext(f)[0].replace('\\', '/').split('/')[0:-1])
		maskFilename = (path_mask+"/"+num_mask+"_mask.png").replace('\\', '/')
		mask = cv2.cvtColor(imExr, cv2.COLOR_RGB2GRAY)
		mask[mask>0] = 255
		kernel = np.ones((3,3),np.uint8)
		mask = cv2.erode(mask,kernel,iterations = 1)
		cv2.imwrite(maskFilename,mask)
		m+=1


	



