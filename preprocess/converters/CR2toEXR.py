#!/usr/bin/env python
#! -*- encoding: utf-8 -*-
import argparse
import glob
import subprocess
import os
import cv2
import time
from threading import Thread
os.environ["OPENCV_IO_ENABLE_OPENEXR"]="1"
parser = argparse.ArgumentParser()
parser.add_argument("folder")
parser.add_argument("xmp")
parser.add_argument('--dng', action='store_true', 
    help="Uses dng instead of cr2")
args = parser.parse_args()

if args.dng:
	files = glob.glob(args.folder+"/*.dng")
else:
	files = glob.glob(args.folder+"/*.CR2")

def treat(f):
	exrFilename = (os.path.splitext(f)[0]+".exr").replace('\\', '/')
	if not os.path.exists(exrFilename):
		subprocess.call(['darktable-cli.exe', f , args.xmp, exrFilename, '--width' , '3000'])
		time.sleep(2)
		#Open And write with cv as halfFloat
		im = cv2.imread(exrFilename, cv2.IMREAD_ANYCOLOR | cv2.IMREAD_ANYDEPTH)
		cv2.imwrite(exrFilename,im,  [cv2.IMWRITE_EXR_TYPE, cv2.IMWRITE_EXR_TYPE_HALF])

i=0
for f in files:
	thrd = Thread(target=treat, args=(f, ))
	thrd.start()
	i+=1
	if i%5 == 0:
		thrd.join()



