#!/usr/bin/env python
#! -*- encoding: utf-8 -*-
import os
import shutil
import glob
import argparse
import OpenEXR

parser = argparse.ArgumentParser()
parser.add_argument("inFolder", type=str,help="folder containing the input data")
arg=parser.parse_args()
fileList = open(arg.inFolder+"/scene_metadata.txt","w") 

fileList.write("Scene Metadata File\n")
fileList.write("\n")
fileList.write("[list_images]\n")
fileList.write("<filename> <image_width> <image_height>\n")

for file in glob.glob(arg.inFolder+"/images/*.exr"):
	header = OpenEXR.InputFile(file).header()
	fileList.write(os.path.basename(file)+" "+str(header["dataWindow"].max.x)+" "+str(header["dataWindow"].max.y)+"\n")

fileList.write("\n")
fileList.write("\n")
fileList.write("// Always specify active/exclude images after list images\n")
fileList.write("\n")
fileList.write("[exclude_images]\n")
fileList.write("<image1_idx> <image2_idx> ... <image3_idx>\n")
fileList.write("\n")
fileList.close()