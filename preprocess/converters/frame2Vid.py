#!/usr/bin/env python
#! -*- encoding: utf-8 -*-
import os
import argparse
parser = argparse.ArgumentParser()
parser.add_argument("folder")
args = parser.parse_args()
folderFrame=args.folder

os.system("ffmpeg -y -r 24 -i "+folderFrame+"/%08d.png -pix_fmt yuv420p -vcodec h264 -y -vf \"pad=ceil(iw/2)*2:ceil(ih/2)*2\" "+folderFrame+"/vid.mp4")