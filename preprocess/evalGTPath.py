import os
import argparse
import glob
import cv2
from skimage.metrics import structural_similarity as ssimFn
import lpips
import torch
from torchvision import transforms

loss_fn_alex = lpips.LPIPS(net='alex').cpu()
tran = transforms.ToTensor()
parser = argparse.ArgumentParser()
parser.add_argument("folderGT")
parser.add_argument("folderTest")
args = parser.parse_args()
folderGT=args.folderGT
folderTest=args.folderTest

PSNR=0
SSIM=0
LPIPS=0
numIm=0
for f in glob.glob(folderTest+"\\*.png"):
	num = int(os.path.splitext(os.path.basename(f))[0])
	GTfile = os.path.join(folderGT,"Cam"+str(num).zfill(3)+"_rgb.jpg")

	imGT=cv2.imread(GTfile)
	imTest = cv2.imread(f)

	imTest=cv2.cvtColor(imTest,cv2.COLOR_BGR2RGB)
	imGT=cv2.cvtColor(imGT,cv2.COLOR_BGR2RGB)

	if imTest.shape != imGT.shape:
		imTest = cv2.resize(imTest,(imGT.shape[1],imGT.shape[0]))


	psnr=cv2.PSNR(imGT,imTest)

	with torch.no_grad():
		lpips_score = loss_fn_alex(2*tran(imTest)-1,2*tran(imGT)-1).item()
	
	(ssim, diff) = ssimFn(imGT, imTest ,multichannel=True, full=True)
	print(psnr,",",ssim,",",lpips_score)

	PSNR +=psnr
	SSIM += ssim
	LPIPS += lpips_score

	numIm += 1

PSNR/=numIm
SSIM/=numIm
LPIPS/numIm

print("PSNR:",PSNR," - SSIM:",SSIM," - LPIPS:",LPIPS)
	
